<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Event extends CI_Controller {

    protected $loggedin = false;
    protected $isAdmin = false;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('events');
        $this->loggedin = ($this->session->userdata('id'));
        if($this->loggedin) $this->isAdmin = ($this->session->userdata('role')==1);
    }

    function index()#order desc
    {
        if($this->input->post()){
            $post = $this->input->post();
            if(isset($post['edit_id'])){
                if($post['edit_id']>0)
                    $data['item'] = $this->events->select(array('id'=>$post['edit_id']),false);
                #if($post['del_id']>0) $this->destroy ($post['del_id']);
            }else $this->store($post);
        }
        if($this->input->get('delete')) $this->destroy ($this->input->get('delete'));
        $data['title'] = 'Landlord Event';
        $data['items'] = $this->events->select(null,true);
        $data['page'] = 'eventlist';
        $this->load->view('welcome_message', $data);
    }

    function show($eid)
    {
        $data['title'] = 'Landlord Event';
        $data['items'] = $this->events->select(array('id'=>$eid),true);
        $data['page'] = 'eventlist';
        $this->load->view('welcome_message', $data);
    }

    function footer()
    {
        $data['items'] = $this->events->latest();
        $this->load->view('parts/event_footer', $data);
    }
    
    function store($event)
    {
        if(!$this->isAdmin) show_404 ();
        $this->events->save($event);
    }

    function destroy($eid)
    {
        if(!$this->isAdmin) show_404 ();
        $this->events->delete(array('id'=>$eid));
    }

    function edit($eid)
    {
        
    }

    function update($eid)
    {
        
    }

}