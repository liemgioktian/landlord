<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Listing extends CI_Controller{
    
    protected $loggedin = false;
    protected $isadmin = false;
    
    public function __construct(){
        parent::__construct();
        $this->load->model('listings');
        $this->load->library('landlordlib');
        $this->loggedin = ($this->session->userdata('id'));
        if($this->loggedin) $this->isadmin = ($this->session->userdata('role')==1);
    }
    
    public function admin()
    {
        $exp = EXPIRED_DAYS;
        $almost = EXPIRED_WARNING;
         if($this->input->get()){
            $this->load->library('datatables');
            if (!$this->isadmin) $this->datatables->where('id',0);
            echo $this->datatables
                    ->select('p_property.id')
                    ->select("IF(hot=1,'checked=\"checked\"','') as hot",false)
                    ->edit_column('hot','<input type="checkbox" $1 onclick="window.location=\''.site_url('property/sethot').'/$2\'">','hot,p_property.id')
                    ->select("CONCAT(jenis, ' ', bentuk,' di ', lokasi) AS alias",false)
                    ->edit_column('alias',
                            '<a target="_blank" href="'.  site_url('property/show').'/$1"><b style="text-transform:capitalize">$2</b></a>'
                            ,'p_property.id, alias')
                    ->unset_column('p_property.id')
                    ->select('s_marketing.nama')
                    ->select("DATE_FORMAT(posted,'%d %b %Y') as posted",false)
                    ->select("DATE_FORMAT(first_publish,'%d %b %Y') as first_publish",false)
                    ->select("DATE_FORMAT(published,'%d %b %Y') as published",false)
                    ->select("DATE_FORMAT(published + INTERVAL $exp DAY,'%d %b %Y') as expired",false)
//                    ->select("DATE_FORMAT(sold,'%d %b %Y') as sold",false)
                    ->select("IF(sold IS NULL,"
                            . "CONCAT('<a href=\"javascript:markAsSold(',p_property.id,')\">[Mark Sold]</a>')"
                            . ",DATE_FORMAT(sold,'%d %b %Y')) as sold",false)
                    ->select("IF(first_publish IS NULL, 'NEW LISTING',"
                            . "IF(sold IS NOT NULL,'SOLD',"
                            . "IF(TIMESTAMPDIFF(DAY, published, CURDATE()) >= $exp,'EXPIRED',"
                            . "IF(published + INTERVAL $exp DAY < NOW() + INTERVAL $almost DAY,'ALMOST EXPIRED',"
                            . "'ACTIVE')))) as PROP_STAT",false)
                    ->edit_column('PROP_STAT','<b>$1</b>','PROP_STAT')
                    ->select("IF(sold IS NOT NULL,'',CONCAT('<a class=\"btn ajax btn-primary\" href=\"".site_url('property')."/setPublicity/',p_property.id,'\">(un)publish</a>'))",false)
                    ->select("IF(sold IS NOT NULL,'',CONCAT("
                            
                            . "'<div class=\"btn-group\"><a class=\"btn btn-info\" href=\"".site_url('property')."/edit/',p_property.id,"
                            . "'\">edit</a>',"
                            
//                            . "'<a class=\"btn btn-danger\" href=\"".site_url('property')."/delete/',p_property.id,"
//                            . "'\">delete</a></div>'"
                            
                            . "'<a class=\"btn btn-danger\" href=\"javascript:llconfirm(\'".site_url('property')."/delete/',p_property.id,"
                            . "'\',true)\">delete</a></div>'"
                            
                            . ")) as edel",false)
                    ->from('p_property')
                    ->join('s_login','p_property.agen=s_login.id','left')
                    ->join('s_marketing','s_marketing.id=s_login.marketing_id','left')
                    ->where('p_property.active',1)
                    ->generate();
        }else{
            if (!$this->isadmin) show_error(UNAUTHORIZED, 401);
            $data['title'] = 'LISTING PROPERTY';
            $data['page'] = 'listing_admin';
            $this->load->view('welcome_message', $data);
        }
    }
    
    public function recyclebin(){
        if($this->input->get()){
            $this->load->library('datatables');
            if (!$this->isadmin) $this->datatables->where('id',0);
            echo $this->datatables
                    ->select("CONCAT(jenis, ' ', bentuk,' di ', lokasi) AS alias",false)
                    ->select('p_property.id')
                    ->edit_column(
                            'p_property.id',
                            '<div class="btn-group">' .
                            '<a class="btn btn-small btn-primary" href="' . site_url('property/restore') . '/$1">restore property</a>' .
                            '<a class="btn btn-small btn-danger" href="' . site_url('property/delete_permanent') . '/$1">delete permanent</a>' .
                            '</div>'
                            , 'p_property.id'
                    )
                    ->from('p_property')
                    ->where('p_property.active',0)
                    ->generate();
        }else{
            if (!$this->isadmin) show_error(UNAUTHORIZED, 401);
            $data['title'] = 'RECYCLE BIN';
            $data['page'] = 'recyclebin';
            $this->load->view('welcome_message', $data);
        }
    }
    
    public function manage() #admin + marketing #info(almost-expired)
    {
        if (!$this->loggedin) show_error(UNAUTHORIZED, 401);
        #if ($this->isadmin) $this->admin ();
        else{
            if($this->input->get()){
                $limit = $this->input->get('limit');
                $offset = $this->input->get('offset');
                $filter = $this->input->get('filter');
                $sorting = $this->input->get('sorting');
                $data['items'] = $this->listings->manage($limit,$offset,$filter,$sorting);
                $this->load->view('singleListing',$data);
            }else{
                $data['title']      = 'Manage Listing';
                $this->load->model('users');
                $agen = $this->users->getAgen($this->session->userdata('id'));
                $data['sub_title']  = $agen['nama'];
                $data['page']       = 'listing';
                $this->load->view('welcome_message',$data);
            }
        }
    }
    
    public function index()
    {
        if($this->input->post()) $data['post'] = $this->input->post();
        if($this->input->get()){
            $limit = $this->input->get('limit');
            $offset = $this->input->get('offset');
            $filter = $this->input->get('filter');
            $sorting = $this->input->get('sorting');
            $data['items'] = $this->listings->draw($limit,$offset,$filter,$sorting);
            $this->load->view('singleListing',$data);
            #die($this->db->last_query());
        }else{
            $data['title']      = 'Property Listing';
            $data['sub_title']  = 'Tampilkan Semua';
            $data['page']       = 'listing';
            $this->load->model('users');
            $data['marketing']  = $this->users->getAgenList();
            $this->load->view('welcome_message',$data);
        }
    }
    
    public function perbandingan($id1, $id2)
    {
        
    }
    
    function report()
    {
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $XLSactvSheet = $this->excel->getActiveSheet();
        $XLSactvSheet->setTitle('Sheet1');
        $startingCol = 'B';
        
        #header
        $borders = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)));
        $bgcolor = array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => 'FFFF00'));
        $report_header = array('NO.','TGL. INPUT','TGL. LISTING','MARKETING','PEMILIK PROPERTY','ALAMAT PEMILIK','TELPON',
        'ALAMAT LISTING','J/S','JENIS','LT','LB','BTK','HDP','LANTAI','KT','KM','AC','TELP','LIST','AIR','HARGA','SERT.','KET');
        $headerCol = $startingCol;#start from B
        $headerTop = '2';
        $headerBottom = '3';
        foreach($report_header as $rh){
            $XLSactvSheet->setCellValue($headerCol.$headerTop, $rh);
            
            $XLSactvSheet->getStyle($headerCol.$headerTop)->applyFromArray($borders);
            $XLSactvSheet->getStyle($headerCol.$headerBottom)->applyFromArray($borders);
            
            $XLSstyle = $XLSactvSheet->getStyle($headerCol.$headerTop);
            $XLSstyle->getFill()->applyFromArray($bgcolor);
            $XLSstyle->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            
            if($rh!='HARGA')$XLSactvSheet->mergeCells($headerCol."$headerTop:".$headerCol.$headerBottom);
            else{
                $headerColi = $headerCol; $headerColi++;
                $XLSactvSheet->mergeCells($headerCol."$headerTop:".$headerColi.$headerTop);
                $XLSactvSheet->getStyle($headerColi.$headerTop)->applyFromArray($borders);
                $XLSactvSheet->setCellValue($headerCol.$headerBottom, 'JUAL');
                $XLSactvSheet->setCellValue($headerColi.$headerBottom, 'SEWA');
                foreach(array($headerCol.$headerBottom,$headerColi.$headerBottom) as $hb){
                    $hbp = $XLSactvSheet->getStyle($hb);
                    $hbp->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $hbp->getFill()->applyFromArray($bgcolor);
                    $hbp->applyFromArray($borders);
                }
                $headerCol++;
            }
            $headerCol ++;
        }

        #body
        $rows = $this->listings->report();
        
//        echo '<pre/>';
//        print_r($headerBottom+count($rows));
//        die();
        $no = 1;
        for($row = $headerBottom+1; $row<= $headerBottom+count($rows);$row++){
            $col=$startingCol;
            while($col!='Z'){
                switch ($col) {
                    case 'B': $content = $no; break;
                    case 'C': $content = $rows[$no-1]->tgl_input; break;
                    case 'D': $content = $rows[$no-1]->tgl_listing; break;
                    case 'E': $content = strtoupper($rows[$no-1]->marketing); break;
                    case 'F': $content = $rows[$no-1]->pemilik_property; break;
                    case 'G': $content = $rows[$no-1]->alamat_pemilik; break;
                    case 'H': $content = $rows[$no-1]->telpon; break;
                    case 'I': $content = $rows[$no-1]->alamat_listing; break;
                    case 'J': $content = $rows[$no-1]->j_s; break;
                    case 'K': $content = $rows[$no-1]->jenis; break;
                    case 'L': $content = $rows[$no-1]->lt; break;
                    case 'M': $content = $rows[$no-1]->lb; break;
                    case 'N': $content = $rows[$no-1]->btk; break;
                    case 'O': $content = $rows[$no-1]->hdp; break;
                    case 'P': $content = $rows[$no-1]->lantai; break;
                    case 'Q': $content = $rows[$no-1]->kt; break;
                    case 'R': $content = $rows[$no-1]->km; break;
                    case 'S': $content = $rows[$no-1]->ac==0?'':'Ada'; break;
                    case 'T': $content = $rows[$no-1]->telp==0?'':'Ada'; break;
                    case 'U': $content = $rows[$no-1]->list; break;
                    case 'V': $content = $rows[$no-1]->air; break;
                    case 'W': $content = $rows[$no-1]->jual>0?($rows[$no-1]->jual/1000000).' Jt':''; break;
                    case 'X': $content = $rows[$no-1]->sewa>0?($rows[$no-1]->sewa/1000000).' Jt':''; break;
                    case 'Y': 
                        if($rows[$no-1]->no_sertifikat!=''){
                            $prefix = explode('(', $rows[$no-1]->jenis_sertifikat);
                            $content = $prefix[0] ." No.".$rows[$no-1]->no_sertifikat;
                        }else $content = $rows[$no-1]->jenis_sertifikat; 
                        break;
                    default: $content = ''; break;#ket
                }
                $XLSactvSheet->setCellValue($col.$row, $content);
                if($col=='E') $XLSactvSheet->getStyle($col.$row)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => '00b050')));
                if($col=='W' || $col=='X') $XLSactvSheet->getStyle($col.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                else if($col>='J' && $col<='V') $XLSactvSheet->getStyle($col.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $col++;
            }
          $no++;
        }
        
        $today = new DateTime();
        $filename="[LandlordIndonesia] Rekap Listing Per ".$today->format('d-m-Y');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');

        //change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    
    function top()
    {
        if($this->input->get()){
            $offset = $this->input->get('offset');
            if($offset==0){
                $filter = $this->input->get('filter');
                $sorting['order'] = 'dilihat';
                $sorting['ascdesc'] = 'DESC';
                $data['items'] = $this->listings->draw(TOP_LISTING_COUNT,0,$filter,$sorting);
                $this->load->view('singleListing',$data);
            }
        }else{
            $data['title']      = 'Top Listing';
            $data['sub_title']  = 'Landlord Indonesia';
            $data['page']       = 'listing';
            $this->load->model('users');
            $data['marketing']  = $this->users->getAgenList();
            $data['disable_order'] = true;
            $this->load->view('welcome_message',$data);
        }
    }
    
    function hot()
    {
        if($this->input->get()){
            $offset = $this->input->get('offset');
            if($offset==0){
                $filter = $this->input->get('filter');
                $filter['hot'] = 1;
                $limit = $this->input->get('limit');
                $sorting = $this->input->get('sorting');
                $data['items'] = $this->listings->draw($limit,0,$filter,$sorting);
                $this->load->view('singleListing',$data);
            }
        }else{
            $data['title']      = 'Hot Listing';
            $data['sub_title']  = 'Landlord Indonesia';
            $data['page']       = 'listing';
            $this->load->model('users');
            $data['marketing']  = $this->users->getAgenList();
            $this->load->view('welcome_message',$data);
        }
    }
    
    function soldby($agentId)
    {
        if($this->input->get()){
            $offset = $this->input->get('offset');
            if($offset==0){
                $filter = $this->input->get('filter');
                $filter['agen'] = $agentId;
                $filter['sold IS NOT NULL'] = null;
                $limit = $this->input->get('limit');
                $sorting = $this->input->get('sorting');
                $data['items'] = $this->listings->draw($limit,0,$filter,$sorting, true);
                $this->load->view('singleListing',$data);
            }
        }else{
            $data['title']      = 'Sold Listing';
            $data['page']       = 'listing';
            $this->load->model('users');
            $agent = $this->users->getAgen($agentId);
            $data['sub_title']  = $agent['nama'];
            $this->load->view('welcome_message',$data);
        }
    }
}