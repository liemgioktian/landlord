<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Page extends CI_Controller {

    public function index()
    {
        $this->load->model(array('events','listings'));
        $this->load->library('Landlordlib');
        
        $data['events'] = $this->events->announce();
        $data['published'] = $this->listings->countPublished();
        $data['dijual'] = $this->listings->slideShow();
        $data['disewakan'] = $this->listings->slideShow('sewa');
        $data['noBanner'] = true;
        $data['page'] = 'home';
        $this->load->view('welcome_message', $data);
    }

    public function about()
    {
        $data['title'] = 'About us';
        $data['page'] = 'about';
        $this->load->view('welcome_message', $data);
    }

    public function contact()
    {
        $data['noBanner'] = true;
        $data['page'] = 'contact';
        $this->load->view('welcome_message', $data);
    }

    public function team(){
        $data['title']      = 'Landlord Indonesia Marketing Team';
        $data['sub_title']  = '';
        $data['page']       = 'team';
        $this->load->model('users');
        $this->load->library('landlordlib');
        $data['items']  = $this->users->getAgenList();
        $this->load->view('welcome_message',$data);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */