<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller {

    protected $visitor_role = null;

    public function __construct() {
        parent::__construct();
        $this->load->model('users');
        if ($this->session->userdata('role'))
            $this->visitor_role = $this->session->userdata('role');
    }

    public function index() {
        if ($this->visitor_role < 1)
            show_error(UNAUTHORIZED, 401);
        if ($this->input->get())
            $this->delete($this->input->get('delete'));
        
        $this->load->model('listings');
        $data['years'] = $this->listings->getYears();
        $data['title'] = 'DAFTAR AGEN';
        $data['page'] = 'userlist';
        $this->load->view('welcome_message', $data);
    }

    function datatable() {
        $this->load->library('datatables');
        $exp = EXPIRED_DAYS;

        if ($this->visitor_role < 1)
            $this->datatables->where('id', 0);
        echo $this->datatables
                #->select("IF(role=1,'ADMINISTRATOR','AGEN') as role",false)
                ->select('nama')
                ->select("DATE_FORMAT(last_login,'%d %b %Y %H:%i:%s') as last_login", false)
                ->select("(SELECT count(id) FROM p_property WHERE agen=s_login.id AND first_publish IS NULL) as baru", false)
                ->select("(SELECT count(id) FROM p_property WHERE agen=s_login.id AND TIMESTAMPDIFF(DAY, published, CURDATE()) < $exp AND sold IS NULL) as active_listing", false)
                ->select("(SELECT count(id) FROM p_property WHERE agen=s_login.id AND TIMESTAMPDIFF(DAY, published, CURDATE()) >= $exp) as expired", false)
                ->select("(SELECT count(id) FROM p_property WHERE agen=s_login.id AND sold IS NOT NULL) as terjual", false)
                ->select("(SELECT CONCAT('Rp ',FORMAT(SUM(harga_terjual),0)) FROM p_property WHERE agen=s_login.id) as sold_listing", false)
                ->select('s_login.id')
                ->edit_column('baru', '<span style="float:right">$1 Unit</span>', 'baru')
                ->edit_column('active_listing', '<span style="float:right">$1 Unit</span>', 'active_listing')
                ->edit_column('expired', '<span style="float:right">$1 Unit</span>', 'expired')
                ->edit_column('terjual', '<span style="float:right">$1 Unit</span>', 'terjual')
                ->edit_column('sold_listing', '<span style="float:right">$1</span>', 'sold_listing')
                ->edit_column(
                        's_login.id', '<div class="btn-group">' .
                        '<a class="btn" href="' . site_url('user/profile') . '/$1">DETAIL</a>' .
                        '<a class="btn" href="javascript:llconfirm(\'' . site_url('user?delete=') . '$1\',true)">HAPUS</button>' .
                        '</div>', 's_login.id'
                )
                ->from('s_login')
                ->where('role <>', 1)
                ->join('s_marketing', 's_login.marketing_id=s_marketing.id', 'left')
                ->generate();
    }

    function profile($id = null) {
        if ($this->input->post()) {
            $post = $this->input->post();
            if($post['login']['password']!='') $post['login']['password'] = md5($post['login']['password']);
            unset($post['login']['cpassword']);
            if(null==$id) $this->store($post);
            else $this->update($post, $id);
        }

        if (null == $id) {
            if ($this->visitor_role < 1)
                show_error(UNAUTHORIZED, 401);
        }else {
            if ($this->visitor_role < 1)
                if ($this->session->userdata('id') != $id)
                    show_error(UNAUTHORIZED, 401);
            $user = $this->users->select(array('id' => $id), false);
            $agen = $this->users->getAgen($id);
            unset($agen['id']);
            $data['user_edit'] = array_merge($user, $agen);
            $this->load->library('landlordlib');
        }

        $data['title'] = 'MARKETING';
        $data['sub_title'] = null != $id ? $agen['nama'] : 'Tambah Agen Baru';
        $data['page'] = 'profile';
        $this->load->view('welcome_message', $data);
    }

    function msgList($id) {
        $data['agen_message'] = $this->users->getMsg($id);
        $this->load->view('parts/messages', $data);
    }

    function rmMSG($mid) {
        $this->users->deleteMSG($mid);
    }

    private function store($data) {
        $userId = $this->users->insert($data);
        return true;
    }

    private function update($data, $id) {
        $data['login']['id'] = $id;
        $this->users->update($data);
        return true;
    }

    private function delete($id) {
        $this->users->delete(array('id' => $id));
    }

    function sendmsg() {
        if ($this->input->post() && ($this->input->post('security_code') == $this->session->userdata('mycaptcha'))) {
            $msg['pesan'] = $this->input->post('msg');
            $msg['user_id'] = $this->input->post('userid');
            $msg['dikirim'] = date('Y-m-d H:i:s');
            $this->users->msgInsert($msg);
            echo 200;
        } else
            echo 401;
    }

    function report()
    {
        $this->load->model('listings');
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $XLSactvSheet = $this->excel->getActiveSheet();
        $XLSactvSheet->setTitle('Sheet1');

        $first_col = 'B';
        $title_row = '2';
        $last_col  = 'E';
        
        # header
        $XLSactvSheet->mergeCells("$first_col$title_row:$last_col$title_row");
        $XLSactvSheet->setCellValue($first_col.$title_row, "DAFTAR MARKETING AGENT");
        $title_style = $XLSactvSheet->getStyle($first_col.$title_row);
        $title_style->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $title_style->getFont()->setBold(true);
        
        # body
        $first_row = $title_row+1;
        foreach($this->users->getAgenList() as $ma){
            $first_row++;
            $mergefrom = $first_row;
            $XLSactvSheet->setCellValue('D'.$first_row, "Nama : $ma->nama");
            $XLSactvSheet->setCellValue('E'.$first_row, "Mobile : $ma->mobile");
            $first_row++;
            $XLSactvSheet->setCellValue('D'.$first_row, "Email : $ma->email");
            $XLSactvSheet->setCellValue('E'.$first_row, "Office : $ma->office");
            $first_row++;
            $XLSactvSheet->setCellValue('D'.$first_row, "Facebook : $ma->facebook");
            $XLSactvSheet->setCellValue('E'.$first_row, "Fax : $ma->fax");
            $first_row++;
            $XLSactvSheet->setCellValue('D'.$first_row, "Twitter : $ma->twitter");
            $mergeto = $first_row;
            $first_row++;
            
            # merge terakhir
            $XLSactvSheet->mergeCells("B$mergefrom:C$mergeto");
            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setWorksheet($XLSactvSheet);
            $objDrawing->setPath(PHOTOSDIR.$ma->photo);
            $objDrawing->setCoordinates("B$mergefrom");
            $objDrawing->setHeight(80);
//            $objDrawing->setWidth(100);
        }        
        
        $today = new DateTime();
        $filename="[LandlordIndonesia] Daftar Agen Per ".$today->format('d-m-Y');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        $objWriter->save('php://output'); 
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */