<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Auth extends CI_Controller{
    
    public function __construct(){
        parent::__construct();
        $this->load->model('users');
        session_start();
    }
    
    public function login()
    {
        $reply = array();
        $user = $this->users->select(array('username'=>  $this->input->post('username')));
        
        if(empty($user)) $reply['error'] = 'user tidak ditemukan';
        else if($user['password']!=  md5($this->input->post('password')))  $reply['error'] = 'password tidak sesuai';
        else{
            //$this->load->library('encrypt');
            //$reply['token'] = site_url('auth/login_token/'.urlencode($this->encrypt->encode($user['id'])));
//            $reply['token'] = site_url('auth/login_token/'. urlencode(base64_encode($user['id'])));
            $prefix = rand(111,999);
            $suffix = rand(111,999);
            $token = $prefix.'354'.$user['id'].'354'.$suffix;
            $reply['token'] = site_url('auth/login_token/'.$token);
            $last_login = $user;
            $last_login['last_login'] = date('Y-m-d H:i:s');
//            die(var_dump($last_login));
            $this->users->simpleUpdate($last_login);
        
            /* session untuk kcfinder */
                    $_SESSION['ses_kcfinder'] = array();
                    $_SESSION['ses_kcfinder']['disabled'] = false;
                    $_SESSION['ses_kcfinder']['uploadURL'] = base_url()."assets/images";
        }
        
        exit(json_encode($reply));
    }
    
    public function login_token($token)
    {
        //$this->load->library('encrypt');
        //$user = $this->users->select(array('id'=> \urldecode($this->encrypt->decode($token))));
//        $user = $this->users->select(array('id'=> urldecode(base64_decode($token))));
        $token = explode('354', $token);
        $user = $this->users->select(array('id'=> $token[1]));
        if(!empty($user)) $this->session->set_userdata($user);
        redirect(site_url());
    }
    
    public function logout()
    {
        session_destroy();
        $this->session->sess_destroy();
        redirect(site_url());
    }
    
    public function forgot()
    {
        $email = $this->input->post('email');
        $user = $this->users->select(array('email'=>$email),false);
        if(!$user) $reply['error'] = 'email tidak terdaftar';
        else{
            $this->load->helper('string');
            $new_password = random_string('alnum', 16);
            $user['password'] = md5($new_password);
            $this->users->update_login($user);
            
//            further email setting http://ellislab.com/codeigniter/user-guide/libraries/email.html
            $this->load->library('email');
            $this->email->from('auth@landlordindonesia.com', 'Admin LandlordIndonesia');
            $this->email->to($email);
            $this->email->subject('Password Recovery');
            $this->email->message("Username:".$user['username'].", Password:$new_password");
            $reply['error'] = $this->email->send()?
                    "email terkirim ke $email, periksa inbox dan spam box anda":
                    "pengiriman email gagal, silakan hubungi kami";
//            die($this->email->print_debugger()); debugger
        }
        exit(json_encode($reply));
    }
}