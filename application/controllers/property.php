<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Property extends CI_Controller {

    protected $loggedin = false;
    protected $isadmin = false;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('properties');
        $this->load->library('landlordlib');
        $this->loggedin = ($this->session->userdata('id'));
        if($this->loggedin) $this->isadmin = ($this->session->userdata('role')==1);
        date_default_timezone_set("Asia/Jakarta");
    }

    function index()# form insert
    {
        if (!$this->loggedin) show_error (UNAUTHORIZED, 401);
        $data['fiturs'] = $this->properties->getFeatures();
        $data['title'] = 'Detail Property';
        $data['sub_title'] = 'Daftarkan Property Baru';
        $data['page'] = 'formProperty';
        if($this->isadmin){ 
            $this->load->model('users');
            $data['agens'] = $this->users->getAgenList();
        }
        $this->load->view('welcome_message', $data);
    }

    function show($pid,$alias=null) #url-alias
    {
        $data['item'] = $this->properties->selectComplete($pid);
        
        # permission
        if(null==$data['item']['published'] || $this->properties->isExpired($data['item'])) 
            if(!$this->loggedin)show_404 ();
            else if($this->session->userdata('id')!=$data['item']['agen'] && !$this->isadmin )show_404 ();
            
        $data['photos'] = $this->properties->getPhotos(array('property_id'=>$pid),true);
        $this->load->model('listings');
        $data['serupa'] = $this->listings->related($data['item']);
        $data['title'] = 'Property';
        $data['sub_title'] = $this->landlordlib->getAlias($data['item']);
        #$data['linkToEdit'] = ($this->isadmin || $this->loggedin==$data['item']['agen']);
        $data['linkToEdit'] = ($this->isadmin && null==$data['item']['sold']);
        
        $data['address'] = $data['item']['lokasi'].' '.$data['item']['kab_kota'].' '.$data['item']['provinsi'];
        $data['address'] = str_replace(array("\r", "\n"), "", $data['address']);
        $data['mapActive'] = false;
        
        $data['page'] = 'singleProperty';
        
        $this->load->helper('captcha');
        $vals = array(
            'img_path'	 => './captcha/',
            'img_url'	 => base_url().'captcha/',
            'img_width'	 => '125',
            'img_height' => 30,
            'border' => 0, 
            'expiration' => 7200
        );
        $cap = create_captcha($vals);
        $data['image'] = $cap['image'];
        $this->session->set_userdata('mycaptcha', $cap['word']);
        
        $this->load->view('welcome_message', $data);
        $this->properties->dilihat($pid);
    }

    function store($addMore='addmore')
    {
        $this->properties->insert($this->input->post());
        if($addMore=='addmore') redirect(site_url('property'));
        else if($this->isadmin) redirect(site_url('listing/admin'));
        else redirect(site_url('listing'));
    }

    function setPublicity($id)
    {
        $property = $this->properties->select(array('id'=>$id));
        if ($this->isadmin || $this->loggedin==$property['agen']){
            if(null==$property['published'] && !$this->isadmin) show_error (UNAUTHORIZED, 401);
            $this->properties->publicity($property);
        }else show_error (UNAUTHORIZED, 401);
    }
    
    function sold($id)
    {
        $post = $this->input->post();
        $property = $this->properties->select(array('id'=>$id));
        $property['sold'] = date('Y-m-d',  strtotime($post['sold']));
        $property['harga_terjual'] = str_replace('.', '', $post['harga_terjual']);
        if ($this->isadmin || $this->loggedin==$property['agen'])
            $this->properties->sold($property);
        else show_error (UNAUTHORIZED, 401);
    }
    
    function edit($id)
    {
        $data['property'] = $this->properties->selectComplete($id);
        $data['property']['form-action'] = site_url('property/update');
        if ($this->isadmin || $this->loggedin==$data['property']['agen']){
            $data['fiturs'] = $this->properties->getFeatures();
            $data['title'] = 'Detail Property';
            $data['sub_title'] = 'Edit Detail Property';
            $data['page'] = 'formProperty';
            if($this->isadmin){ 
                $this->load->model('users');
                $data['agens'] = $this->users->getAgenList();
            }
            $this->load->view('welcome_message', $data);
        }else show_error (UNAUTHORIZED, 401);
    }

    function update()
    {
        $prop = $this->input->post();
        $this->properties->update($prop);
        redirect(site_url('property/edit/'.$prop['id']));
    }

    function delete($id)
    {
        if($this->isadmin)
            $this->properties->recyclebin(array('id'=>$id));
        redirect(site_url('listing/admin'));
    }
    
    function restore($id){
        if($this->isadmin)
            $this->properties->restore(array('id'=>$id));
        redirect(site_url('listing/recyclebin'));
    }
    
    function delete_permanent($id)
    {
        if($this->isadmin)
            $this->properties->delete(array('id'=>$id));
        redirect(site_url('listing/recyclebin'));
    }
    
    function deleteImg($imgId)
    {
        $imgRecord = $this->properties->getPhotos(array('id'=>$imgId),false);
        $prop = $this->properties->selectComplete($imgRecord['property_id']);
        if($this->isadmin || $this->loggedin==$prop['agen']){
            $this->properties->removePhoto($imgRecord);
            redirect($this->landlordlib->gotoPropDetail($prop));
        }else show_error(UNAUTHORIZED, 401);
    }
    
    function feature()
    {
        if($this->input->post()){
            $this->properties->addRmvFitur($this->input->post());
        }else if($this->input->get()){
            if($this->input->get('id')){
                $this->properties->addRmvFitur($this->input->get());
            }else{
                $this->load->library('datatables');
                if (!$this->isadmin) $this->datatables->where('id',0);
                echo $this->datatables
                        ->select('fitur')
                        ->select('id')
                        ->edit_column('fitur','<a href="javascript:llconfirm(\''.  site_url('property/feature?id=').'$2\',true)"><i class="icon icon-trash"></i></a> $1','fitur, id')
                        ->unset_column('id')
                        ->from('p_fitur')
                        ->order_by('id','DESC')
                        ->generate();
            }
        }else{
            if (!$this->isadmin) show_error(UNAUTHORIZED, 401);
            $data['title'] = 'Daftar Feature Property';
            $data['page'] = 'featurelist';
            $this->load->view('welcome_message', $data);
        }
    }

    function autoKota()
    {
        $kotas = $this->properties->getKotas($_GET['term']);
        echo json_encode($kotas);
    }

    function autoOwner()
    {
        $owners = $this->properties->getOwners($_GET['term']);
        echo json_encode($owners);
    }
 
    function sethot($id)
    {
        $this->properties->sethot($id);
        redirect(site_url('listing/admin'));
    }
}