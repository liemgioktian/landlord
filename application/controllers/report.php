<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Report extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('users');
        if ($this->session->userdata('role'))
            $this->visitor_role = $this->session->userdata('role');
    }

    public function penjualan() {
        if ($this->input->get()) {
            $this->load->library('datatables');
            if ($this->visitor_role < 1)
                $this->datatables->where('id', 0);
            if ($this->input->get('since') != '')
                $where['sold >='] = date('Y-m-d', strtotime($this->input->get('since')));
            if ($this->input->get('until') != '')
                $where['sold <='] = date('Y-m-d', strtotime($this->input->get('until')));
            if(isset($where)) $this->datatables->where($where);
            $aaData = $this->datatables
                    ->select("DATE_FORMAT(sold,'%d %b %Y') as sold", false)
                    ->select("CONCAT(COUNT(id),' UNIT') as unit", false)
                    ->select("CONCAT('Rp ',FORMAT(harga_terjual,0)) as terjual", false)
                    ->from('p_property')
                    ->where('sold IS NOT NULL', null, false)
                    ->group_by('sold')
                    ->generate();
            $aaData = json_decode($aaData);
            if(isset($where)) $this->db->where($where);
            $total = $this->db
                    ->select("CONCAT('Rp ',FORMAT(SUM(harga_terjual),0)) as totalamount", false)
                    ->select("CONCAT(COUNT(id),' UNIT') as totalunit", false)
                            ->from('p_property')->where('sold IS NOT NULL', '', false)->get()->row_array();
            $aaData->totalamount = $total['totalamount'];
            $aaData->totalunit = $total['totalunit'];
            $aaData = json_encode($aaData);
            echo $aaData;
        }else {
            if ($this->visitor_role < 1)
                show_error(UNAUTHORIZED, 401);
            $data['title'] = 'LAPORAN PENJUALAN';
            $data['page'] = 'penjualan';
            $this->load->model('listings');
            $data['monthly'] = $this->listings->monthly();
            $data['totals'] = $this->listings->monthly('get_total');
            $this->load->view('welcome_message', $data);
        }
    }

    function agen($agen_id) {
        $this->load->library('datatables');
        $where['agen'] = $agen_id;
        if ($this->input->get('since') != '')
            $where['sold >='] = date('Y-m-d', strtotime($this->input->get('since')));
        if ($this->input->get('until') != '')
            $where['sold <='] = date('Y-m-d', strtotime($this->input->get('until')));
        $aaData = $this->datatables
                ->select("CONCAT(jenis,' ', bentuk,' di ',lokasi) as nama", false)
                ->select("DATE_FORMAT(first_publish,'%d %b %Y %H:%i:%s') as publish", false)
                ->select("DATE_FORMAT(sold,'%d %b %Y %H:%i:%s') as sold", false)
                ->select("CONCAT('Rp ',FORMAT(harga_terjual,0)) as terjual", false)
                ->from('p_property')
                ->where('sold IS NOT NULL', '', false)
                ->where($where)
                ->generate();
        $aaData = json_decode($aaData);
        $total = $this->db->select("CONCAT('Rp ',FORMAT(SUM(harga_terjual),0)) as total", false)
                        ->from('p_property')->where('sold IS NOT NULL', '', false)->where($where)->get()->row_array();
        $aaData->total = $total['total'];
        $aaData = json_encode($aaData);
        echo $aaData;
    }

    function daily() {
        
    }

    function monthly() {
        
    }

    function rekap_ma()
    {
        $tahun = $this->input->post('tahun');
        
        $this->load->model('listings');
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $XLSactvSheet = $this->excel->getActiveSheet();
        $XLSactvSheet->setTitle('Sheet1');
        
        # header
        $borders = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
        $report_header = array('NO.','NAMA','BULAN','TANGGAL');
        foreach($this->listings->getTypes() as $jenis) $report_header[] = strtoupper($jenis->jenis);
        foreach(array('OR','TOTAL','SUCCESS FEE','KETERANGAN') as $after) $report_header[] = $after;
        
        $first_col = 'A';
        $title_row = '2';
        $header_row= '3';
        $last_col = $first_col; for($col=1;$col<count($report_header);$col++) $last_col++;
        
        $XLSactvSheet->mergeCells("$first_col$title_row:$last_col$title_row");
        $XLSactvSheet->setCellValue($first_col.$title_row, "SELLING REPORT $tahun");
        $title_style = $XLSactvSheet->getStyle($first_col.$title_row);
        $title_style->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $title_style->getFont()->setBold(true);
        $XLSactvSheet->getStyle("$first_col$title_row:$last_col$title_row")->applyFromArray($borders);
        
        $col_loop = $first_col;
        foreach($report_header as $rh){
            $XLSactvSheet->setCellValue($col_loop.$header_row, $rh);
            $header_style = $XLSactvSheet->getStyle($col_loop.$header_row);
            $header_style->applyFromArray($borders);
            $header_style->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $header_style->getFont()->setBold(true);
            $col_loop++;
        }

        # body 
        $records = $this->listings->report_ma($tahun);
        $rownum = $header_row+1;
        $number = 1;
        $align_center = array('A','C','D');
        foreach($records as $record){
            $colnum = $first_col;
            array_unshift($record, $number);
            for($i=0;$i<4;$i++) array_push($record, '');// insert empty cells
            $number++;
            foreach($record as $cell){
                $XLSactvSheet->setCellValue($colnum.$rownum,$cell);
                $body_style = $XLSactvSheet->getStyle($colnum.$rownum);
                $body_style->applyFromArray($borders);
                $body_style->getAlignment()
                        ->setHorizontal(in_array($colnum, $align_center)?
                                PHPExcel_Style_Alignment::HORIZONTAL_CENTER:
                                PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $colnum++;
            }
            $rownum++;
        }
        
        $today = new DateTime();
        $filename="[LandlordIndonesia] Rekap Agen Per ".$today->format('d-m-Y');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        $objWriter->save('php://output'); 
    }
}