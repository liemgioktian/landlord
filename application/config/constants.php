<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('BOOTSTRAPCDN', 'http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.1');
define('PENAWARAN', serialize(array('jual', 'sewa', 'jual_sewa')));
define('JENIS', serialize(array('rumah','apartemen','ruko','gudang','tanah','gedung')));
define('KONDISI', serialize(array('istimewa', 'bagus', 'butuh renovasi', 'sudah renovasi')));
define('PHOTOSDIR', "./photos/");
define('KISARAN', serialize(array('1000000-2000000','2000000-3000000')));
define('LANTAI', serialize(array(1, 1.5, 2, 2.5, 3, 3.5, 4.1)));
define('AIR', serialize(array('PAM/PDAM', 'Sumur', 'Artetis', 'PAM + Sumur', 'PAM + Artetis')));
define('EXPIRED_DAYS',183); # 6 bulan
define('EXPIRED_WARNING',30); # 30 hari
define('LIMIT_RELATED',5); # related property di sidebar di detail property
define('UNAUTHORIZED', 'Silakan Login Terlebih Dahulu');
define('EVENT_ANNOUNCE',7); # 7 hari
define('TOP_LISTING_COUNT',6); # max listing yg tampil di top listing

/* 
status property ada lima
    baru: posted !published yet: first_published is null
    active: tampil d listing
            - first_publish is not null
            - publish is not null
            - sold is null
            - publish + expired < now()
     sold: terjual: sold is not null
     almost expired : warning: publish + expired > today - expired_warning
     expired : publish + expired >= now()
 */

/* End of file constants.php */
/* Location: ./application/config/constants.php */