<!DOCTYPE html>
<html lang="en">
    <head>
        <?php
            $userlogin = $this->session->all_userdata();
        ?>
        <meta charset="utf-8">
        <title>Landlord Property Industry</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <?php $bs_source = (ENVIRONMENT=='production')?BOOTSTRAPCDN:base_url('themes') ?>
        <link id="callCss" rel="stylesheet" href="<?= base_url('themes/current/bootstrap.min.css') ?>" type="text/css" media="screen"/>
        <link href="<?= $bs_source ?>/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css">
        <link href="<?= (ENVIRONMENT=='production')?'http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css':  base_url('themes/css/font-awesome.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url('themes/css/base.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url('assets/css/landlordindonesia.css') ?>" rel="stylesheet" type="text/css">
        <style type="text/css" id="enject"></style>
        <script src="<?= (ENVIRONMENT=='production')?'http://code.jquery.com/jquery-1.8.3.min.js':base_url('themes/js/jquery-1.8.3.min.js') ?>"></script>
        <script src="<?= $bs_source ?>/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= base_url('assets/js/bootstrap-confirmation.js') ?>"></script>
        <script type="text/javascript">
            var $site_url = '<?= site_url() ?>';
            var $table = null;
        </script>
    </head>
    <body>
        
        <?php include APPPATH.'views/parts/top.php'; ?>
        
        <?php if(!isset($noBanner))include APPPATH."views/parts/banner.php"; ?>
        <?php include APPPATH."views/pages/$page.php"; ?>
        
        <?php include APPPATH.'views/parts/bottom.php'; ?>
        <?php include APPPATH.'views/parts/confirm.php'; ?>
        <a href="javascript:;" class="btn" style="position: fixed; bottom: 38px; right: 10px; display: none; " id="toTop"> <i class="icon-arrow-up"></i> Go to top</a>

        <?php if(ENVIRONMENT=='production'): ?><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script><?php endif; ?>
        <script src="<?= base_url('assets/js/landlordindonesia.js') ?>"></script>
        <script src="<?= base_url('themes/js/business_ltd_1.0.js') ?>"></script>
        
    </body>
</html>