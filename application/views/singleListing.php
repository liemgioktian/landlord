<?php // echo $this->db->last_query() ?>
<?php foreach($items as $item): ?>
<?php $alias = $this->landlordlib->getAlias($item); ?>
<li class="span3">
    <div class="thumbnail">
        <div class="blockDtl">
            <a href="<?= $this->landlordlib->gotoPropDetail($item) ?>">
                <h5><?= $alias."<br/>$item->kab_kota" ?></h5>
                <img src="<?= $this->landlordlib->imgSrc($item->photo) ?>" />
            </a>
            <!--<p><?= "$item->nama ($item->mobile)" ?></p>-->
            <p>
                <?= $item->k_tidur>0?"$item->k_tidur Kamar Tidur, ":'' ?>
                <?= $item->k_mandi>0?"$item->k_mandi Kamar Mandi, ":'' ?>
            <?php if($item->harga_sewa>0) echo "Harga Sewa ".$this->landlordlib->toRp($item->harga_sewa)."/tahun" ?>
            <?php if($item->harga_jual>0) echo ", Harga Jual ".$this->landlordlib->toRp($item->harga_jual) ?></p>
            <p>
                telah dikunjungi <?= $item->dilihat ?> kali,
                <br/><?= null!=$item->published?"sejak diunggah ".date('d M Y',  strtotime($item->published)):""; ?>
                <?php 
                    if(isset($item->usia)){
                         $timeToexp = EXPIRED_DAYS - $item->usia;
                         $pstat = '';
                         if(null==$item->published){ $pstat = "UNPUBLISHED";}
                         if(null!=$item->published && $item->usia >= EXPIRED_DAYS){ $pstat =  "EXPIRED";}
                         if($timeToexp>0 && $timeToexp < EXPIRED_WARNING){ $pstat =  " EXPIRED IN $timeToexp DAY(S)";}
                         $pstat .= null!=$item->sold?', SOLD':'';
                         echo "<br/><b>$pstat</b>";
                         if($item->agen==$this->session->userdata('id') || $this->session->userdata('role')==1){
                            if(null==$item->sold){
                                echo '<a href="javascript:markAsSold('.$item->id.')">[MARK AS SOLD]</a>';
                                if($pstat=='EXPIRED')
                                    echo '<br/><a href="javascript:agentRepublishProperty('.$item->id.')">[REPUBLISH]</a>';
                            }
                         }
                    }
                    # trace expire calculation
//                    $publish = new DateTime($item->published);
//                    $exp = clone($publish);
//                    $expDate = EXPIRED_DAYS;
//                    $exp->modify("+$expDate day");
//                    echo "<br/>publish: ".$publish->format('d-m-Y');
//                    echo "<br/>expired: ".$exp->format('d-m-Y');
                ?>
            </p>
            <div class="btn-toolbar">
                <div class="btn-group toolTipgroup">
                    <a class="btn" href="#" data-placement="right" data-original-title="send email"><i class="icon-envelope"></i></a>
                    <a class="btn" href="#" data-placement="top" data-original-title="do you like?"><i class="icon-thumbs-up"></i></a>
                    <a class="btn" href="#" data-placement="top" data-original-title="dont like?"><i class="icon-thumbs-down"></i></a>
                    <a class="btn" href="#" data-placement="top" data-original-title="share"><i class="icon-link"></i></a>
                    <a class="btn" href="#" data-placement="left" data-original-title="browse"><i class="icon-globe"></i></a>
                </div>
            </div>
        </div>
    </div>
</li>
<?php endforeach; ?>