<section id="bodySection">		
    <div id="wrapper">
        <div class="container">	
            <div class="row" style="min-height: 400px">
                <a href="<?= site_url('listing/report') ?>" class="btn btn-large btn-info">
                    <i class="icon icon-download-alt"></i> &nbsp; Download Report Detail (Excel)
                </a>
                <table id="tbl_listing_admin" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th rowspan="2">HOT</th>
                            <th rowspan="2">PROPERTY</th>
                            <th rowspan="2">AGEN</th>
                            <th colspan="5" style="text-align: center">TANGGAL</th>
                            <th rowspan="2"></th>
                            <th rowspan="2" width="10%"></th>
                            <th rowspan="2" width="10%"></th>
                        </tr>
                        <tr>
                            <th>POSTING</th>
                            <th>ACCEPTED</th>
                            <th>LAST PUBLISH</th>
                            <th>EXPIRED</th>
                            <th>TERJUAL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="6" class="dataTables_empty">MOHON TUNGGU SEJENAK...</td>
                        </tr>
                    </tbody>
                </table>
                <a href="<?= site_url('listing/recyclebin') ?>" class="pull-right btn btn-large btn-danger">
                    <i class="icon icon-trash"></i> RECYCLE BIN
                </a>
            </div>
            <br/>
        </div>
    </div>
</section>

<link href="<?= base_url('assets/css/ui-lightness/jquery-ui-1.10.0.custom.min.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/js/jquery-ui-1.10.0.custom.min.js') ?>"></script>
<script src="<?php echo base_url('themes/js/autoNumeric-1.9.15.js') ?>"></script>
<?php include APPPATH.'views/parts/modal_sold.php'; ?>

<link rel="stylesheet" href="<?= base_url('datatables/DT_bootstrap.css') ?>" />
<script type="text/javascript" src="<?= base_url('datatables/js/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('datatables/DT_bootstrap.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $table = $('table').eq(0).dataTable({
            "iDisplayLength": 5,
            "bServerSide": true,
            "bStateSave": true,
            "sAjaxSource": '',
            "aoColumnDefs":[{"sClass": "txtalrt", "aTargets": [2,3,4,5,6]}],
            "fnDrawCallback":function(){
                $('a.ajax').click(function(e){
                    e.preventDefault();
                    $url = $(this).attr('href');
                    $.get($url,{},function(){
                        $table.fnDraw();
                    });
                });
            }
        });
    });
</script>