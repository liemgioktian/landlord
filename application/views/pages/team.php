<section id="bodySection">
    <div id="sectionTwo"> 	
        <div class="container">	
            <div class="row">
                <div class="span12">

                    <ul class="thumbnails">
                        <?php $fav_MA_qty = 2; ?>
                        <?php for ($fav = 0; $fav < $fav_MA_qty; $fav++): ?>
                            <li class="span3">
                                <div class="thumbnail">
                                    <div class="blockDtl">
                                        <h4><?= $items[$fav]->nama ?></h4>
                                        <img style="width: 260px; height: 195px;" src="<?= $this->landlordlib->imgSrc($items[$fav]->photo) ?>" />

                                        <ul style="margin-top: 20px; list-style: none">
                                            <li><b>Email</b> <?= $items[$fav]->email ?></li>
                                            <li><b>Office</b> <?= $items[$fav]->office ?></li>
                                            <li><b>Mobile</b> <?= $items[$fav]->mobile ?></li>
                                            <li><b>Fax   </b> <?= $items[$fav]->fax ?></li>
                                        </ul>

                                        <br/>
                                        <form method="POST" action="<?= site_url('listing') ?>">
                                            <input type="hidden" name="agen" value="<?= $items[$fav]->uid ?>" />
                                            <div class="btn-group">
                                                <button class="btn"><i class="icon icon-home"></i></button>
                                                <a href="<?= site_url('listing/soldby/'.$items[$fav]->uid) ?>" class="btn"><i class="icon icon-briefcase"></i></a>
                                                <?php if($items[$fav]->facebook!=''): ?>
                                                <a href="<?= $items[$fav]->facebook ?>" target="_blank" class="btn"><i class="icon-facebook-sign"></i></a>
                                                <?php endif; ?>
                                                <?php if($items[$fav]->twitter!=''): ?>
                                                <a href="<?= $items[$fav]->twitter ?>" target="_blank" class="btn"><i class="icon-twitter-sign"></i></a>
                                                <?php endif; ?>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </li>
                        <?php endfor; ?>
                    </ul>
                    <ul class="thumbnails">
                        <?php for ($fav = $fav_MA_qty; $fav < count($items); $fav++): ?>
                            <li class="span3">
                                <div class="thumbnail">
                                    <div class="blockDtl">
                                        <h4><?= $items[$fav]->nama ?></h4>
                                        <img style="width: 260px; height: 195px;" src="<?= $this->landlordlib->imgSrc($items[$fav]->photo) ?>" />

                                        <ul style="margin-top: 20px; list-style: none">
                                            <li><b>Email</b> <?= $items[$fav]->email ?></li>
                                            <li><b>Office</b> <?= $items[$fav]->office ?></li>
                                            <li><b>Mobile</b> <?= $items[$fav]->mobile ?></li>
                                            <li><b>Fax   </b> <?= $items[$fav]->fax ?></li>
                                        </ul>

                                        <br/>
                                        <form method="POST" action="<?= site_url('listing') ?>">
                                            <input type="hidden" name="agen" value="<?= $items[$fav]->uid ?>" />
                                            <div class="btn-group">
                                                <button class="btn"><i class="icon icon-home"></i></button>
                                                <a href="<?= site_url('listing/soldby/'.$items[$fav]->uid) ?>" class="btn"><i class="icon icon-briefcase"></i></a>
                                                <?php if($items[$fav]->facebook!=''): ?>
                                                <a href="<?= $items[$fav]->facebook ?>" target="_blank" class="btn"><i class="icon-facebook-sign"></i></a>
                                                <?php endif; ?>
                                                <?php if($items[$fav]->twitter!=''): ?>
                                                <a href="<?= $items[$fav]->twitter ?>" target="_blank" class="btn"><i class="icon-twitter-sign"></i></a>
                                                <?php endif; ?>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </li>
                        <?php endfor; ?>
                    </ul>


                </div>
            </div>
        </div>
    </div>
</section>