<section id="bodySection">	
    <div class="container">	
        <div class="row">
            <div class="span3">
                <div class="well well-small">
                    <h3>Selamat Datang!</h3>
                    Landlord Property Industry.<br/><br/>

                    Landlord Property Industry.<br/><strong> Landlord Property Industry.</strong>
                </div>
                <br/>
                <div class="thumbnail">
                    <a href="#" target="_blank" ><img src="<?= base_url('themes/images/lld.jpg') ?>" alt=""></a>
                    <h4>Landlord Property Industry</h4>
                    <p>Landlord Property Industry.Landlord Property Industry.Landlord Property Industry.Landlord Property Industry. </p>
                    <div class="btn-toolbar">
                        <div class="btn-group toolTipgroup">
                            <a class="btn" href="#" data-placement="right" data-original-title="send email"><i class="icon-envelope"></i></a>
                            <a class="btn" href="#" data-placement="top" data-original-title="do you like?"><i class="icon-thumbs-up"></i></a>
                            <a class="btn" href="#" data-placement="top" data-original-title="dont like?"><i class="icon-thumbs-down"></i></a>
                            <a class="btn" href="#" data-placement="top" data-original-title="share"><i class="icon-link"></i></a>
                            <a href="#" target="_blank" class="btn" data-placement="left" data-original-title="browse"><i class="icon-globe"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span9">
                <div class="well well-small" style="text-align:left">
                    <h3>Landlord Property Industry</h3>
                    <p><img src="<?= base_url('themes/images/carousel/business_website_templates_3.jpg') ?>" alt="business templates" /></p>
                    <p> 
                        Pendiri Landlord Property Industry ini adalah Ir. Heru Iswanto, arsitek alumni Fakultas Arsitektur Universitas Parahyangan Bandung 
                        angkatan tahun 1979. Dengan segudang prestasi dalam bidang property mulai dari design, perencanaan, kontraktor, developer, 
                        investor, marketing/pemasaran property. Dunia property mulai dijajaki sejak tahun 1982 bergabung di Cipadung Indah, 
                        Taman Sakura Bandung. Setelah lulus, beliau mendirikan PT. Bangun Tripatiajati, PT. Gajah Tiluwi Artha, dsb 
                        di kota asal beliau yaitu Pekalongan, dan dimulailah pembangunan real estate, pertokoan, rumah tinggal, dsb. 
                        sebagai berikut : Kapuas Endah, Villa Griya Bahagia, Duta Bahagia, Gama Asri, Gama Permai, Taman Seruni, Puri Asri, 
                        Kampung Paradise, Pasar Grosir Setono Tahap II, Baros Plaza, Pertokoan Gama Plaza, Perum Villa Jati Raya Indah (Semarang), 
                        Pertokoan Imam Bonjol, rumah tinggal perorangan dan masih banyak yang lainnya.
                        <br/>
                    </p>
                    <p> 
                        Untuk memperluas jaringan dan jangkauan proyek maka pada tahun 2005 beliau sekeluarga hijrah ke kota Semarang hingga saat ini. 
                        Tahun 2007, beliau bergabung di bidang Marketing Property dengan membuka Century 21 Star, dilanjutkan dengan ERA Top Pluit 
                        di Jakarta sebagai Owner/CEO. Dimulailah pengembangan dalam bidang investasi, developer, marketing property di kota Semarang. 
                        Pertokoan-pertokoan di Setiabudi 84 (8 unit), Setiabudi 152 (15 unit), Kedung Mundu, D.I Panjaitan, Erlangga, Lampersari, 
                        Sultan Agung, Brigjend Katamso, Dr. Wahidin, Dr. Sutomo, Kondotel & Apartemen Best Western Star. 
                        Proyek berikutnya : Jl. Diponegoro (Ruko Siranda), Ruko Gajahmada, Jl. S. Parman (Ruko & Heritage Hotel), Office Tower (Simatupang Jakarta). 
                        <br/>
                    </p>
                    <p> 
                        Selanjutnya Maret tahun 2012 untuk meningkatkan dan memperluas bidang usahanya maka Bapak Ir. Heru Siswanto membuka/mendirikan 
                        lahan usaha baru yaitu Lanlord Property Industry yang bergerak dalam bidang jual beli, sewa, investasi, developer property. 
                        Pemikiran ini pun didasarkan atas pengalaman dan prestasi dalam bidang properti yang didukung oleh Staf Ahli, Konsultan Property, 
                        Marketing yang profesional, maka Landlord Property Industry pun siap beroperasi sesuai bidang usahanya dan juga melayani 
                        klien/konsumen yang berminat berinvestasi di dunia properti. 
                        <br/>
                    </p>
                    <p> 
                        Sebagai Ilustarasi, Bapak Ir. Heru Siswanto adalah pengamat properti handal yang dapat disejajarkan dengan konsultan-konsultan 
                        Properti Nasional. Setiap Investasi/development properti yang digelutinya akan mempunyai nilai jual (capital gain) yang cepat 
                        melesat, sehingga ROI yang akan dicapai per tahun bisa mencapai hampir 50%.

                        Bagi anda yang berminat bergabung, kami menyediakan sarana Waralaba (Franchise) dengan harga kompetitif yang dijamin akan memberi 
                        hasil yang profitable.
                        <br/>
                    </p>
                    <div class="btn-toolbar">
                        <div class="btn-group toolTipgroup">
                            <a class="btn" href="#" data-placement="top" data-original-title="send email"><i class="icon-envelope"></i></a>
                            <a class="btn" href="#" data-placement="top" data-original-title="do you like?"><i class="icon-thumbs-up"></i></a>
                            <a class="btn" href="#" data-placement="top" data-original-title="dont like?"><i class="icon-thumbs-down"></i></a>
                            <a class="btn" href="#" data-placement="top" data-original-title="share"><i class="icon-link"></i></a>
                            <a class="btn" href="#" data-placement="top" data-original-title="print"><i class="icon-print"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>