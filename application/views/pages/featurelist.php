<section id="bodySection">		
    <div id="wrapper">
        <div class="container">	
            <div class="row" style="min-height: 400px">
                <div class="span8 offset2">
                <div class="input-append">
                    <input type="text" id="newFeature" />
                    <a href="javascript:addFeature();" class="add-on"><i class="icon icon-plus"></i></a>
                </div>
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>FEATURE PROPERTY</th>
<!--                            <th width="15%"></th>-->
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="1" class="dataTables_empty">MOHON TUNGGU SEJENAK...</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            </div>
            <br/>
        </div>
    </div>
</section>
<link rel="stylesheet" href="<?= base_url('datatables/DT_bootstrap.css') ?>" />
<script type="text/javascript" src="<?= base_url('datatables/js/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('datatables/DT_bootstrap.js') ?>"></script>
<script type="text/javascript">
    function addFeature()
    {
        var $input = $('#newFeature');
        $.post('',{fitur:$input.val()},function(){
            $input.val('');
            $table.fnDraw();
        });
    }
    
    $(document).ready(function() {
        $table = $('table').eq(0).dataTable({
            "iDisplayLength": 5,
            "bServerSide": true,
            sAjaxSource: '<?= site_url('property/feature') ?>',
        });
    });
</script>