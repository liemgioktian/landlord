<section id="bodySection">		
    <div id="wrapper">
        <div class="container">	
            <div class="row" style="min-height: 400px">
 
                
                <div class="span12">
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>PROPERTY</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="2" class="dataTables_empty">MOHON TUNGGU SEJENAK...</td>
                        </tr>
                    </tbody>
                </table>
                </div>
                
            </div>
            <br/>
        </div>
    </div>
</section>
<link rel="stylesheet" href="<?= base_url('datatables/DT_bootstrap.css') ?>" />
<script type="text/javascript" src="<?= base_url('datatables/js/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('datatables/DT_bootstrap.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $table = $('table').eq(0).dataTable({
            "iDisplayLength": 5,
            "bServerSide": true,
            "sAjaxSource": '<?= site_url('listing/recyclebin') ?>',
        });
    });
</script>