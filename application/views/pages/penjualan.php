<section id="bodySection">		
    <div id="wrapper">
        <div class="container">	
            <div class="row" style="min-height: 400px">
                <div class="well">

                    <div id="tglcalc">
                        <input type="text" placeholder="Sejak Awal" id="since" class="date" />
                        <input type="text" placeholder="Hingga Sekarang" id="until" class="date" />
                        <button class="btn" onclick="$('.date').val('');$table.fnDraw();" >reset</button>
                        <button class="btn" onclick="$table.fnDraw();" >hitung</button>
                    </div>

                    <table id="daily" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>TANGGAL</th>
                                <th>TOTAL UNIT</th>
                                <th>TOTAL PENJUALAN</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="3" class="dataTables_empty">MOHON TUNGGU SEJENAK...</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>TOTAL</th>
                                <th id="totalunit"></th>
                                <th id="totalamount"></th>
                            </tr>
                        </tfoot>
                    </table>

                </div>
                <div class="well">
                    <!--
                    <select class="input-small">
                    <?php for ($month = 1; $month <= 12; $month++): ?>
                                    <option value="<?= $month ?>"><?= $month ?></option>
                    <?php endfor; ?>
                    </select>
                    <select class="input-small">
                    <?php for ($year = 2014; $year <= 2017; $year++): ?>
                                    <option value="<?= $year ?>"><?= $year ?></option>
                    <?php endfor; ?>
                    </select>
                    -->
                    <table id="monthly" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>BULAN</th>
                                <th>TAHUN</th>
                                <th>TOTAL UNIT</th>
                                <th>TOTAL PENJUALAN</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($monthly as $data): ?>
                                <tr>
                                    <td><?= $data->month ?></td>
                                    <td><?= $data->year ?></td>
                                    <td><?= $data->unit ?></td>
                                    <td><?= $data->amount ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="2">TOTAL</th>
                                <th><?= $totals['unit'] ?></th>
                                <th><?= $totals['amount'] ?></th>
                            </tr>
                        </tfoot>
                    </table>

                </div>
            </div>
            <br/>
        </div>
    </div>
</section>
<link href="<?= base_url('assets/css/ui-lightness/jquery-ui-1.10.0.custom.min.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/js/jquery-ui-1.10.0.custom.min.js') ?>"></script>
<link rel="stylesheet" href="<?= base_url('datatables/DT_bootstrap.css') ?>" />
<script type="text/javascript" src="<?= base_url('datatables/js/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('datatables/DT_bootstrap.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
//        $('.date').datepicker({dateFormat: 'dd-mm-yy'});
        $table = $('table').eq(0).dataTable({
            "iDisplayLength": 5,
            "bServerSide": true,
            "sAjaxSource": '',
            "fnServerData": function(sSource, aoData, fnCallback) {
                aoData.push({"name": "since", "value": $('#since').val()}, {"name": "until", "value": $('#until').val()});
                $.getJSON(sSource, aoData, function(json) {
                    $('#totalunit').html(json.totalunit);
                    $('#totalamount').html(json.totalamount);
                    fnCallback(json);
                });
            }
        });
    });
</script>