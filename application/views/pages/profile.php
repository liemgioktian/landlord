<section id="bodySection">
    <div id="sectionTwo"> 	
        <div class="container">	
            <div class="row">
                <div class="span9">
                    <form class="well form-horizontal" name="frm-usr" action="" method="POST" enctype="multipart/form-data" >
                        <fieldset>
                            <legend>PROFIL MARKETING</legend>
                            <div class="row-fluid">
                                <div class="span5">

                                    <div class="control-group">
                                        <label class="control-label" for="agen[nama]">nama</label>
                                        <div class="controls">
                                            <input type="text" class="input-large" name="agen[nama]" />
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="agen[office]">office</label>
                                        <div class="controls">
                                            <input type="text" class="input-large" name="agen[office]" />
                                        </div>
                                    </div>

                                </div>
                                <div class="span5">

                                    <div class="control-group">
                                        <label class="control-label" for="agen[mobile]">mobile</label>
                                        <div class="controls">
                                            <input type="text" class="input-large" name="agen[mobile]" />
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="agen[fax]">fax</label>
                                        <div class="controls">
                                            <input type="text" class="input-large" name="agen[fax]" />
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="photo_marketing">photo</label>
                                        <div class="controls">
                                            <input type="file" class="input-large" name="photo_marketing" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>INFORMASI LOGIN</legend>
                            <div class="row-fluid">
                                <div class="span5">

                                    <div class="control-group">
                                        <label class="control-label" for="login[username]">username</label>
                                        <div class="controls">
                                            <input type="text" class="input-large" name="login[username]" />
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="login[email]">email</label>
                                        <div class="controls">
                                            <input type="text" class="input-large" name="login[email]" />
                                        </div>
                                    </div>

                                </div>
                                <div class="span5">
<div class="span5">
                                    <div class="control-group">
                                        <label class="control-label" for="login[password]">password</label>
                                        <div class="controls">
                                            <input type="password" class="input-large" name="login[password]" />
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="login[cpassword]">sekali lagi</label>
                                        <div class="controls">
                                            <input type="password" class="input-large" name="login[cpassword]" />
                                        </div>
                                    </div>
</div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>SOCIAL MEDIA</legend>
                            <div class="row-fluid">
                                <div class="span5">
                                    <div class="control-group">
                                        <label class="control-label" for="agen[facebook]">Facebook URL</label>
                                        <div class="controls">
                                            <input type="text" class="input-large" name="agen[facebook]" />
                                        </div>
                                    </div>
                                </div>
                                <div class="span5">
                                    <div class="control-group">
                                        <label class="control-label" for="agen[twitter]">Twitter URL</label>
                                        <div class="controls">
                                            <input type="text" class="input-large" name="agen[twitter]" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend></legend>
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="btn-group">
                                        <button class="btn btn-large btn-success" type="submit"><i class="icon-save"></i> simpan</button>
                                        <!--<a class="btn btn-large btn-danger" href="<?= site_url('user') ?>" ><i class="icon-arrow-left"></i> batal</a>-->
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                    </form>
                    <?php if (isset($user_edit)) include APPPATH . 'views/parts/user_report.php'; ?>
                </div>
                <?php include APPPATH . 'views/parts/sidebar_profile.php'; ?>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
<?php if (isset($user_edit)): ?>
        var useredit = <?= json_encode($user_edit) ?>;
        $('form[name="frm-usr"] input[type="text"]').each(function() {
            var $name = $(this).attr('name');
            var $split = $name.split('[');
            var $pre = $split[0];
            var $post = $split[1].replace(']', '');
            //$(this).val(useredit[$pre][$post]);
            $(this).val(useredit[$post]);
        });
<?php endif; ?>
    function preview_photo(input)
    {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#photo_agen').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $('[type="file"]').change(function() {
        preview_photo(this);
    });
</script>
