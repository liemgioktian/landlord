<section id="bodySection">
    <div id="sectionTwo"> 	
        <div class="container">	
            <div class="row">
                <div class="span9">
                    
                    <?php if(!isset($disable_order)): ?>
                    <form class="well form-inline" id="form-sorting">
                        Urutkan Listing Berdasar &nbsp;
                        <div class="input-append">
                            <select name="order" class="span2" >
                                <option value="tanggal">Tanggal</option>
                                <option value="harga">Harga</option>
                            </select>
                            <a class="btn"><i class="icon icon-chevron-down"></i></a>
                            <select name="ascdesc" class="span2 hidden">
                                <option value="asc">terkecil ke terbesar</option>
                                <option value="desc">terbesar ke terkecil</option>
                            </select>
                        </div>
                    </form>
                    <?php endif; ?>

                    <div class="row">
                        <div class="span9">
                            <ul class="thumbnails">
                                <?php //include APPPATH.'views/singleListing.php' ?>
                            </ul>
                        </div>
                        <div class="span9 loading_bar">
                            <div class="well">
                                <img src="<?= base_url('assets/loading.gif') ?>" /> 
                                Sedang Memuat . .
                            </div>
                        </div>
                    </div>
                </div>
                <?php include APPPATH . 'views/parts/sidebar_listing.php'; ?>
            </div>
        </div>
    </div>
</section>
<script src="<?= base_url('themes/js/waypoints.js') ?>"></script>
<link href="<?= base_url('assets/css/ui-lightness/jquery-ui-1.10.0.custom.min.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/js/jquery-ui-1.10.0.custom.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/jqui-autocomplete.js') ?>"></script>

<?php include APPPATH.'views/parts/modal_sold.php'; ?>