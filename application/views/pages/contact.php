<section id="mapSection"> 
    <div id="myMap" style="height:400px">
    </div>	
</section>
<section id="bodySection"> 	
    <div class="container">					
        <div class="row">
            <div class="span4">
                <h3>  Mailing Address </h3>	
                Jl. Pandanaran 50 A<br/>
                Semarang, Jawa Tengah<br/>
                office@landlordindonesia.com<br/>
                Tel (024) 86453000<br/>
                Fax (024) 86453030<br/>
            </div>
            <div class="span4">
                <h3> Opening Hours</h3>
                <h4> Monday - Friday</h4>
                08:00am - 05:00pm<br/><br/>
                <h4>Saturday</h4>
                08:00am - 04:00pm<br/><br/>
            </div>
            <div class="span4">
                <h3>  Email Us</h3>
                <form class="form-horizontal">
                    <fieldset>
                        <div class="control-group">

                            <input type="text" placeholder="name" class="input-xlarge"/>

                        </div>
                        <div class="control-group">

                            <input type="text" placeholder="email" class="input-xlarge"/>

                        </div>
                        <div class="control-group">

                            <input type="text" placeholder="subject" class="input-xlarge"/>

                        </div>
                        <div class="control-group">
                            <textarea rows="4" id="textarea" class="input-xlarge"></textarea>

                        </div>

                        <button class="btn btn-large" type="submit"> <i class="icon-envelope"></i> Send Message</button>

                    </fieldset>
                </form>
            </div>
        </div>

    </div>
</section>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="<?= base_url('themes/js/jquery.gmap.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#myMap").gMap({controls: false,
            scrollwheel: false,
            draggable: true, 
            markers: [{latitude: -6.986619, //your company location latitude 
                    longitude: 110.414498, //your company location longitude
                    icon: {image: "<?= base_url('themes/images/lld.jpg') ?>",
                        iconsize: [100, 90],
                        iconanchor: [42, 48],
                        infowindowanchor: [14, 0]}}
            ],
            icon: {image: "http://www.google.com/mapfiles/marker.png",
                iconsize: [28, 48],
                iconanchor: [14, 48],
                infowindowanchor: [14, 0]},
            latitude: -6.986619,
            longitude: 110.414498,
            zoom: 18 });
    });
</script>