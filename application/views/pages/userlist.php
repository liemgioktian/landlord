<section id="bodySection">		
    <div id="wrapper">
        <div class="container">	
            <div class="row" style="min-height: 400px">
 
                <div class="span12" style="margin-bottom: 20px">
                    <form action="<?= site_url('report/rekap_ma') ?>" class="input-append" method="POST">
                        <select name="tahun">
                            <?php foreach ($years as $year): ?>
                            <option value="<?= $year->year ?>">Agent Selling Report <?= $year->year ?></option>
                            <?php endforeach; ?>
                        </select>
                        <button class="btn btn-info">
                            <i class="icon icon-download-alt"></i> &nbsp; Download
                        </button>>
                    </form>
                </div>
                
                <div class="span12">
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th rowspan="2">NAMA LENGKAP</th>
                            <th rowspan="2">AKTIVITAS TERAKHIR</th>
                            <th colspan="4" style="text-align: center">LISTING</th>
                            <th rowspan="2">TOTAL PENJUALAN</th>
                            <th rowspan="2" width="10%"></th>
                        </tr>
                        <tr>
                            <th>BARU</th>
                            <th>AKTIF</th>
                            <th>EXPIRED</th>
                            <th>TERJUAL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="8" class="dataTables_empty">MOHON TUNGGU SEJENAK...</td>
                        </tr>
                    </tbody>
                </table>
                </div>
                <div class="span12">
                    <div class="btn-group">
                        <a href="<?= site_url('user/report') ?>" class="btn btn-large btn-primary">
                            <i class="icon icon-download-alt"></i> &nbsp; Download Daftar Agen
                        </a>
                        <a href="<?= site_url('user/profile') ?>" class="btn btn-large btn-danger">
                            <i class="icon icon-user"></i> &nbsp; Pendaftaran Agen Baru
                        </a>
                    </div>
                </div>
                
            </div>
            <br/>
        </div>
    </div>
</section>
<link rel="stylesheet" href="<?= base_url('datatables/DT_bootstrap.css') ?>" />
<script type="text/javascript" src="<?= base_url('datatables/js/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('datatables/DT_bootstrap.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $table = $('table').eq(0).dataTable({
            "iDisplayLength": 5,
            "bServerSide": true,
            "sAjaxSource": '<?= site_url('user/datatable') ?>',
        });
    });
</script>