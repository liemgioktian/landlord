
<section id="bodySection">
    <div class="container">					
        <div class="row">						
            <div class="span12" style="min-height: 400px">
                
                <?php $showForm = ($this->session->userdata('role')==1 && current_url()==site_url('event')); ?>
                <?php if($showForm): ?>
                <div class="thumbnail">
                    <br/>
                    <form action="" method="POST" class="form-vertical span6 offset3" id="frmevt" />
                        <input type="hidden" name="id" value="<?= isset($item)?$item['id']:0 ?>"/>
                        <div class="control-group">
                            <label class="control-label" for="title">title</label>
                            <div class="controls">
                                <input type="text" class="input-xxlarge" name="title"
                                       value="<?= isset($item)?$item['title']:'' ?>"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="due_date">due date</label>
                            <div class="controls">
                                <input type="text" class="date input-xlarge" name="due_date" 
                                       value="<?= isset($item)?$item['due_date']:'' ?>"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="description">description</label>
                            <div class="controls">
                                <textarea id="description" class="description" name="description" ><?= isset($item)?$item['description']:'' ?></textarea>
                            </div>
                        </div>
                        <div class="btn-group">
                            <input type="submit" value="simpan" class="btn btn-large btn-success" />
                            <a href="" class="btn btn-large btn-danger">reset</a>
                        </div>
                    </form>
                    <form class="hidden" name="edit-event" method="POST" action="">
                        <input type="hidden" id="edit_id" name="edit_id" value="0" />
                        <input type="hidden" id="del_id" name="del_id" value="0" />
                    </form>
                </div>
<link href="<?= base_url('assets/css/ui-lightness/jquery-ui-1.10.0.custom.min.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/js/jquery-ui-1.10.0.custom.min.js') ?>"></script>
<script src="<?=base_url('assets/js/editor/ckeditor/ckeditor.js')?>"></script>
<script src="<?=base_url('assets/js/bootstrap-fileupload.js')?>"></script>

<script type="text/javascript">
		var editor = CKEditor.replace('description', {
			filebrowserBrowseUrl    : '',
    filebrowserWindowWidth  : 1000,
    filebrowserWindowHeight : 500
			});
</script>
<script type="text/javascript">
    if( !('description' in CKEDITOR.instances)) {
		CKEDITOR.replace( 'description' );
    }
</script>             
                <?php endif; ?>
                    
                <?php foreach($items as $evt): ?>
                <br/>
                <div class="thumbnail">
                    <h3><a href="<?= site_url('event/show/'.$evt->id) ?>"><?= $evt->title ?></a></h3>
                    <p class="meta"><?= date("j F Y", strtotime($evt->due_date)) ?></p>
                    <span style="text-align: left; padding: 10px"><?= $evt->description ?></span>
                    
                    <?php if($showForm): ?>
                    <div class="btn-group pull-right">
                        <a class="btn btn-success" href="javascript:$('#edit_id').val(<?= $evt->id ?>).parent().submit();">
                            <i class="icon icon-pencil"></i>
                        </a>
                        <a class="btn btn-danger" href="javascript:llconfirm('<?= site_url('event?delete='.$evt->id,false) ?>');">
                            <i class="icon icon-trash"></i>
                        </a>
                    </div>
                    <?php endif; ?>
                    
                </div>
                <?php endforeach; ?>
                
                <br/>
            </div>
        </div>
    </div>
</section>

