<section id="bodySection">
    <div id="sectionTwo"> 	
        <div class="container">	
            <div class="row">
                <div class="span9">
                    <form class="well form-horizontal" action="<?= site_url('property/store') ?>" name="frmprprt"
                          method="POST" enctype="multipart/form-data" id="frmprprt"
                          onsubmit="submitProperty($(this))">
                        <fieldset>
                            <legend>
                                FORMULIR PROPERTY
                                <?php if(isset($property)): ?>
                                <a href="<?= $this->landlordlib->gotoPropDetail($property) ?>"><i class="icon icon-circle-arrow-right"></i></a>
                                <?php endif; ?>
                            </legend>
                            <div class="row-fluid">
                                <div class="span6">
                                    
                                    <input type="hidden" name="id" value="0" />

                                    <?php if($userlogin['role']==1): ?>
                                    <div class="control-group">
                                        <label class="control-label" for="agen">agen property</label>
                                        <div class="controls">
                                            <select name="agen">
                                                <?php foreach($agens as $agen): ?>
                                                <option 
                                                    value="<?= $agen->uid ?>"><?= $agen->nama ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <?php else: ?>
                                    <input type="hidden" name="agen" value="<?= $this->session->userdata('id') ?>" />
                                    <?php endif ?>
                                    
                                    <div class="control-group">
                                        <label class="control-label" for="lokasi">lokasi</label>
                                        <div class="controls">
                                            <input type="text" class="input-large" name="lokasi" />
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="kota">kota</label>
                                        <div class="controls">
                                            <div class="input-append">
                                            <input type="hidden" name="kota" />
                                            <input type="text" class="input-large" name="" />
                                            <button class="btn hidden autoc-change">change</button>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="control-group">
                                        <label class="control-label" for="jenis">jenis</label>
                                        <div class="controls">
                                            <select name="jenis">
                                                <?php foreach (unserialize(JENIS) as $jenis): ?>
                                                    <option value="<?= ucwords($jenis) ?>"><?= ucwords($jenis) ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="control-group">
                                        <label class="control-label" for="penawaran">Pilihan</label>
                                        <div class="controls">
                                            <select name="penawaran">
                                                <?php foreach (unserialize(PENAWARAN) as $penawaran): ?>
                                                <option value="<?= $penawaran ?>"><?= ucwords(str_replace('_', ' &amp; ', $penawaran)) ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="luas_tanah">luas tanah</label>
                                        <div class="controls">
                                            <div class="input-append">
                                                <input type="text" class="input-medium txtalrt" name="luas_tanah" />
                                                <span class="add-on">meter <sup>2</sup></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="luas_bangunan">luas bangunan</label>
                                        <div class="controls">
                                            <div class="input-append">
                                                <input type="text" class="input-medium txtalrt" name="luas_bangunan" />
                                                <span class="add-on">meter <sup>2</sup></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="bentuk">bentuk</label>
                                        <div class="controls">
                                            <input type="text" class="input-large" name="bentuk" />
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="hadap">mengahadap</label>
                                        <div class="controls">
                                            <input type="text" class="input-large" name="hadap" />
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="lantai">lantai</label>
                                        <div class="controls">
                                            <div class="input-append">
                                                <select name="lantai" class="input-medium">
                                                    <?php foreach (unserialize(LANTAI) as $lantai): ?>
                                                    <option value="<?= $lantai ?>"><?= $lantai === 4.1 ? ucwords('lebih dari 4') : $lantai ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <span class="add-on">tingkat</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="k_tidur">kamar tidur</label>
                                        <div class="controls">
                                            <div class="input-append">
                                                <input type="text" class="input-medium txtalrt" name="k_tidur" />
                                            <span class="add-on">kamar</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="k_mandi">kamar mandi</label>
                                        <div class="controls">
                                            <div class="input-append">
                                            <input type="text" class="input-medium txtalrt" name="k_mandi" />
                                            <span class="add-on">kamar</span>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>

                                <div class="span6">
                                    
                                    <div class="control-group">
                                        <label class="control-label" for="ac">AC</label>
                                        <div class="controls">
                                            <select name="ac">
                                                <option value="0">tidak tersedia</option>
                                                <option value="1">tersedia</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="control-group">
                                        <label class="control-label" for="telepon">telepon</label>
                                        <div class="controls">
                                            <select name="telepon">
                                                <option value="0">tidak tersedia</option>
                                                <option value="1">tersedia</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="control-group">
                                        <label class="control-label" for="air">air</label>
                                        <div class="controls">
                                            <select name="air">
                                                <?php foreach (unserialize(AIR) as $air): ?>
                                                    <option value="<?= $air ?>"><?= ucwords($air) ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="listrik">listrik</label>
                                        <div class="controls">
                                            <div class="input-append">
                                                <input type="text" class="input-medium txtalrt" name="listrik" />
                                            <span class="add-on">watt</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="harga_jual">harga jual</label>
                                        <div class="controls">
                                            <div class="input-prepend input-append">
                                                <span class="add-on">Rp</span>
                                                <input type="text" class="input-medium txtalrt" name="harga_jual">
                                                <span class="add-on">.00</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="harga_sewa">harga sewa</label>
                                        <div class="controls">
                                            <div class="input-prepend input-append">
                                                <span class="add-on">Rp</span>
                                                <input type="text" class="input-small txtalrt" name="harga_sewa">
                                                <span class="add-on"> / tahun</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="kondisi">kondisi</label>
                                        <div class="controls">
                                            <select name="kondisi">
                                                <?php foreach (unserialize(KONDISI) as $kondisi): ?>
                                                    <option value="<?= $kondisi ?>"><?= ucwords($kondisi) ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="jenis_sertifikat">jenis sertifikat</label>
                                        <div class="controls">
                                            <select name="jenis_sertifikat">
                                                <?php foreach (array('SHM (Sertifikat Hak Milik)', 'HGB (Hak Guna Bangunan)', 'HP (Hak Pakai)', 'HS (Hak Sewa)', 'Lainnya (PPJB, Girik, Adat)') as $sertifikat): ?>
                                                    <option value="<?= $sertifikat ?>"><?= $sertifikat ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="no_sertifikat">no sertifikat</label>
                                        <div class="controls">
                                            <input type="text" class="input-large" name="no_sertifikat" />
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="dokumen">dokumen</label>
                                        <div class="controls">
                                            <input type="text" class="input-large" name="dokumen" placeholder="KTP / Sertifikat dsb." />
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="keterangan">keterangan</label>
                                        <div class="controls">
                                            <input type="text" class="input-large" name="keterangan" />
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="photo[]">photo</label>
                                        <div class="controls" style="margin-left: 165px">
                                            <div class="input-append">
                                                <input type="file" style="display: none" name="photo[]" />
                                                <input type="text" class="input-medium" disabled="disabled" />
                                                <button class="btn btn-primary telusur_photo"><i class="icon-folder-open"></i></button>
                                                <button class="btn btn-info" name="tambah-photo"><i class="icon-plus-sign"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <br/>
                        <fieldset>
                            <legend>FASILITAS LAINNYA</legend>
                            <div class="row-fluid">
                                <div class="span12 form-inline">
                                    <?php foreach ($fiturs as $fitur): ?>
                                        <label><input type="checkbox" name="fitur[]" value="<?= $fitur->id ?>"> <?= $fitur->fitur ?></label>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <?php if($userlogin['role']==1): ?>
                            <br/>
                            <a class="btn btn-warning" href="<?= site_url('property/feature') ?>"><i class="icon icon-cog"></i> &nbsp; Pengaturan Feature Property</a>
                            <?php endif; ?>
                        </fieldset>
                        <br/>
                        <fieldset>
                            <legend>PEMILIK PROPERTY</legend>
                            <div class="row-fluid">
                                <div class="span6">
                                    <div class="control-group">
                                        <label class="control-label" for="pemilik[nama]">nama</label>
                                        <div class="controls">
                                            <div class="input-append">
                                            <input type="hidden" name="pemilik[id]" value="0" />
                                            <input type="text" class="input-large" name="pemilik[nama]" />
                                            <button class="btn owner-change hidden">change</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="pemilik[alamat]">alamat</label>
                                        <div class="controls">
                                            <input type="text" class="input-large" name="pemilik[alamat]" />
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="pemilik[kontak]">kontak</label>
                                        <div class="controls">
                                            <input type="text" class="input-large" name="pemilik[kontak]" placeholder="No. Tlp / Email" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend></legend>
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="btn-group">
                                        <?php if(current_url()==site_url('property')): ?>
                                        <button onclick="$('#frmprprt').attr('action','<?= site_url('property/store/noMore') ?>'); return true;" class="btn btn-large btn-danger" type="submit"><i class="icon-save"></i> simpan &amp; selesai</button>
                                        <button class="btn btn-large btn-info" type="submit"><i class="icon-save"></i> simpan &amp; tambah</button>
                                        <?php else: ?>
                                        <button class="btn btn-large btn-success" type="submit"><i class="icon-save"></i> simpan</button>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                    </form>
                </div>
                <?php include APPPATH . 'views/parts/sidebar.php'; ?>
            </div>
        </div>
    </div>
</section>
<script src="<?php echo base_url('themes/js/autoNumeric-1.9.15.js') ?>"></script>       
<link href="<?= base_url('assets/css/ui-lightness/jquery-ui-1.10.0.custom.min.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/js/jquery-ui-1.10.0.custom.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/jqui-autocomplete.js') ?>"></script>
<script type="text/javascript">
    <?php if(isset($property)): ?>
    $(document).ready(function(){
        var p = <?= json_encode($property) ?>;
        var $form = $('form[name="frmprprt"]');
        $form.find('select').each(function(){
            $(this).find('option[value="'+p[$(this).attr('name')]+'"]').attr('selected','selected');
        });
        $form.find('input').each(function(){
           $name = $(this).attr('name');
           if(p[$name]!='')
               switch($(this).attr('type')){
                   case 'text': 
                       if($(this).hasClass('txtalrt')) $(this).val(toRp(p[$name]));
                       else $(this).val(p[$name]);
                       break;
                   case 'hidden':
                       $(this).val(p[$name]);
                       switch($name){
                           case 'kota':
                               $(this).siblings('input')
                                   .attr('disabled','disabled').removeClass('input-large').addClass('input-medium')
                                   .siblings('button').removeClass('hidden');
                               //.val(p['kab_kota']);
                               break;
                       }
                       break;
               }
        });
        if(null!=p['fids']){
            fids = p['fids'].split(',');
            for($f=0;$f<fids.length;$f++) $form.find('[type="checkbox"][value="'+fids[$f]+'"]').attr('checked','checked');
        }
        $form.attr('action','<?= site_url('property/update') ?>');
    });
    <?php endif; ?>
</script>