<section id="bodySection">
    <div id="sectionTwo"> 	
        <div class="container">	
            <div class="row">
                
                <div class="span9">
                    <div class="well well-small" style="text-align:left">
                        <h4>
                            <a href="<?= $this->landlordlib->gotoPropDetail($item) ?>">
                                # <?= $this->landlordlib->getAlias($item) ?>
                            </a>
                            <?php if($linkToEdit): ?>
                            <a href="<?= site_url('property/edit/'.$item['id'])?>"><i class="icon icon-edit"></i></a>
                            <?php endif; ?>
                        </h4>
                        <div id="myCarousel" class="carousel slide blogCarousel">
                            <div class="carousel-inner">
                                <?php foreach($photos as $photo): ?>
                                <div  style="text-align:center"  class="item <?= $photo===end($photos)?'active':'' ?>">
                                    <?php $imgSrc = $this->landlordlib->imgSrc($photo->photo); ?>
                                    <a class="prop_imgs mfp-zoom" href="<?= $imgSrc ?>" style="background:url(<?= $imgSrc ?>) no-repeat center center;"></a>
                                    <?php if(isset($userlogin['id']) && $userlogin['role']==1): ?>
                                    <a href="javascript:llconfirm('<?= site_url('property/deleteImg/'.$photo->id) ?>',false)">
                                        <i class="icon icon-trash"></i> HAPUS GAMBAR <?= $photo->photo ?>
                                    </a><?php endif; ?>
                                </div>
                                <?php endforeach; ?>
                                <?php if(count($photos)<1): ?>
                                <div  style="text-align:center"  class="item active">
                                    <a href="blog_details.html" style="display:block; background:url(<?= $this->landlordlib->imgSrc(null) ?>) no-repeat center center; height:250px; max-height:250px; margin-bottom:18px"></a>
                                </div>
                                <?php endif; ?>
                            </div>
                            <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                            <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
                        </div>
                        
                        <div class="row-fluid">
                            <div class="span3">
                                <ul>
                                    <li>
                                        Jenis Property <b><?= strtoupper($item['jenis']) ?></b>
                                    </li>
                                    <li>
                                        Bentuk <b><?= strtoupper($item['bentuk']) ?></b>
                                    </li>
                                    <li>
                                        Menghadap <b><?= strtoupper($item['hadap']) ?></b>
                                    </li>
                                    <li>
                                        Kondisi <b><?= strtoupper($item['kondisi']) ?></b>
                                    </li>
                                    <li>
                                        Penawaran <b><?= strtoupper($item['penawaran']!='jual_sewa'?$item['penawaran']:'jual &amp; sewa') ?></b>
                                    </li>
                                </ul>
                            </div>
                            <div class="span3">
                                <ul>
                                    <li>
                                        Sertifikat <b><?= $item['jenis_sertifikat'] ?></b>
                                    </li>
                                    <li>
                                        Luas Tanah <b><?= $item['luas_tanah']>0?$item['luas_tanah'].'M<sup>2</sup>':'' ?></b>
                                    </li>
                                    <li>
                                        Luas Bangunan <b><?= $item['luas_bangunan']>0?$item['luas_bangunan'].'M<sup>2</sup>':'' ?></b>
                                    </li>
                                    <li>
                                        Jumlah Lantai <b><?= $item['lantai'] ?></b>
                                    </li>
                                    <li>
                                        Kamar Tidur <b><?= $item['k_tidur'] ?></b>
                                    </li>
                                    <li>
                                        Kamar Mandi <b><?= $item['k_mandi'] ?></b>
                                    </li>
                                </ul>
                            </div>
                            <div class="span3">
                                <ul>
                                    <li>
                                        AC <b><?= $item['ac']==1?'':'Tidak' ?> Tersedia</b>
                                    </li>
                                    <li>
                                        Telepon <b><?= $item['telepon']==1?'':'Tidak' ?> Tersedia</b>
                                    </li>
                                    <li>
                                        Listrik <b><?= $item['listrik']<=0?'':$item['listrik'].' Watt' ?></b>
                                    </li>
                                    <li>
                                        Air <b><?= strtoupper($item['air']) ?></b>
                                    </li>
                                </ul>
                            </div>
                            <div class="span3">
                                <ul>
                                    <li>
                                        Lokasi 
                                        <b><?= strtoupper($item['lokasi']) ?></b>
                                        <b><?= $item['kab_kota']!=''?', '.$item['kab_kota']:'' ?></b>
                                        <b><?= $item['provinsi']!=''?', '.$item['provinsi']:'' ?></b>
                                    </li>
                                    <li>
                                        Harga Jual <b><?= $item['harga_jual']<=0?'':'<br/>'.$this->landlordlib->toRp($item['harga_jual']) ?></b>
                                    </li>
                                    <li>
                                        Harga Sewa <b><?= $item['harga_sewa']<=0?'':'<br/>'.$this->landlordlib->toRp($item['harga_sewa']).' /tahun' ?></b>
                                    </li>
                                </ul>
                            </div>
                            <div class="span12">
                                <?= isset($item['fiturs'])?'<b>Fitur Lain: </b>'.str_replace(',', ', ', $item['fiturs']):'' ?>
                            </div>
                            <div class="span12">
                                <?php if(isset($item['nm_pemilik']) && ''!=$item['nm_pemilik'] ): ?>
                                    <?= '<b>PEMILIK</b> '.$item['nm_pemilik'] ?><?= isset($item['alamat'])?', '.$item['alamat']:'' ?><?= isset($item['kontak'])?', '.$item['kontak']:'' ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        
                        <?php if($mapActive) include APPPATH . 'views/parts/map.php'; ?>
                        
                    </div>
                </div>
            
                <?php include APPPATH . 'views/parts/sidebar_property.php'; ?>
                
            </div>
        </div>
    </div>
</section>

<!--IMAGE POP-UP-->
<link rel="stylesheet" href="<?= base_url('assets/css/magnific-popup.css')?>" /> 
<script type="text/javascript" src="<?= base_url('assets/js/jquery.magnific-popup.min.js')?>"></script>
<script type="text/javascript">
    $('.prop_imgs').magnificPopup({
        type:'image',
        gallery:{enabled:true}
    });
</script>