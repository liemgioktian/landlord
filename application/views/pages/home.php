<section id="carouselSection" style="text-align:center">
    <div id="myCarousel" class="carousel slide">
        <div class="carousel-inner">
            <div  style="text-align:center"  class="item active">
                <div class="wrapper"><img src="themes/images/carousel/carousel3.jpg" alt="business webebsite template">
                    <div class="carousel-caption">
                        <h2>What we do?</h2>
                        <p>Landlord Property Industry siap beroperasi sesuai bidang usahanya 
                            dan juga melayani klien/konsumen yang berminat berinvestasi di dunia properti. </p>
                        <a href="<?= site_url('listing') ?>" class="btn btn-large btn-success">Read more</a>
                    </div>
                </div>
            </div>
            <div  style="text-align:center"  class="item">
                <div class="wrapper"><img src="themes/images/carousel/carousel2.jpg" alt="business themes">
                    <div class="carousel-caption">
                        <h2>Who we are?</h2>
                        <p>Kami merupakan salah satu industri properti di kota Semarang yang bergerak dalam bidang jual beli, sewa, investasi, 
                            developer property.</p>
                        <a href="<?= site_url('page/about') ?>" class="btn btn-large btn-success">Read more</a>
                    </div>
                </div>
            </div>
            <div  style="text-align:center"  class="item">
                <div class="wrapper"><img src="themes/images/carousel/carousel3.jpg" alt="business themes">
                    <div class="carousel-caption">
                        <h2>What we have done?</h2>
                        <p>Pertokoan di Setiabudi 84 (8 unit), Setiabudi 152 (15 unit), Kedung Mundu, D.I Panjaitan, Erlangga, 
                            Lampersari, Sultan Agung, Brigjend Katamso, Dr. Wahidin, Dr. Sutomo, Kondotel & Apartemen Best Western Star.</p>
                        <a href="#" class="btn btn-large btn-success">Our Portfolio</a>
                    </div>
                </div>
            </div>
            <div  style="text-align:center"  class="item">
                <div class="wrapper"><img src="themes/images/carousel/business_website_templates_1.jpg" alt="business themes">
                    <div class="carousel-caption">
                        <h2>Join Us?</h2>
                        <p> Bagi anda yang berminat bergabung, kami menyediakan sarana Waralaba (Franchise) dengan harga kompetitif yang dijamin akan memberi hasil yang profitable.</p>
                        <a href="<?= site_url('page/contact') ?>" class="btn btn-large btn-success">Contact us</a>
                    </div>
                </div>
            </div>
        </div>
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
    </div>
</section>
<section id="middleSection">
    <div class="container">
        <div class="row" style="text-align:center">
            
            <?php if(isset($events) && count($events)>0): foreach($events as $evt): ?>
            <h3 style="">Upcoming Event</h3>
            <div class="alert alert-info span12">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <?= date("j F Y", strtotime($evt->due_date))." : $evt->title" ?>
                <a href="<?= site_url('event/show/'.$evt->id) ?>">selengkapnya..</a>
            </div>
            <?php endforeach; endif; ?>                     	
        </div>
    </div>
</section>
<section id="bodySection">
    <div class="container">
        <div class="row">
            <h3 class="span12" style="text-align:center; cursor: pointer;" onclick="window.location='<?= site_url('listing') ?>'">
                Listing Terbaru 
                <small>lihat semua (<?= $published ?>+)</small>
            </h3>
            <form class="hidden" id="frmjb" method="POST" action="<?= site_url('listing') ?>"><input id="penawaran" type="hidden" name="penawaran" value="" /></form>
            <div class="span4">
                <div class="thumbnail" style="min-height: 461px">
                    <h4><a href="javascript:$('#frmjb #penawaran').val('jual').parent().submit();">Property Dijual</a></h4>
                    
                    <div class="slideprop carousel slide">
                        <div class="carousel-inner">
                            <?php foreach($dijual as $slideprop): ?>
                            <div class="item <?= $slideprop==end($dijual)?'active':'' ?>">
                                <a href="portfolio.php">
                                    <img width="260" height="166" src="<?= $this->landlordlib->imgSrc($slideprop->photo) ?>">
                                </a>
                                <h5>
                                    <a href="<?= $this->landlordlib->gotoPropDetail($slideprop) ?>">
                                    # <?= $this->landlordlib->getAlias($slideprop) ?>
                                    </a>
                                </h5>
                                <p>
                                    <?= $slideprop->k_tidur > 0 ? ", $slideprop->k_tidur Kamar Tidur" : '' ?>
                                    <?= $slideprop->k_mandi > 0 ? ", $slideprop->k_mandi Kamar Mandi" : '' ?>
                                    <?php if ($slideprop->harga_sewa > 0) echo ", Harga Sewa Rp. " . number_format($slideprop->harga_sewa, 0, ',', '.') . "/tahun" ?>
                                    <?php if ($slideprop->harga_jual > 0) echo ", Harga Jual Rp. " . number_format($slideprop->harga_jual, 0, ',', '.') ?>
                                </p>
                                <div class="btn-toolbar">
                                    <div class="btn-group toolTipgroup">
                                        <a class="btn" href="#" data-placement="right" data-original-title="send email"><i class="icon-envelope"></i></a>
                                        <a class="btn" href="#" data-placement="top" data-original-title="do you like?"><i class="icon-thumbs-up"></i></a>
                                        <a class="btn" href="#" data-placement="top" data-original-title="dont like?"><i class="icon-thumbs-down"></i></a>
                                        <a class="btn" href="#" data-placement="top" data-original-title="share"><i class="icon-link"></i></a>
                                        <a class="btn" href="portfolio.php" data-placement="left" data-original-title="browse"><i class="icon-globe"></i></a>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="span4">
                <div class="thumbnail">
                    <h4>Cari Property</h4>
                    <form style="margin-bottom: 88px" class="form-horizontal" id="frm_cari_home" method="POST" action="<?= site_url('listing') ?>">
                    <div class="control-group">
                        <label class="control-label" for="jenis">cari</label>
                        <div class="controls">
                            <select name="jenis" class="input-medium" >
                                <option value="semua">Semua</option>
                                <?php foreach (unserialize(JENIS) as $jenis): ?>
                                    <option value="<?= $jenis ?>"><?= ucwords($jenis) ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="penawaran">untuk di</label>
                        <div class="controls">
                            <select name="penawaran" class="input-medium">
                                <option value="semua">Semua</option>
                                <?php foreach (unserialize(PENAWARAN) as $penawaran): ?>
                                <option value="<?= $penawaran ?>"><?= ucwords(str_replace('_', ' &amp; ', $penawaran)) ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <?php if(isset($marketing)): ?>
                    <div class="control-group">
                        <label class="control-label" for="agen">agen</label>
                        <div class="controls">
                            <select name="agen" class="input-medium">
                                <option value="semua" >Semua</option>
                                <?php foreach ($marketing as $agen): ?>
                                <option value="<?= $agen->uid ?>">
                                        <?= ucwords($agen->nama) ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <?php endif; ?>

<!--                    <div class="control-group">
                        <label class="control-label" for="kota">di kota</label>
                        <div class="controls">
                            <input type="hidden" name="kota" />
                            <input type="text" class="input-medium" name="fake_kota" placeholder="Semua Kota">
                        </div>
                    </div>-->

                    <div class="control-group">
                        <label class="control-label" for="lokasi">lokasi</label>
                        <div class="controls">
                            <input type="text" class="input-medium" name="lokasi" placeholder="Semua Lokasi">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="harga_min">harga min</label>
                        <div class="controls">
                            <input type="text" class="input-medium txtalrt" name="harga_min" placeholder="0">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="harga_max">harga max</label>
                        <div class="controls">
                            <input type="text" class="input-medium txtalrt" name="harga_max" placeholder="tidak terbatas">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="k_tidur">k. tidur</label>
                        <div class="controls">
                            <div class="input-prepend">
                                <span class="add-on">min. &nbsp; </span>
                                <input type="text" class="input-small txtalrt" name="k_tidur" placeholder="0" />
                            </div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="k_mandi">k. mandi</label>
                        <div class="controls">
                            <div class="input-prepend">
                                <span class="add-on">min. &nbsp; </span>
                                <input type="text" class="input-small txtalrt" name="k_mandi" placeholder="0" />
                            </div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="submit"></label>
                        <div class="controls">
                            <input type="submit" value="temukan property" class="btn btn-success" />
                        </div>
                    </div>

                </form>
                </div>
            </div>
            <div class="span4">
                <div class="thumbnail" style="min-height: 461px">
                    <h4><a href="javascript:$('#frmjb #penawaran').val('sewa').parent().submit();">Property Disewakan</a></h4>
                    
                    <div class="slideprop carousel slide">
                        <div class="carousel-inner">
                            <?php foreach($disewakan as $slideprop): ?>
                            <div class="item <?= $slideprop==end($dijual)?'active':'' ?>">
                                <a href="portfolio.php">
                                    <img width="260" height="166" src="<?= $this->landlordlib->imgSrc($slideprop->photo) ?>">
                                </a>
                                <h5>
                                    <a href="<?= $this->landlordlib->gotoPropDetail($slideprop) ?>">
                                    # <?= $this->landlordlib->getAlias($slideprop) ?>
                                    </a>
                                </h5>
                                <p>
                                    <?= $slideprop->k_tidur > 0 ? ", $slideprop->k_tidur Kamar Tidur" : '' ?>
                                    <?= $slideprop->k_mandi > 0 ? ", $slideprop->k_mandi Kamar Mandi" : '' ?>
                                    <?php if ($slideprop->harga_sewa > 0) echo ", Harga Sewa Rp. " . number_format($slideprop->harga_sewa, 0, ',', '.') . "/tahun" ?>
                                    <?php if ($slideprop->harga_jual > 0) echo ", Harga Jual Rp. " . number_format($slideprop->harga_jual, 0, ',', '.') ?>
                                </p>
                                <div class="btn-toolbar">
                                    <div class="btn-group toolTipgroup">
                                        <a class="btn" href="#" data-placement="right" data-original-title="send email"><i class="icon-envelope"></i></a>
                                        <a class="btn" href="#" data-placement="top" data-original-title="do you like?"><i class="icon-thumbs-up"></i></a>
                                        <a class="btn" href="#" data-placement="top" data-original-title="dont like?"><i class="icon-thumbs-down"></i></a>
                                        <a class="btn" href="#" data-placement="top" data-original-title="share"><i class="icon-link"></i></a>
                                        <a class="btn" href="portfolio.php" data-placement="left" data-original-title="browse"><i class="icon-globe"></i></a>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <br/>
    </div>
</section>
<section id="clientsSection" style="text-align:center; padding:48px 0">
    <div class="container">
        <a href="#" target="_blank"><img src="themes/images/clients/twitterLogo.jpg" alt="business template"></a>
        <a href="#" target="_blank"><img src="themes/images/clients/googleLogo.jpg" alt="business template"></a>
        <a href="#" target="_blank"><img src="themes/images/clients/facebookLogo.jpg" alt="business template"></a>
    </div>
</section>
<script src="<?php echo base_url('themes/js/autoNumeric-1.9.15.js') ?>"></script>