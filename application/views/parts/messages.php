<?php foreach ($agen_message as $msg): ?>
    <li class="media well well-small msgBody" id="msg-<?= $msg->id ?>">
        <div class="media-body">
            <small><?= date('d-M-Y H:i', strtotime($msg->dikirim)) ?></small>
            <a href="javascript:$delMsg(<?= $msg->id ?>);" class="pull-right"><i class="icon icon-remove-circle"></i></a>
            <p><?= $msg->pesan ?></p>
        </div>
    </li>
<?php endforeach; ?>