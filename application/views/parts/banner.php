<section id="bannerSection" style="background:url(<?= base_url('themes/images/banner/portfolio.png') ?>) no-repeat center center #000;">
    <div class="container" >	
        <h1 id="pageTitle"><?= $title ?><small><?= (isset($sub_title))?" : $sub_title":'' ?></small> 
            <span class="pull-right toolTipgroup">
                <a href="#" data-placement="top" data-original-title="Find us on via facebook"><img style="width:45px" src="<?= base_url('themes/images/facebook.png') ?>" alt="facebook" title="facebook"></a>
                <a href="#" data-placement="top" data-original-title="Find us on via twitter"><img style="width:45px" src="<?= base_url('themes/images/twitter.png') ?>" alt="twitter" title="twitter"></a>
                <a href="#" data-placement="top" data-original-title="Find us on via youtube"><img style="width:45px" src="<?= base_url('themes/images/youtube.png') ?>" alt="youtube" title="youtube"></a>
            </span>
        </h1>
    </div>
</section>