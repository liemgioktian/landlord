<!--tandai terjual-->
<div class="modal fade" id="sold" tabindex="-1" role="dialog" aria-labelledby="confirmLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="confirmLabel" >BERI TANDA PROPERTY TELAH TERJUAL</h4>
            </div>
            <div class="modal-body">
                <form method="POST" action="" class="form-horizontal" style="width: 75%; margin-left: auto; margin-right: auto">
                    <div class="control-group">
                        <label class="control-label" for="sold">TANGGAL</label>
                        <div class="controls">
                            <input type="text" name="sold" class="date" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="harga_terjual">HARGA</label>
                        <div class="controls">
                            <input type="text" name="harga_terjual" />
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a class="btn btn-primary" onclick="$('#sold form').submit()">Simpan</a>
                <a class="btn btn-default" data-dismiss="modal">Batal</a>
            </div>
        </div>
    </div>
</div>