<section id="headerSection">
    <div class="container">
        <div class="navbar">
            <div class="container">
                <button type="button" class="btn btn-navbar active" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <h1><a class="brand" href="<?= site_url() ?>">Landlord Property <small>Industry</small></a></h1>
                <div class="nav-collapse collapse">
                    <ul class="nav pull-right" id="main_menu">
                        <li class="active"><a href="<?= site_url() ?>">Home</a></li>
                        <li class=""><a href="<?= site_url('page/about') ?>">Who we are ?</a></li>
                        <li class=""><a href="<?= site_url('event') ?>">Special Event</a></li>
                        <li class=""><a href="<?= site_url('listing') ?>">Property Listing</a></li>
                        <li class=""><a href="<?= site_url('page/team') ?>">Marketing Team</a></li>
                        <li class=""><a href="<?= site_url('page/contact') ?>">Contact Us</a></li>
                        <li class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                <?php echo isset($userlogin['id'])? $userlogin['role']==1?'Admin':'Agen':'Login' ?>
                            <b class="caret"></b></a>
                            <?php if(!isset($userlogin['id'])): ?>
                            <div class="dropdown-menu">
                                <form class="well form-vertical" style="margin-bottom: 0" name="login-form">
                                    <div class="alert alert-error hidden" id="login-alert">
                                        <button class="close" onclick="$(this).parent().addClass('hidden');" type="button">×</button>
                                        <i></i>
                                    </div>
                                    <input type="text" class="input-large login" name="username" placeholder="Username">
                                    <input type="password" class="input-large login" name="password" placeholder="Password">
                                    <input type="text" class="input-large forgot hidden" name="email" placeholder="Email">
                                    
                                    <button class="btn login" name="btn-login">Masuk</button>
                                    <button class="btn forgot hidden" name="btn-forgot">Kirim Password</button>
                                    
                                    <small>
                                    <a href="javascript:$('.login').addClass('hidden');$('.forgot').removeClass('hidden');" class="pull-right login">Lupa Password?</a>
                                    <a href="javascript:$('.forgot').addClass('hidden');$('.login').removeClass('hidden');" class="pull-right forgot hidden">Login?</a>
                                    </small>
                                    
                                </form>
                            </div>
                            <?php else: ?>
                            <ul class="dropdown-menu">
                                <?php if($userlogin['role']==1): ?>
                                <li><a href="<?= site_url('user') ?>">Rekap Agen</a></li>
                                <li><a href="<?= site_url('listing/admin') ?>">Rekap Listing</a></li>
                                <li><a href="<?= site_url('report/penjualan') ?>">Rekap Penjualan</a></li>
                                <?php else: ?>
                                <li><a href="<?= site_url('listing/manage') ?>">Listing Agen</a></li>
                                <li><a href="<?= site_url('user/profile/'.$this->session->userdata('id')) ?>">Profile &amp; Rekap</a></li>
                                <?php endif; ?>
                                <li><a href="<?= site_url('auth/logout') ?>">Logout</a></li>
                            </ul>
                            <?php endif; ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>