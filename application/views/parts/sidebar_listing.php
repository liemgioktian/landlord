<style type="text/css">
    .form-horizontal .control-label{
        width: 65px;
        font-size: 12px;
    }
    .form-horizontal .controls{margin-left: 90px;}
    .input-medium[type="text"]{width: 140px}
</style>

<div class="span3">
    <ul class="media-list">
        <?php if (isset($userlogin['role'])): ?>
            <li class="media well well-small">
                <a class="btn btn-large btn-info" href="<?= site_url('property') ?>">
                    <i class="icon icon-home"></i> Daftarkan Property Baru
                </a>
            </li>
        <?php endif; ?>
        <li class="media well well-small">
            <h4>Filter Listing Property</h4>
            <form class="form-horizontal" id="form-filter">

                <div class="control-group">
                    <label class="control-label" for="jenis">cari</label>
                    <div class="controls">
                        <select name="jenis" class="input-medium" >
                            <option value="semua">Semua</option>
                            <?php foreach (unserialize(JENIS) as $jenis): ?>
                                <option value="<?= $jenis ?>" <?= isset($post['jenis']) && $post['jenis'] == $jenis ? 'selected="selected"' : '' ?> ><?= ucwords($jenis) ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="penawaran">untuk di</label>
                    <div class="controls">
                        <select name="penawaran" class="input-medium">
                            <option value="semua">Semua</option>
                            <?php foreach (unserialize(PENAWARAN) as $penawaran): ?>
                                <option value="<?= $penawaran ?>" <?= isset($post['penawaran']) && $post['penawaran'] == $penawaran ? 'selected="selected"' : '' ?> ><?= ucwords(str_replace('_', ' &amp; ', $penawaran)) ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <?php if (isset($marketing)): ?>
                    <div class="control-group">
                        <label class="control-label" for="agen">agen</label>
                        <div class="controls">
                            <select name="agen" class="input-medium">
                                <option value="semua" >Semua</option>
                                <?php foreach ($marketing as $agen): ?>
                                    <option value="<?= $agen->uid ?>" <?= isset($post['agen']) && $post['agen'] == $agen->uid ? 'selected="selected"' : '' ?> >
                                        <?= ucwords($agen->nama) ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                <?php endif; ?>

                <div class="control-group">
                    <label class="control-label" for="lokasi">lokasi</label>
                    <div class="controls">
                        <input type="text" class="input-medium" name="lokasi" placeholder="Semua Lokasi"
                        value="<?= isset($post['lokasi'])?$post['lokasi']:'' ?>">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="kota">di kota</label>
                    <div class="controls">
                        <input type="hidden" name="kota" />
                        <input type="text" class="input-medium" name="fake_kota" placeholder="Semua Kota">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="harga_min">harga min</label>
                    <div class="controls">
                        <input type="text" class="input-medium txtalrt" name="harga_min" 
                               placeholder="0" value="<?= isset($post['harga_min']) ? $post['harga_min'] : '' ?>">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="harga_max">harga max</label>
                    <div class="controls">
                        <input type="text" class="input-medium txtalrt" 
                               value="<?= isset($post['harga_max']) ? $post['harga_max'] : '' ?>"
                               name="harga_max" placeholder="tidak terbatas">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="k_tidur">k. tidur</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on">min. &nbsp; </span>
                            <input type="text" class="input-small txtalrt" 
                                   value="<?= isset($post['k_tidur']) ? $post['k_tidur'] : '' ?>"
                                   name="k_tidur" placeholder="0" />
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="k_mandi">k. mandi</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on">min. &nbsp; </span>
                            <input type="text" class="input-small txtalrt" 
                                   value="<?= isset($post['k_mandi']) ? $post['k_mandi'] : '' ?>"
                                   name="k_mandi" placeholder="0" />
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="">
                    </label>
                    <div class="controls">
                        <button class="btn btn-primary" onclick="resetFilter();
                                return false;">Reset</button>
                    </div>
                </div>

            </form>
        </li>
        <li class="media well well-small">
            <a class="btn btn-large btn-info" href="<?= site_url('listing/top') ?>">
                <i class="icon icon-star"></i> Landlord Top Listing
            </a>
        </li>
        <li class="media well well-small">
            <a class="btn btn-large btn-info" href="<?= site_url('listing/hot') ?>">
                <i class="icon icon-thumbs-up"></i> Landlord Hot Listing
            </a>
        </li>
    </ul>

</div>
<script src="<?php echo base_url('themes/js/autoNumeric-1.9.15.js') ?>"></script>       