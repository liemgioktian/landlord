<?php foreach ($items as $evt): ?>
    <li class="media">
        <a class="pull-left" href="blog_#">
            <img width="64" height="64" class="media-object" src="<?= base_url('themes/images/lld.jpg') ?>" alt="bootstrap business template">
        </a>
        <div class="media-body">
            <h5 class="media-heading"><?= $evt->title ?></h5>
            <small><em><?= date("j F Y", strtotime($evt->due_date)) ?></em> <a href="<?= site_url('event/show/'.$evt->id) ?>"> Selengkapnya..</a></small>
        </div>
    </li>
<?php endforeach; ?>