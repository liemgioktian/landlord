<div id="listinglocation" onload="initialize()">
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" >
        function setMapAddress(address)
        {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({address: address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var latlng = results[0].geometry.location;
                    var options = {
                        zoom: 15,
                        center: latlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        streetViewControl: false
                    };
                    var mymap = new google.maps.Map(document.getElementById('map'), options);
                    var marker = new google.maps.Marker({
                        map: mymap,
                        position: results[0].geometry.location
                    });
                }
            });
        }
        setMapAddress("<?= $address ?>");
    </script>

    <script type="text/javascript">
        var map;
        function setLatLngByAddress(mymap, address) {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    mymap.latlngbyaddress = results[0].geometry.location;
                    mymap.setCenter(map.latlngbyaddress);
                    var marker = new google.maps.Marker({
                        map: mymap,
                        position: mymap.latlngbyaddress
                    });
                    if (mymap.streetView != undefined)
                        mymap.streetView.setPosition(mymap.latlngbyaddress);
                } else {
                    alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        function initialize() {
            var myOptions = {
                zoom: 14,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                streetViewControl: true
            }
            map = new google.maps.Map(document.getElementById('streetview'), myOptions);
            setLatLngByAddress(map, "<?= $address ?>");
            var panoramaOptions = {
                position: map.latlngbyaddress,
                pov: {
                    heading: 0,
                    pitch: 0,
                    zoom: 1
                },
                visible: true
            };
            var panorama = new google.maps.StreetViewPanorama(document.getElementById("streetview"), panoramaOptions);
            map.setStreetView(panorama);
            panorama.setVisible(true);

        }
    </script>
    <div id="locationmap">
        <div class="mapnostreetview" id="map"></div>
    </div>
    <div style='display:none;' id="locationstreetview">
        <div id="streetview"></div>
    </div>
</div>