<div class="span3">
    <ul class="media-list" id="sidebar_profile">
        <li class="media well well-small">
            <h4>Photo Agen</h4>
            <p>
                <img src="<?= isset($user_edit)?$this->landlordlib->imgSrc($user_edit['photo']):  base_url('themes/images/portfolio/1.jpg') ?>"  id="photo_agen"/>
            </p>
        </li>
        
        <?php if(isset($user_edit)): ?>
        <li id="msg_head">
            <h4>Messages</h4>
        </li>
        <!-- messages goes here -->
        <?php endif; ?>
        
    </ul>
</div>
<?php if(isset($user_edit)): ?>
<script type="text/javascript">
    msgDraw();
    function msgDraw()
    {
        $('.msgBody').remove();
        $.get('<?= site_url('user/msgList/'.$user_edit['id']) ?>',{},function(msgs){
            $('#msg_head').append(msgs);
        });
    }
    
    $delMsg = function($msgid)
    {
        $.get('<?= site_url('user/rmMsg') ?>/'+$msgid,{},function(){msgDraw();});
    }
</script>
<?php endif; ?>