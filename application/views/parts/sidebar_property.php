<div class="span3">
    <ul class="media-list">
        <li class="media well well-small">
            <h4>Agen Property</h4>
            <p>
                <img src="<?= $this->landlordlib->imgSrc($item['photo']) ?>" />
            <table style="width: 100%">
                <tr><td>Agen</td><td><?= $item['nm_agen'] ?></td></tr>
                <tr><td>Mobile</td><td><?= $item['mobile'] ?></td></tr>
                <tr><td>Office</td><td><?= $item['office'] ?></td></tr>
                <tr><td>Fax</td><td><?= $item['fax'] ?></td></tr>
                <tr>
                    <td colspan="2" style="text-align: center; padding-top: 10px">
                        <div class="btn-group">
                        <a class="btn" data-toggle="modal" data-target="#message">
                            <i class="icon icon-envelope"></i> pesan
                        </a>
                        <a href="javascript:$('#frmagt').submit()" class="btn">
                            <i class="icon icon-list"></i> listing agen
                        </a>
                        </div>
                    </td>
                </tr>
            </table>
            </p>
        </li>
        <li>
            <h4>Property Serupa</h4>
        </li>
        <?php foreach ($serupa as $related): ?>
            <li class="media well well-small" style="cursor: pointer" onclick="window.location = '<?= $this->landlordlib->gotoPropDetail($related) ?>';" >
                <a class="pull-left" href="#">
                    <img class="media-object" 
                         src="<?= $this->landlordlib->imgSrc($related->photo) ?>" 
                         width="64" height="37" alt="bootstrap business template"/>
                </a>
                <div class="media-body">
                    <?= $this->landlordlib->getAlias($related) ?>
                    <?= $related->k_tidur > 0 ? ", $related->k_tidur Kamar Tidur" : '' ?>
                    <?= $related->k_mandi > 0 ? ", $related->k_mandi Kamar Mandi" : '' ?>
                    <?php if ($related->harga_sewa > 0) echo ", Harga Sewa Rp. " . number_format($related->harga_sewa, 0, ',', '.') . "/tahun" ?>
                    <?php if ($related->harga_jual > 0) echo ", Harga Jual Rp. " . number_format($related->harga_jual, 0, ',', '.') ?>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
<div class="modal fade" id="message" tabindex="-1" role="dialog" aria-labelledby="messageLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="messageLabel" >TINGGALKAN PESAN BAGI <?= strtoupper($item['nm_agen']) ?></h4>
                <small>* mohon mencantumkan property yang dimaksud beserta contact yang dapat kami hubungi</small>
            </div>
            <div class="modal-body">
                <textarea style="width: 90%" id="text_message"></textarea>
                <div class="input-prepend">
                    <span class="add-on captcha"  ><?=$image;?></span>
                    <input type="text" id="secutity_code" class="captcha" >
                </div>
            </div>
            <div class="modal-footer">
                <a class="btn btn-primary" href="javascript:sendMsg();">Kirim</a>
                <a class="btn btn-default" data-dismiss="modal" id="closeMsg">Batal</a>
            </div>
        </div>
    </div>
</div>
<form class="hidden" id="frmagt" method="POST" action="<?= site_url('listing') ?>"><input type="hidden" name="agen" value="<?= $item['agenid'] ?>" /></form>
<script type="text/javascript">
    function sendMsg()
    {
        $img = '<?=$image?>';
        $sc = $('#secutity_code').val();
        $msg = $('#text_message').val();
        $userid = <?= $item['agenid'] ?>;
        $.post($site_url+'/user/sendmsg',{
            msg:$msg, userid:$userid,
            security_code:$sc, img:$img
        },function(code){
            if(code==200) $('#closeMsg').click();
            else if(code==401) $('#secutity_code').attr('placeholder','kode salah');
        });
    }
    $('#message').on('hidden.bs.modal', function (e) {
        $('#text_message, #secutity_code').val('');
    })
</script>