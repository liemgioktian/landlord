<div class="well">
    <div id="tglcalc">
        <b>KALKULASI TOTAL PENJUALAN </b>
        <input type="text" placeholder="Sejak Awal" id="since" class="date" />
        <input type="text" placeholder="Hingga Sekarang" id="until" class="date" />
        <button class="btn" onclick="$('.date').val('');
            $table.fnDraw();" >reset</button>
        <button class="btn" onclick="$table.fnDraw();" >hitung</button>
    </div>

    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>PROPERTY</th>
                <th>SEJAK</th>
                <th>TERJUAL</th>
                <th>HARGA</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="4" class="dataTables_empty">MOHON TUNGGU SEJENAK...</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <th colspan="3" style="text-align: right">TOTAL</th>
                <th id="total"></th>
            </tr>
        </tfoot>
    </table>
    <link rel="stylesheet" href="<?= base_url('datatables/DT_bootstrap.css') ?>" />
    <link href="<?= base_url('assets/css/ui-lightness/jquery-ui-1.10.0.custom.min.css') ?>" rel="stylesheet">
    <script src="<?= base_url('assets/js/jquery-ui-1.10.0.custom.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('datatables/js/jquery.dataTables.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('datatables/DT_bootstrap.js') ?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $table = $('table').eq(0).dataTable({
                "iDisplayLength": 5,
                "bServerSide": true,
                "aoColumnDefs":[{"sClass": "txtalrt", "aTargets": [3]}],
                "sAjaxSource": '<?= site_url('report/agen/'.$user_edit['id']) ?>',
                "fnServerData": function(sSource, aoData, fnCallback) {
                    aoData.push({"name": "since", "value": $('#since').val()}, {"name": "until", "value": $('#until').val()});
                    $.getJSON(sSource, aoData, function(json) {
                        $('#total').html(json.total);
                        fnCallback(json);
                    });
                }
            });
//            $('.date').datepicker({dateFormat: 'dd-mm-yy'});
        });
    </script>
</div>