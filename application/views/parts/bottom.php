<section id="footerSection">
    <div class="container">
        <footer class="footer well well-small">
            <div class="row-fluid">
                <div class="span4">
                    <h5>Visi</h5>
                    <em>
                        "Mengembangkan sebuah grup bisnis properti dengan inovasi dan kreativitas yang tinggi secara terus menerus sehingga menjadi 
                        market leader dalam industri bisnis properti di Indonesia khususnya JawaTengah dan secara konsisten menciptakan nilai tambah 
                        dalam penyediaan ruang kehidupan dan kesejahteraan yang lebih baik lagi bagi masyarakat dan mitra bisnis." <br/><br/>
                    </em>
                    <h5>Misi</h5>
                    <em>
                        "Menjadi yang terbaik dan terdepan dalam industri bisnis properti dengan sistem kemitraan yang saling menguntungkan. Mengedepankan 
                        kepuasan pelanggan & mitra dengan memberikan pelayanan yang terbaik. Memiliki jaringan yang luas di area Jawa Tengah pada khususnya 
                        dan seluruh Indonesia pada umumnya."
                    </em>
                    <!--
                    <br/><br/>
                    <h5>Subscription</h5>
                    <form>
                        <div class="input-append">
                            <input id="appendedInputButton"  placeholder="Enter your e-mail" type="text">
                            <button class="btn btn-warning" type="button">send </button>
                        </div>
                    </form>
                    -->
                </div>
                <div class="span5">
                    <h4>Latest events</h4>
                    <ul class="media-list" id="latesteventcontainer">
                        
                    <!-- latest event goes here trough ajax-->
                        
                    </ul>
                </div>

                <div class="span3">
                    <h4>Visit us</h4>
                    <address style="margin-bottom:15px;">
                        <strong><a href="index.php" title=""><i class=" icon-home"></i> Landlord Property Industry </a></strong><br>
                        Jl. Pandanaran 50 A<br>
                        Semarang, Jawa Tengah<br>
                    </address>
                    Phone: <i class="icon-phone-sign"></i> &nbsp; (024) 86453000 <br>
                    Email: <a href="contact.php" title=""><i class="icon-envelope-alt"></i> office@landlordindonesia.com</a><br/>
                    Link: <a href="index.php" title=""><i class="icon-globe"></i> www.landlordindonesia.com</a><br/><br/>
                    <h5>Quick Links</h5>	
                    <a href="<?= site_url('listing') ?>" title=""><i class="icon-cogs"></i> Listing </a><br/>
                    <a href="<?= site_url('page/about') ?>" title=""><i class="icon-info-sign"></i> About us </a><br/>
                    <a href="portfolio.php" title="portfolio"><i class="icon-question-sign"></i> Portfolio </a><br/>

                    <h5>Find us on</h5>	
                    <div style="font-size:2.5em;">
                        <a href="#facebook" role="button" data-toggle="modal" style="display:inline-block; width:1em"> <i class="icon-facebook-sign"> </i> </a> 
                        <a href="#twitter" role="button" data-toggle="modal" title="" style="display:inline-block; width:1em"> <i class="icon-twitter-sign"> </i> </a>
                        <a href="#youtube" role="button" data-toggle="modal" style="display:inline-block;width:1em"> <i class="icon-facetime-video"> </i> </a>
                        <a href="#" title="" style="display:inline-block;width:1em"> <i class="icon-google-plus-sign"> </i> </a>
                        <a href="#rss" role="button" data-toggle="modal" style="display:inline-block;width:1em" > <i class="icon-rss"> </i> </a>
                        <!-- Facebook -->
                        <div id="facebook" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="facebook" aria-hidden="true">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h3>Facebook Header</h3>
                            </div>
                            <div class="modal-body">
                                <p>"Our aim is simple - to provide affordable web design and development services for Mobile and Computer by creating websites that fully meet your requirements a professional look that inspire confidence in your customer."</p>
                            </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                <button class="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                        <!-- Twitter -->
                        <div id="twitter" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="twitter" aria-hidden="true">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h3>Twitter Header</h3>
                            </div>
                            <div class="modal-body">
                                <p>"Our aim is simple - to provide affordable web design and development services for Mobile and Computer by creating websites that fully meet your requirements a professional look that inspire confidence in your customer."</p>
                            </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                <button class="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                        <!-- Rss feed -->
                        <div id="rss" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="rss" aria-hidden="true">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h3>RSS feed header</h3>
                            </div>
                            <div class="modal-body">
                                <p>"Our aim is simple - to provide affordable web design and development services for Mobile and Computer by creating websites that fully meet your requirements a professional look that inspire confidence in your customer."</p>
                            </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                <button class="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                        <!-- Youtube -->
                        <div id="youtube" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="youtube" aria-hidden="true">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h3>Youtube Video</h3>
                            </div>
                            <div class="modal-body">
                                Videos here
                            </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                <button class="btn btn-primary">Save changes</button>
                            </div>
                        </div>		
                    </div>
                </div>
            </div>

            <p style="padding:18px 0 44px">&copy; 2013 Landlord Property Industry</p>
        </footer>
    </div>
</section>