<div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="confirmLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="confirmLabel" >KONFIRMASI SISTEM</h4>
            </div>
            <div class="modal-body">
                APAKAH ANDA YAKIN ?
            </div>
            <div class="modal-footer">
                <a class="btn btn-primary yakin">Yakin</a>
                <a class="btn btn-default tidak" data-dismiss="modal">Tidak</a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function llconfirm($url, $ajax){
        if($ajax)$('a.yakin').off('click').click(function(){$.get($url,{},function(){
                $('a.tidak').click();
                if($table.length>0) $table.fnDraw();
            });});
        else $('a.yakin').attr('href',$url);
        $('#confirm').modal('show');
    }
</script>