<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Landlord Property Industry</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <link id="callCss" rel="stylesheet" href="http://localhost/landlord/themes/current/bootstrap.min.css" type="text/css" media="screen"/>
        <link href="http://localhost/landlord/themes/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css">
        <link href="http://localhost/landlord/themes/css/font-awesome.css" rel="stylesheet" type="text/css">
        <link href="http://localhost/landlord/themes/css/base.css" rel="stylesheet" type="text/css">
        <link href="http://localhost/landlord/assets/css/landlordindonesia.css" rel="stylesheet" type="text/css">
        <style type="text/css" id="enject"></style>
        <script src="http://localhost/landlord/themes/js/jquery-1.8.3.min.js"></script>
        <script src="http://localhost/landlord/themes/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="http://localhost/landlord/assets/js/bootstrap-confirmation.js"></script>
        <script type="text/javascript">
            var $site_url = 'http://localhost/landlord/index.php';
            var $table = null;
        </script>
    </head>
    <body>

        <section id="headerSection">
            <div class="container">
                <div class="navbar">
                    <div class="container">
                        <button type="button" class="btn btn-navbar active" data-toggle="collapse" data-target=".nav-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <h1><a class="brand" href="http://localhost/landlord/index.php">Landlord Property <small>Industry</small></a></h1>
                        <div class="nav-collapse collapse">
                            <ul class="nav pull-right" id="main_menu">
                                <li class="active"><a href="http://localhost/landlord/index.php">Home</a></li>
                                <li class=""><a href="http://localhost/landlord/index.php/page/about">Who we are ?</a></li>
                                <li class=""><a href="http://localhost/landlord/index.php/event">Special Event</a></li>
                                <li class=""><a href="http://localhost/landlord/index.php/listing">Property Listing</a></li>
                                <li class=""><a href="http://localhost/landlord/index.php/page/contact">Contact Us</a></li>
                                <li class="dropdown">
                                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                        Login                            <b class="caret"></b></a>
                                    <div class="dropdown-menu">
                                        <form class="well form-vertical" style="margin-bottom: 0" name="login-form">
                                            <div class="alert alert-error hidden" id="login-alert">
                                                <button class="close" onclick="$(this).parent().addClass('hidden');" type="button">×</button>
                                                <i></i>
                                            </div>
                                            <input type="text" class="input-large" name="username" placeholder="Username">
                                            <input type="password" class="input-large" name="password" placeholder="Password">
                                            <button class="btn" name="btn-login">Masuk</button>
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>        
        <section id="bannerSection" style="background:url(http://localhost/landlord/themes/images/banner/portfolio.png) no-repeat center center #000;">
            <div class="container" >	
                <h1 id="pageTitle">Landlord Event<small></small> 
                    <span class="pull-right toolTipgroup">
                        <a href="#" data-placement="top" data-original-title="Find us on via facebook"><img style="width:45px" src="http://localhost/landlord/themes/images/facebook.png" alt="facebook" title="facebook"></a>
                        <a href="#" data-placement="top" data-original-title="Find us on via twitter"><img style="width:45px" src="http://localhost/landlord/themes/images/twitter.png" alt="twitter" title="twitter"></a>
                        <a href="#" data-placement="top" data-original-title="Find us on via youtube"><img style="width:45px" src="http://localhost/landlord/themes/images/youtube.png" alt="youtube" title="youtube"></a>
                    </span>
                </h1>
            </div>
        </section>        
        <section id="bodySection">
            <div class="container">					
                <div class="row">						
                    <div class="span12" style="min-height: 400px">
                        <p>Severity: <?php echo $severity; ?></p>
                        <p>Message:  <?php echo $message; ?></p>
                        <p>Filename: <?php echo $filepath; ?></p>
                        <p>Line Number: <?php echo $line; ?></p>
                    </div>
                </div>
            </div>
        </section>

        <section id="footerSection">
            <div class="container">
                <footer class="footer well well-small">
                    <div class="row-fluid">
                        <div class="span4">
                            <h5>Visi</h5>
                            <em>
                                "Mengembangkan sebuah grup bisnis properti dengan inovasi dan kreativitas yang tinggi secara terus menerus sehingga menjadi 
                                market leader dalam industri bisnis properti di Indonesia khususnya JawaTengah dan secara konsisten menciptakan nilai tambah 
                                dalam penyediaan ruang kehidupan dan kesejahteraan yang lebih baik lagi bagi masyarakat dan mitra bisnis." <br/><br/>
                            </em>
                            <h5>Misi</h5>
                            <em>
                                "Menjadi yang terbaik dan terdepan dalam industri bisnis properti dengan sistem kemitraan yang saling menguntungkan. Mengedepankan 
                                kepuasan pelanggan & mitra dengan memberikan pelayanan yang terbaik. Memiliki jaringan yang luas di area Jawa Tengah pada khususnya 
                                dan seluruh Indonesia pada umumnya."
                            </em>
                            <!--
                            <br/><br/>
                            <h5>Subscription</h5>
                            <form>
                                <div class="input-append">
                                    <input id="appendedInputButton"  placeholder="Enter your e-mail" type="text">
                                    <button class="btn btn-warning" type="button">send </button>
                                </div>
                            </form>
                            -->
                        </div>
                        <div class="span5">
                            <h4>Latest events</h4>
                            <ul class="media-list">
                                <li class="media">
                                    <a class="pull-left" href="blog_#">
                                        <img class="media-object" src="http://localhost/landlord/themes/images/lld.jpg" alt="bootstrap business template">
                                    </a>
                                    <div class="media-body">
                                        <h5 class="media-heading">Kenapa Landlord Property?</h5>
                                        "Menjadi yang terbaik dan terdepan dalam industri bisnis properti..."<br/>
                                        <small><em>August 15, 2013</em> <a href="blog_#"> More</a></small>
                                    </div>
                                </li>
                                <li class="media">
                                    <a class="pull-left" href="blog_#">
                                        <img class="media-object" src="http://localhost/landlord/themes/images/lld.jpg" alt="bootstrap business template">
                                    </a>
                                    <div class="media-body">
                                        <h5 class="media-heading">Kenapa Landlord Property?</h5>
                                        "Menjadi yang terbaik dan terdepan dalam industri bisnis properti..."<br/>
                                        <small><em>August 15, 2013</em> <a href="blog_#"> More</a></small>
                                    </div>
                                </li>
                                <li class="media">
                                    <a class="pull-left" href="blog_#">
                                        <img class="media-object" src="http://localhost/landlord/themes/images/lld.jpg" alt="bootstrap business template">
                                    </a>
                                    <div class="media-body">
                                        <h5 class="media-heading">Kenapa Landlord Property?</h5>
                                        "Menjadi yang terbaik dan terdepan dalam industri bisnis properti..."<br/>
                                        <small><em>August 15, 2013</em> <a href="blog_#"> More</a></small>
                                    </div>
                                </li>
                                <li class="media">
                                    <a class="pull-left" href="blog_#">
                                        <img class="media-object" src="http://localhost/landlord/themes/images/lld.jpg" alt="bootstrap business template">
                                    </a>
                                    <div class="media-body">
                                        <h5 class="media-heading">Kenapa Landlord Property?</h5>
                                        "Menjadi yang terbaik dan terdepan dalam industri bisnis properti..."<br/>
                                        <small><em>August 15, 2013</em> <a href="blog_#"> More</a></small>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <div class="span3">
                            <h4>Visit us</h4>
                            <address style="margin-bottom:15px;">
                                <strong><a href="index.php" title="business"><i class=" icon-home"></i> Landlord Property Industry </a></strong><br>
                                Jl. D.I Panjaitan No. 27 A <br>
                                Semarang, Jawa Tengah<br>
                            </address>
                            Phone: <i class="icon-phone-sign"></i> &nbsp; (024) 3575757 <br>
                            Email: <a href="contact.php" title="contact"><i class="icon-envelope-alt"></i> office@landlordindonesia.com</a><br/>
                            Link: <a href="index.php" title="Business ltd"><i class="icon-globe"></i> www.landlordindonesia.com</a><br/><br/>
                            <h5>Quick Links</h5>	
                            <a href="#" title="services"><i class="icon-cogs"></i> Services </a><br/>
                            <a href="about.php" title=""><i class="icon-info-sign"></i> About us </a><br/>
                            <a href="portfolio.php" title="portfolio"><i class="icon-question-sign"></i> Portfolio </a><br/>

                            <h5>Find us on</h5>	
                            <div style="font-size:2.5em;">
                                <a href="#facebook" role="button" data-toggle="modal" style="display:inline-block; width:1em"> <i class="icon-facebook-sign"> </i> </a> 
                                <a href="#twitter" role="button" data-toggle="modal" title="" style="display:inline-block; width:1em"> <i class="icon-twitter-sign"> </i> </a>
                                <a href="#youtube" role="button" data-toggle="modal" style="display:inline-block;width:1em"> <i class="icon-facetime-video"> </i> </a>
                                <a href="#" title="" style="display:inline-block;width:1em"> <i class="icon-google-plus-sign"> </i> </a>
                                <a href="#rss" role="button" data-toggle="modal" style="display:inline-block;width:1em" > <i class="icon-rss"> </i> </a>
                                <!-- Facebook -->
                                <div id="facebook" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="facebook" aria-hidden="true">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h3>Facebook Header</h3>
                                    </div>
                                    <div class="modal-body">
                                        <p>"Our aim is simple - to provide affordable web design and development services for Mobile and Computer by creating websites that fully meet your requirements a professional look that inspire confidence in your customer."</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                        <button class="btn btn-primary">Save changes</button>
                                    </div>
                                </div>
                                <!-- Twitter -->
                                <div id="twitter" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="twitter" aria-hidden="true">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h3>Twitter Header</h3>
                                    </div>
                                    <div class="modal-body">
                                        <p>"Our aim is simple - to provide affordable web design and development services for Mobile and Computer by creating websites that fully meet your requirements a professional look that inspire confidence in your customer."</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                        <button class="btn btn-primary">Save changes</button>
                                    </div>
                                </div>
                                <!-- Rss feed -->
                                <div id="rss" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="rss" aria-hidden="true">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h3>RSS feed header</h3>
                                    </div>
                                    <div class="modal-body">
                                        <p>"Our aim is simple - to provide affordable web design and development services for Mobile and Computer by creating websites that fully meet your requirements a professional look that inspire confidence in your customer."</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                        <button class="btn btn-primary">Save changes</button>
                                    </div>
                                </div>
                                <!-- Youtube -->
                                <div id="youtube" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="youtube" aria-hidden="true">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h3>Youtube Video</h3>
                                    </div>
                                    <div class="modal-body">
                                        Videos here
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                        <button class="btn btn-primary">Save changes</button>
                                    </div>
                                </div>		
                            </div>
                        </div>
                    </div>

                    <p style="padding:18px 0 44px">&copy; 2013 Landlord Property Industry</p>
                </footer>
            </div>
        </section>        <div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="confirmLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="confirmLabel" >KONFIRMASI SISTEM</h4>
                    </div>
                    <div class="modal-body">
                        APAKAH ANDA YAKIN ?
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-primary yakin">Yakin</a>
                        <a class="btn btn-default tidak" data-dismiss="modal">Tidak</a>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            function llconfirm($url, $ajax) {
                if ($ajax)
                    $('a.yakin').off('click').click(function() {
                        $.get($url, {}, function() {
                            $('a.tidak').click();
                            if ($table.length > 0)
                                $table.fnDraw();
                        });
                    });
                else
                    $('a.yakin').attr('href', $url);
                $('#confirm').modal('show');
            }
        </script>        
        <a href="javascript:;" class="btn" style="position: fixed; bottom: 38px; right: 10px; display: none; " id="toTop"> <i class="icon-arrow-up"></i> Go to top</a>

        <script src="http://localhost/landlord/assets/js/landlordindonesia.js"></script>
        <script src="http://localhost/landlord/themes/js/business_ltd_1.0.js"></script>

    </body>
</html>