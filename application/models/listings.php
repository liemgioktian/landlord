<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Listings extends CI_Model {

    public $table = 'p_property';
    protected $common_filter = array();

    public function __construct() {
        parent::__construct();
        $this->load->database();

        # common filters
        $this->common_filter['TIMESTAMPDIFF(DAY, published, CURDATE()) <'] = EXPIRED_DAYS;
        $this->common_filter['first_publish IS NOT NULL'] = null;
        $this->common_filter['active ='] = 1;
        $this->common_filter['sold'] = null;
    }

    public function draw($limit, $offset, $filter, $sorting, $disable_common_filter = false) {
//        die(var_dump($filter));
        unset($filter['fake_kota']);
        if ($filter['kota'] < 1)
            unset($filter['kota']);

        if ($filter['jenis'] == 'semua')
            unset($filter['jenis']);
        if ($filter['penawaran'] == 'semua')
            unset($filter['penawaran']);
        $jenis_harga = isset($filter['penawaran']) ? $filter['penawaran'] : null;

        if ($filter['harga_min'] > 0) {
            $filter['harga_min'] = str_replace('.', '', $filter['harga_min']);
            if (null != $jenis_harga && $jenis_harga != 'jual_sewa')
                $filter['harga_' . $jenis_harga . ' >='] = $filter['harga_min'];
            else
                $filter_after_query = array('key' => 'harga_min', 'value' => $filter['harga_min']);
//                foreach (unserialize(PENAWARAN) as $jenis_harga)if($jenis_harga!='jual_sewa')$filter['harga_'.$jenis_harga.' >'] = $filter['harga_min'];
        }
        unset($filter['harga_min']);

        if ($filter['harga_max'] > 0) {
            $filter['harga_max'] = str_replace('.', '', $filter['harga_max']);
            if (null != $jenis_harga && $jenis_harga != 'jual_sewa')
                $filter['harga_' . $jenis_harga . ' <='] = $filter['harga_max'];
            else
//                $filter_after_query = array('key'=>'harga_max','value'=>$filter['harga_max']); 
                foreach (unserialize(PENAWARAN) as $jenis_harga)
                    if ($jenis_harga != 'jual_sewa')
                        $filter['harga_' . $jenis_harga . ' <'] = $filter['harga_max'];
        }
        unset($filter['harga_max']);

        if ($filter['k_tidur'] > 0) {
            $filter['k_tidur >'] = (int) $filter['k_tidur'];
        }
        unset($filter['k_tidur']);

        if ($filter['k_mandi'] > 0) {
            $filter['k_mandi >'] = (int) $filter['k_mandi'];
        }
        unset($filter['k_mandi']);

        if (isset($filter['agen']))
            if ($filter['agen'] == 'semua')
                unset($filter['agen']);

        # sorting
        if ($sorting['order'] == 'harga') $order = null != $jenis_harga ? "harga_$jenis_harga" : "harga_jual";
        else if ($sorting['order'] == 'tanggal') $order = 'published';
        else $order = $sorting['order'];
        $by = $sorting['ascdesc'];

        if(!$disable_common_filter)$this->db->where($this->common_filter);
        if (isset($filter['lokasi'])) {
          $this->db->like('lokasi', $filter['lokasi']);
          unset($filter['lokasi']);
        }
        $this->db
                ->select($this->table . '.*')
                ->select('p_photo.photo')
                ->select('u_kab_kota.kab_kota')
                ->select('s_marketing.nama,s_marketing.mobile')
                ->from($this->table)
                ->join('p_photo', $this->table . '.id=p_photo.property_id', 'left')
                ->join('u_kab_kota', $this->table . '.kota=u_kab_kota.id_kab_kota', 'left')
                ->join('s_login', 'p_property.agen=s_login.id', 'left')
                ->join('s_marketing', 's_login.marketing_id=s_marketing.id', 'left')
                ->where($filter)
                ->order_by($order, $by)
                ->limit($limit, $offset)
                ->group_by($this->table . '.id'); // to choose single photo
        $result = $this->db->get()->result();
//        die(var_dump($this->db->last_query()));
        // php filtering for unhandled filter by query
        if (isset($filter_after_query)) {
            for ($i = 0; $i < count($result); $i++) {
                if ($filter_after_query['key'] == 'harga_min') {
                    $lolos = ($result[$i]->harga_jual >= $filter_after_query['value'] || $result[$i]->harga_sewa >= $filter_after_query['value']);
                } else if ($filter_after_query['key'] == 'harga_max') {
                    $lolos = ($result[$i]->harga_jual <= $filter_after_query['value'] || $result[$i]->harga_sewa <= $filter_after_query['value']);
                }
                if (!$lolos)
                    unset($result[$i]);
            }
        }
        return $result;
    }

    public function manage($limit, $offset, $filter, $sorting) {
        unset($filter['fake_kota']);
        if ($filter['kota'] < 1)
            unset($filter['kota']);

        if ($filter['jenis'] == 'semua')
            unset($filter['jenis']);
        if ($filter['penawaran'] == 'semua')
            unset($filter['penawaran']);
        $jenis_harga = isset($filter['penawaran']) ? $filter['penawaran'] : null;

        if ($filter['harga_min'] > 0) {
            if (null != $jenis_harga)
                $filter['harga_' . $jenis_harga . ' >'] = $filter['harga_min'];
            else
                foreach (unserialize(PENAWARAN) as $jenis_harga)
                    $filter['harga_' . $jenis_harga . ' >'] = $filter['harga_min'];
        }
        unset($filter['harga_min']);

        if ($filter['harga_max'] > 0) {
            if (null != $jenis_harga)
                $filter['harga_' . $jenis_harga . ' <'] = $filter['harga_max'];
            else
                foreach (unserialize(PENAWARAN) as $jenis_harga)
                    $filter['harga_' . $jenis_harga . ' <'] = $filter['harga_max'];
        }
        unset($filter['harga_max']);

        if ($filter['k_tidur'] > 0) {
            $filter['k_tidur >'] = (int) $filter['k_tidur'];
        }
        unset($filter['k_tidur']);

        if ($filter['k_mandi'] > 0) {
            $filter['k_mandi >'] = (int) $filter['k_mandi'];
        }
        unset($filter['k_mandi']);

        # sorting
        if ($sorting['order'] == 'harga')
            $order = null != $jenis_harga ? "harga_$jenis_harga" : "harga_jual";
        else
            $order = 'published';
        $by = $sorting['ascdesc'];

        $exp = EXPIRED_DAYS;
        $this->db
                ->select($this->table . '.*')
                ->select("IF($this->table.published IS NULL,$exp,TIMESTAMPDIFF(DAY, $this->table.published, CURDATE())) as usia", false)
                ->select('p_photo.photo')
                ->select('u_kab_kota.kab_kota')
                ->select('s_marketing.nama,s_marketing.mobile')
                ->from($this->table)
                ->join('p_photo', $this->table . '.id=p_photo.property_id', 'left')
                ->join('u_kab_kota', $this->table . '.kota=u_kab_kota.id_kab_kota', 'left')
                ->join('s_login', 'p_property.agen=s_login.id', 'left')
                ->join('s_marketing', 's_login.marketing_id=s_marketing.id', 'left')

                #->where($this->common_filter) no filter for admin & agent
                ->where("$this->table.agen", $this->session->userdata('id'))
                ->where($filter)
                ->order_by($order, $by)
                ->limit($limit, $offset)
                ->group_by($this->table . '.id'); // to choose single photo
        return $this->db->get()->result();
    }

    public function related($base_property) {
        $mirip = "";
        $related_attr = array('lokasi', 'bentuk', 'jenis', 'lantai');
        foreach ($related_attr as $rel)
            if ($base_property[$rel] != '')
                $mirip.=" $this->table.$rel='$base_property[$rel]' OR ";

        foreach (array('sewa', 'jual') as $pnwrn) {
            $harga = (int) $base_property["harga_$pnwrn"];
            $toleransi = 0.25 * $harga;
            $min = $harga - $toleransi;
            $max = $harga + $toleransi;
            $mirip .= " ( $this->table.harga_$pnwrn > $min AND $this->table.harga_$pnwrn < $max ) OR ";
        }
        $mirip = substr($mirip, 0, -3);

        $common_filter = "";
        foreach ($this->common_filter as $key => $value)
//            $common_filter.=$value != null ? " $key $value AND " : " sold IS NULL ";
            $common_filter .= $key != 'sold' ? " $key $value AND " : " sold IS NULL ";

        $limit = LIMIT_RELATED;
        $query = $this->db
                ->query(
                "SELECT $this->table.*, p_photo.photo, u_kab_kota.kab_kota, s_marketing.nama,s_marketing.mobile " .
                "FROM $this->table " .
                "LEFT JOIN p_photo ON $this->table.id = p_photo.property_id " .
                "LEFT JOIN u_kab_kota ON $this->table.kota = u_kab_kota.id_kab_kota " .
                "LEFT JOIN s_login ON $this->table.agen = s_login.id " .
                "LEFT JOIN s_marketing ON s_login.marketing_id=s_marketing.id " .
                "WHERE $common_filter AND ( $mirip ) AND $this->table.id<> " . $base_property['id'] .
                " GROUP BY $this->table.id LIMIT $limit "
        );
        return $query->result();
    }

    function report() {
        return $this->db
                        ->select("DATE_FORMAT(posted,'%d/%m/%Y') as tgl_input", false)
                        ->select("DATE_FORMAT(first_publish,'%d/%m/%Y') as tgl_listing", false)
                        ->select('s_marketing.nama as marketing', false)
                        ->select('p_owner.nama as pemilik_property', false)
                        ->select('alamat as alamat_pemilik')
                        ->select('kontak as telpon')
                        #->select("CONCAT(lokasi,', ',kab_kota,', ', provinsi) as alamat_listing",false)
                        ->select("CONCAT(lokasi,', ',kab_kota) as alamat_listing", false)
                        ->select("penawaran as j_s")
                        ->select("jenis")
                        ->select("luas_tanah as lt")
                        ->select("luas_bangunan as lb")
                        ->select("bentuk as btk")
                        ->select("hadap as hdp")
                        ->select("lantai")
                        ->select("k_tidur as kt")
                        ->select("k_mandi as km")
                        ->select("ac")
                        ->select("telepon as telp")
                        ->select("listrik as list")
                        ->select("air")
                        ->select("harga_jual as jual")
                        ->select("harga_sewa as sewa")
                        ->select("jenis_sertifikat")
                        ->select("no_sertifikat")
                        ->from('p_property')
                        ->join('s_login', 's_login.id=p_property.agen', 'left')
                        ->join('u_kab_kota', 'u_kab_kota.id_kab_kota=p_property.kota', 'left')
                        ->join('u_provinsi', 'u_kab_kota.id_provinsi=u_provinsi.id_provinsi', 'left')
                        ->join('s_marketing', 's_login.marketing_id=s_marketing.id', 'left')
                        ->join('p_owner', 'p_owner.id=p_property.pemilik', 'left')
                        ->get()->result();
    }

    function monthly($isTotal=null) {
        if(null!=$isTotal)
            return $this->db
                ->select("CONCAT(COUNT(id),' UNIT') as unit", false)
                ->select("CONCAT('Rp ',FORMAT(SUM(harga_terjual),0)) as amount", false)
                ->from($this->table)
                ->where("SOLD IS NOT NULL", null, false)
                ->get()->row_array();
            return $this->db
                ->select("DATE_FORMAT(sold,'%b') as month", false)
                ->select("DATE_FORMAT(sold,'%Y') as year", false)
                ->select("CONCAT(COUNT(id),' UNIT') as unit", false)
                ->select("CONCAT('Rp ',FORMAT(SUM(harga_terjual),0)) as amount", false)
                ->from($this->table)
                ->where("SOLD IS NOT NULL", null, false)
                ->group_by("MONTH(sold)")
                ->group_by("YEAR(sold)")
                ->get()->result();
    }

    function countPublished() {
        $record = $this->db
                        ->select("COUNT(id) as total", false)
                        ->from($this->table)
                        ->where($this->common_filter)
                        ->get()->row_array();
        return $record['total'];
    }
    
    function slideShow($penawaran='jual')
    {
        return $this->db
                ->select('*')
                ->from($this->table)
                ->join('p_photo',"p_photo.property_id=$this->table.id",'LEFT')
                ->where('penawaran',$penawaran)
                ->where($this->common_filter)
                ->group_by("$this->table.id")
                ->limit(5)
                ->get()
                ->result();
    }

    function getTypes(){
        $this->db->distinct();
        return $this->db->select('jenis')->from($this->table)->get()->result();
    }

    function getYears(){
        $this->db->distinct();
        return $this->db
                ->select("DATE_FORMAT(sold,'%Y') as year",false)
                ->from($this->table)
                ->where('sold IS NOT NULL',null,false)
                ->get()->result();
    }
    
    function report_ma($tahun){
        foreach($this->getTypes() as $type)
            $selects[] = "CONCAT('Rp ',FORMAT(SUM(IF(jenis='$type->jenis',harga_terjual,0)),0)) as $type->jenis";
        
        $this->db
                ->select('s_marketing.nama as nama')
                ->select("DATE_FORMAT(sold,'%M') as month",false)
                ->select("DATE_FORMAT(sold,'%m-%b-%y') as sold",false);
        
        foreach ($selects as $select) $this->db->select($select,false);
        
        $this->db
                ->from($this->table)
                ->join('s_login', 'p_property.agen=s_login.id', 'left')
                ->join('s_marketing', 's_login.marketing_id=s_marketing.id', 'left')
                ->where("sold IS NOT NULL", null, false)
                ->where("YEAR(sold)",$tahun)
                ->group_by('sold');
        
        return $this->db->get()->result_array();
    }
}