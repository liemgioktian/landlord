<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Properties extends CI_Model {

    public $table = 'p_property';
    protected $table_img = 'p_photo';
    protected $table_ftr = 'p_fitur';
    protected $owner = array('id','nama','alamat','kontak');
    protected $autonumeric = array('harga_jual','harga_sewa','luas_tanah','luas_bangunan','k_tidur','k_mandi','listrik');

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        $this->load->database();
    }
    
    function dilihat($id)
    {
        $this->db
            ->where('id',$id)
            ->set('dilihat','dilihat+1',false)
            ->update('p_property');
    }
    
    function sold($property)
    {
//        $property['sold'] = date('Y-m-d H:i:s');
        $this->db->where('id',$property['id'])->update($this->table,$property);
    }
    
    public function draw($limit, $offset, $where = null)
    {
        $where['status'] = 1;
        if ($this->session->userdata('role'))
            if ($this->session->userdata('role') == 1)
                unset($where['status']);
        $this->db
                ->select($this->table . '.*')
                ->select('p_photo.photo')
                ->select('u_kab_kota.kab_kota')
                ->from($this->table)
                ->join('p_photo', $this->table . '.id=p_photo.property_id', 'left')
                ->join('u_kab_kota', $this->table . '.lokasi=u_kab_kota.id_kab_kota', 'left')
                ->where($where)
                ->limit($limit, $offset)
                ->group_by($this->table . '.id');
        return $this->db->get()->result();
    }

    public function select($item = null, $list = false)
    {
        $query = ($item == null) ? $this->db->get($this->table) : $this->db->get_where($this->table, $item);
        return ($list) ? $query->result() : $query->row_array();
    }

    public function selectComplete($pid)
    {
        $full = $this->db
                ->query(
                        "SELECT
                        p.*,
                        k.kab_kota,
                        pr.provinsi,
                        l.id as agenid, m.nama as nm_agen, m.office, m.mobile, m.fax, m.photo,
                        GROUP_CONCAT(ph.photo) as photos,
                        o.nama as nm_pemilik, o.alamat, o.kontak,
                        GROUP_CONCAT(fp.fitur_id) as fids,
                        GROUP_CONCAT(DISTINCT f.fitur) as fiturs
                        FROM
                        p_property p
                        LEFT JOIN u_kab_kota k ON p.kota=k.id_kab_kota
                        LEFT JOIN u_provinsi pr ON k.id_provinsi=pr.id_provinsi
                        LEFT JOIN s_login l ON p.agen=l.id
                        LEFT JOIN s_marketing m ON l.marketing_id=m.id                        
                        LEFT JOIN p_photo ph ON p.id=ph.property_id
                        LEFT JOIN p_owner o ON p.pemilik=o.id
                        LEFT JOIN p_fitur_property fp ON p.id=fp.property_id
                        LEFT JOIN p_fitur f ON fp.fitur_id=f.id
                        WHERE p.id=$pid"
                        );
        return $full->row_array();
    }

    public function insert($item)
    {
        if (isset($item['fitur'])) $fiturs = $item['fitur'];
        unset($item['photo']);
        unset($item['fitur']);
        $item['posted'] = date('Y-m-d H:i:s');
        
        # pemilik
        foreach($this->owner as $ownAttr) $owner[$ownAttr] = $item['pemilik'][$ownAttr];
        if(!$owner['id']>0 && $owner['nama']!=''){ 
            $this->db->insert('p_owner',$owner);
            $item['pemilik'] = $this->db->insert_id();
        }else $item['pemilik'] = $owner['id'];
        
        #autonumeric
        foreach($this->autonumeric as $num) $item[$num] = str_replace ('.', '', $item[$num]);
        
        # insert
        if($item['lokasi']!='') $this->db->insert($this->table, $item);
        $id = $this->db->insert_id();

        if ($id > 0)
        {
            # fitur
            if (isset($fiturs))foreach ($fiturs as $fitur)
                    $this->db->insert('p_fitur_property', array('property_id' => $id, 'fitur_id' => $fitur));
            
            # photos
            $this->load->library('upload');
            $this->upload->initialize(array("upload_path" => PHOTOSDIR, 'allowed_types' => 'gif|jpg|png'));
            if ($this->upload->do_multi_upload("photo"))
            {
                $photos = $this->upload->get_multi_upload_data();
                foreach ($photos as $photo)
                    $this->db->insert('p_photo', array('property_id' => $id, 'photo' => $photo['file_name']));
            }
        }
        return $id;
    }

    public function update($item)
    {
        if (isset($item['fitur'])) $fiturs = $item['fitur'];
        unset($item['photo']);
        unset($item['fitur']);
        
        # pemilik
        foreach(array('id','nama','alamat','kontak') as $ownAttr) $owner[$ownAttr] = $item['pemilik'][$ownAttr];
        if(!$owner['id']>0){ 
            $this->db->insert('p_owner',$owner);
            $item['pemilik'] = $this->db->insert_id();
        }else $item['pemilik'] = $owner['id'];
        
        #autonumeric
        $numeric = array('harga_jual','harga_sewa','luas_tanah','luas_bangunan','k_tidur','k_mandi','listrik');
        foreach($numeric as $num) $item[$num] = str_replace ('.', '', $item[$num]);
        
        # agen
        if($item['agen']=='') unset($item['agen']);
        
        # update
        $data_update = $item;
        unset($data_update['id']);
        $this->db->where('id',$item['id'])->update($this->table, $data_update);

        if ($item['id'] > 0)
        {
            # fitur
            # unset unselected fiturs
            if (isset($fiturs)){
                foreach ($fiturs as $fitur){
                    $pair = array('property_id' => $item['id'], 'fitur_id' => $fitur);
                    $exist = $this->db->where($pair)->get('p_fitur_property')->row_array();
                    if (empty($exist))$this->db->insert('p_fitur_property', $pair);
                }
                $this->db
                    ->where('property_id',$item['id'])
                    ->where_not_in('fitur_id',$fiturs)
                    ->delete('p_fitur_property');
            }else{
                $this->db
                    ->where('property_id',$item['id'])
                    ->delete('p_fitur_property');
            }
            
            # photos
            $this->load->library('upload');
            $this->upload->initialize(array("upload_path" => PHOTOSDIR, 'allowed_types' => 'gif|jpg|png'));
            if ($this->upload->do_multi_upload("photo"))
            {
                $photos = $this->upload->get_multi_upload_data();
                foreach ($photos as $photo)
                    $this->db->insert('p_photo', array('property_id' => $item['id'], 'photo' => $photo['file_name']));
            }
        }
    }

    function isExpired($property)
    {
        if(!isset($property['published'])) 
            $property = $this->db->get_where($this->table,$property)->row_array();
        $published = new DateTime($property['published']);# it's if null then now
        $now = new DateTime();
        $umur = $now->diff($published);
        $selisih = (int) $umur->format('%a');
        return ($now>$published && $selisih>=EXPIRED_DAYS);
    }
    
    function publicity($property)
    {
//        $published = new DateTime($property['published']);# it's if null then now
//        $now = new DateTime();
//        $umur = $now->diff($published);
//        $selisih = (int) $umur->format('%a');
//
//        $isExp = ($now>$published && $selisih>=EXPIRED_DAYS);
        $isExp = $this->isExpired($property);
        $isPublished = ($property['published']!=null);
        $isUnpublished = !$isPublished;

//        echo $isExp?'exp':'';
//        echo $isPublished?'published':'';
//        echo $isUnpublished?'unpublished':'';
//        die();

        if($isExp || $isUnpublished){
            $property['published'] = date('Y-m-d H:i:s');#publish or re-publish
            if($isUnpublished) $property['first_publish'] = $property['published'];
        }else if(!$isExp && $isPublished) $property['published'] = null;#unpublish
        
        $this->db->where('id',$property['id'])->update($this->table,$property);
    }
    
    public function delete($item)
    {
        $this->db->where('property_id', $item['id'])->delete('p_fitur_property');
        foreach($this->db->get_where('p_photo',array('property_id'=>$item['id']))->result() as $photo) 
                unlink (PHOTOSDIR.$photo->photo);
        $this->db->where('property_id', $item['id'])->delete('p_photo');
        return $this->db->where('id', $item['id'])->delete($this->table);
    }

    public function recyclebin($item){
        return $this->db->where('id',$item['id'])->set('active',0)->update($this->table);
    }
    
    public function restore($item){
        return $this->db->where('id',$item['id'])->set('active',1)->update($this->table);
    }
            
    function getFeatures()
    {
        return $this->db->get('p_fitur')->result();
    }
    
    function addRmvFitur($feature)
    {
        if(isset($feature['id'])){
            if($this->db->delete($this->table_ftr,$feature))
                return $this->db->where('fitur_id',$feature['id'])->delete('p_fitur_property');
        }else return $this->db->insert($this->table_ftr,$feature);
    }
    
    function getkotas($term)
    {
        return $this->db
                ->select('id_kab_kota as id',false)
                ->select('kab_kota as label',false)
                ->from('u_kab_kota')
                ->like('kab_kota',$term)
                ->limit(5)
                ->get()->result();
    }
    
    function getOwners($term)
    {
        return $this->db
                ->select('id')
                ->select('nama as label',false)
                ->select('alamat')
                ->select('kontak')
                ->from('p_owner')
                ->like('nama',$term)
                ->limit(5)
                ->get()->result();
    }
    
//    function getPhotos($id)
//    {return $this->db->where('property_id',$id)->get('p_photo')->result();}
    
    function getPhotos($item = null, $list = false)
    {
        $query = ($item == null) ? $this->db->get($this->table_img) : $this->db->get_where($this->table_img, $item);
        return ($list) ? $query->result() : $query->row_array();
    }
    
    function removePhoto($photoRecord)
    {
        #die(file_exists(PHOTOSDIR.$photoRecord['photo'])?'yes':'no');
        if(unlink(PHOTOSDIR.$photoRecord['photo'])) $this->db->where('id',$photoRecord['id'])->delete($this->table_img);
    }
 
    function sethot($id)
    {
        $prop = $this->select(array('id'=>$id), false);
        $this->db
            ->update($this->table,array('hot'=>$prop['hot']==0?1:0),"id = $id");
    }
}