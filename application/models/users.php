<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Users extends CI_Model{
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }
    public $table = 's_login';
    protected $agen = 's_marketing';
    protected $msg = 's_pesan';
            
    function getAgenList($term=null)
    {
        if(null!=$term){
            $this->db->like("$this->agen.nama",$term);
            $this->db->limit(5);
        }
        
        $result = $this->db
                ->select('*')
                ->select("$this->table.id as uid",false)
                ->select("(SELECT SUM(harga_terjual) FROM p_property WHERE agen = $this->table.id) as total_penjualan", false)
                ->from($this->table)
                ->join($this->agen,"$this->table.marketing_id=$this->agen.id",'right')
                ->order_by('total_penjualan','DESC')
                ->get()
                ->result();
//        die($this->db->last_query());
        return $result;
    }
    
    function getAgen($uid){
        $query = $this->db->query("SELECT * FROM s_marketing WHERE id=(SELECT marketing_id FROM $this->table WHERE id=$uid)");
        return $query->row_array();
    }
    
    public function select($item = null, $list = false) {
        $query = ($item == null) ? $this->db->get($this->table) : $this->db->get_where($this->table, $item);
        return ($list) ? $query->result() : $query->row_array();
    }

    public function insert($item) {
        $this->load->library('upload');
        $this->upload->initialize(array("upload_path" => PHOTOSDIR, 'allowed_types' => 'gif|jpg|png'));
        if ($this->upload->do_upload("photo_marketing"))
        {
            $photo = $this->upload->data();
            $item['agen']['photo'] = $photo['file_name'];
        }
        
        $this->db->insert($this->agen,$item['agen']);
        $item['login']['marketing_id'] = $this->db->insert_id();
        
        $this->db->insert($this->table, $item['login']);
    }

    public function simpleUpdate($item) {
        $this->db->where('id',$item['id']);
        unset($item['id']);
        $this->db->update($this->table,$item);
    }
    
    public function update($item) {
//        echo '<pre/>';
//        print_r($item);
//        die();
        $user = $this->select(array('id'=>$item['login']['id']),false);
        $agen = $this->getAgen($item['login']['id']);
        
        $this->load->library('upload');
        $this->upload->initialize(array("upload_path" => PHOTOSDIR, 'allowed_types' => 'gif|jpg|png'));
        if ($this->upload->do_upload("photo_marketing"))
        {
            $photo = $this->upload->data();
            $item['agen']['photo'] = $photo['file_name'];
            $this->deletePhoto($agen);
        }
        
        $this->db->where('id',$agen['id'])->update($this->agen,$item['agen']);
        if($item['login']['password']=='') unset($item['login']['password']);
        return $this->db->where('id', $item['login']['id'])->update($this->table, $item['login']);
    }

    public function delete($item) {
        $user = $this->select(array('id'=>$item['id']), false);
        if(empty($user)) return false;
        if($user['role']==1) return false;
        $agen = $this->getAgen($item['id']);
        if(!empty($agen)){
            $this->deletePhoto($agen);
            $this->db->where('id',$agen['id'])->delete($this->agen);
        }
        return $this->db->where('id', $item['id'])->delete($this->table);
    }
    
    function deletePhoto($agen)
    {
        if(null!=$agen['photo'] && ''!=$agen['photo'])
            if(file_exists(PHOTOSDIR.$agen['photo'])) 
                unlink (PHOTOSDIR.$agen['photo']);
    }
    
    function msgInsert($msg)
    {
        $this->db->insert($this->msg,$msg);
    }
    
    function getMsg($uid)
    {
        return $this->db->where('user_id',$uid)->order_by('dikirim','desc')
                #->limit(5)
                ->get($this->msg)->result();
    }
    
    function deleteMSG($msgId)
    {
        return $this->db->where('id',$msgId)->delete($this->msg);
    }
    
    function update_login($login){
        $id = $login['id'];
        unset($login['id']);
        $this->db->where('id', $id)->update($this->table, $login);
    }
}