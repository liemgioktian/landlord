<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class events extends CI_Model {
    
    protected $table = 's_event';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    function announce()
    {
        $ann = EVENT_ANNOUNCE;
        return $this->db
                ->query("SELECT * FROM $this->table where due_date > CURDATE() AND due_date < NOW() + INTERVAL $ann DAY ORDER BY due_date ASC")
                ->result();
//        return $this->db
//                ->where('due_date >','CURDATE()', null, false)
//                ->where('due_date <',"NOW() + INTERVAL $ann DAY", null, false)
////                ->where('due_date >','CURDATE()', null, false)
////                ->where('due_date <',"NOW() + INTERVAL $ann DAY", null, false)
//                ->order_by('due_date','desc')
//                ->get($this->table)
//                ->result();
    }
    
    public function select($item = null, $list = false)
    {
        $query = ($item == null) ? 
                $this->db->order_by('id','desc')->get($this->table) : 
                $this->db->get_where($this->table, $item);
        return ($list) ? $query->result() : $query->row_array();
    }
    
    function save($event)
    {
        if($event['title']=='') return false;
        $event['due_date'] = date('Y-m-d',strtotime($event['due_date']));
        if($event['id']!=0)#update
            $this->db->where('id',$event['id'])->update($this->table,$event);
        else {
            unset($event['id']);
            $this->db->insert($this->table,$event);
        }
    }
    
    function latest()
    {
        return $this->db
                ->select('*')
                ->from($this->table)
                ->order_by('due_date','DESC')
                ->limit(6)
                ->get()->result();
    }
    
    function delete($event)
    {
        $this->db->where('id',$event['id'])->delete($this->table);
    }
}
