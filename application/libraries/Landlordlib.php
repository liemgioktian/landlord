<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Landlordlib {
    

    function getAlias($property)
    {
        if(is_object($property)) $property = (array)$property;
        return strtoupper($property['jenis'] .' '. $property['bentuk'] .' DI '. $property['lokasi']);# .' '. $property['kab_kota']);
    }
    
    function imgSrc($fileName)
    {
        return $fileName!='' &&  $fileName!=NULL && file_exists('photos/'.$fileName)?
            base_url('photos/'.$fileName):
            base_url('themes/images/portfolio/1.jpg');
    }
    
    function gotoPropDetail($property){
        if(is_object($property)) $property = (array)$property;
        $pid = $property['id'];
        return site_url("property/show/$pid/". urlencode(str_replace(array(' ','.'), '-', $this->getAlias($property))));
    }
    
    function toRp($price)
    {
        return 'Rp '.number_format($price, 0 , ',' , '.' );
    }
    
}
