FYI : client minta dibikinin web baru buat upgrade dari web lama yang di landlordindonesia.com
karena untuk update listing 30'an marketing, lemot banget. Web lama corenya pake WP dengan plugin khusus
property but not custom build.

untuk referensi fitur standar seperti web
www.rumah123.com
www.eraindonesia.com


Requirement :
Main Feature :

1. Fitur standard yang sudah ada di web landlordindonesia.com, hanya dioptimasi supaya saat marketing update listing secara bersamaan 
   web tidak down/lemot.

2. Marketing memperoleh fitur standar untuk insert new listing, edit personal profile tapi tidak bisa edit, update, delete property.

3. Fitur Edit, Update & Delete hanya diberikan untuk 1 user Admin.

4. Area khusus untuk info event yang akan diadakan, bila event sudah mendekati Due Date dibuat semacam pop-up di front page.

5. Fitur reporting (supaya si admin bisa extract langsung report property dari database website baik daily, weekly, monthly, yearly, etc).
   Format report menyusul. Termasuk reporting untuk marketing jadi bisa diperoleh data penjualan marketing, mrketing dgn listing terbanyak, 
   marketing teraktif, dll .

6. Fitur Expired & Renew Property (semisal batas waktu sebuah property bisa ditampilkan di listing 6 bulan, maka melewati 6 bulan listing akan expired,
   tapi at least 1 bulan sebelum masa expired muncul notifikasi buat si admin bahwa property akan expired)

7. Penambahan kolom "dokumen" pada form input new listing property. (maksudnya biar nanti diisi dokumen penyerta yang dipakai buat masukin listing apakah 
   pake sertifikat tanah, KTP, dll.)


