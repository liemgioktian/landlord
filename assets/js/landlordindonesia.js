var $form_filter = $('#form-filter');
var $form_sorting = $('#form-sorting');
var current_url = window.location.href;

if($form_filter.length>0){
    $form_filter.find('input').not('[name="fake_kota"]').each(function(){
        $(this).keyup(function(){
            applyFilter();
       });
    });
    
    $('[name="kota"]').siblings('input').autocomplete({
        source: $site_url+'/property/autokota',
        open:function(){
            $('[name="kota"]').val(0);
        },
        select: function(event, ui) {
            $('[name="kota"]').val(ui.item.id);
            applyFilter();
        }
    });

    $('select').each(function(){
        $(this).change(function(){
            applyFilter();
        });
    });
    
    var $ascdsc = $('[name="ascdesc"]');
    $ascdsc.siblings('a.btn').click(function(){
       var $ai = $(this).find('i');
       var $was = $ai.hasClass('icon-chevron-down')?'desc':'asc' ;
       var $will= $was==='desc'?'asc':'desc';
       $ai.removeClass($was==='desc'?'icon-chevron-down':'icon-chevron-up');
       $ai.addClass($will==='desc'?'icon-chevron-down':'icon-chevron-up');
       $ascdsc.val($will);
       applyFilter();
    });
}

function getItems($offset) {
    $filter = getFormData($form_filter);
    $sorting = getFormData($form_sorting);
    $limit = 6;
    $.get('', {offset:$offset, limit:$limit, filter:$filter, sorting:$sorting}, 
    function(items) {
        if (items !== '') {
            $('ul.thumbnails').append(items);
            $('ul.thumbnails li:first').waypoint(function() {
                getItems($limit + $offset);
            }, {triggerOnce: true});
        } else $('.loading_bar > .well').html('- Selesai Memuat -');
    });
}

var $sub_title = $('#pageTitle small').text();
if ($('div.span9 ul.thumbnails').length > 0) setTimeout(applyFilter, 500) //resetFilter(); // init-load

function applyFilter()
{
    if(current_url==$site_url+'/listing')
        $('#pageTitle small').html('Filter Property');
    $('ul.thumbnails li').remove();
    getItems(0);
    if($('[name="kota"]').val()<1)$('[name="kota"]').siblings('input').val('');// handle autocomplete
    return false;
}

function resetFilter()
{
    $($form_filter).find('input').val('');
    $('select').each(function(){
        $(this).find('option').eq(0).attr('selected','selected');
    });
    $('#pageTitle small').html($sub_title);
    $('ul.thumbnails li').remove(); 
    getItems(0);
    return false;
}

function toRp(a, b, c, d, e) {
    e = function(f) {
        return f.split('').reverse().join('');
    };
    b = e(parseInt(a, 10).toString());
    for (c = 0, d = ''; c < b.length; c++) {
        d += b[c];
        if ((c + 1) % 3 === 0 && c !== (b.length - 1)) {
            d += '.';
        }
    }
    return e(d);
}

$('#main_menu').ready(function() {
    $('#main_menu > li').each(function() {
        if ($(this).find('a').attr('href') === current_url)
            $(this).addClass('active');
        else
            $(this).removeClass('active');
    });
});

function getFormData($form) {
    $data = new Object();
    $form.find('input,select').each(function() {
        $data[$(this).attr('name')] = $(this).val();
    });
    return $data;
}

$('[name="btn-login"]').click(function() {
    $.post($site_url + '/auth/login', getFormData($('[name="login-form"]')), function(r) {
        if (r.error)
            $('#login-alert').removeClass('hidden').find('i').html(r.error);
        else
            window.location = r.token;
    }, 'json');
    return false;
});

$('[name="btn-forgot"]').click(function() {
    $.post($site_url + '/auth/forgot', getFormData($('[name="login-form"]')), function(r) {
        $('#login-alert').removeClass('hidden').find('i').html(r.error);
    }, 'json');
    return false;
});

if ($('[name^="harga_"]').length > 0)
    $('[name^="harga_"]').autoNumeric({aSep: '.', aDec: ',', aPad: false, vMax: 100000000000});
if ($('[name^="luas_"]').length > 0)
    $('[name^="luas_"]').autoNumeric({aSep: '.', aDec: ',', aPad: false});
if ($('[name^="k_"]').length > 0)// k_tidur, k_mandi
    $('[name^="k_"]').autoNumeric({aSep: '.', aDec: ',', aPad: false});
if ($('[name="listrik"]').length > 0)
    $('[name="listrik"]').autoNumeric({aSep: '.', aDec: ',', aPad: false});
if ($('[name="tambah-photo"]').length > 0) {
    function telusur() {
        $('.telusur_photo').on('click', function() {
            $(this).siblings('[type="file"]').trigger('click').on('change', function() {
                $(this).siblings('[type="text"]').val($(this).val());
            });
            return false;
        });
    }
    telusur();
    $('[name="tambah-photo"]').on('click', function() {
        $(this).parent().parent().prepend(
                '<div class="input-append" style="margin:5px 0 5px 0;">' +
                '<input type="file" style="display: none" name="photo[]" />' +
                '<input type="text" class="input-medium" disabled="disabled" />' +
                '<button class="btn btn-primary telusur_photo"><i class="icon-folder-open"></i></button>' +
                '<button class="btn btn-danger kurangi_photo"><i class="icon-remove-circle"></i></button>' +
                '</div>'
                );
        $(this).parent().css('margin-top', '5px');
        $('.kurangi_photo').on('click', function() {
            $(this).parent().remove();
            return false;
        });
        telusur();
        return false;
    });
}

function rusure(pesan)
{
    $('.rusure').confirmation({
        singleton: true,
        title: pesan,
        btnOkLabel: '<i class="icon-ok-sign icon-white"></i> Ya',
        btnCancelLabel: '<i class="icon-remove-sign"></i> Tidak'
    });
}

$('.autoc-change').click(function() {
    $(this).siblings('input')
            .val('')
            .removeAttr('disabled')
            .removeClass('input-medium').addClass('input-large')
            .focus();
    $(this).addClass('hidden');
    return false;
});

if($('[name="kota"]').length>0 && $('[name="frmprprt"]').length>0)
    $('[name="kota"]').siblings('input').autocomplete({
        source: $site_url+'/property/autokota',
        select: function(event, ui) {
            $('[name="kota"]').val(ui.item.id)
                .siblings('input')
                .attr('disabled', 'disabled')
                .removeClass('input-large').addClass('input-medium')
                .next('button').removeClass('hidden');
        }
    });

if($('[name="pemilik[id]"]').length>0)
$('[name="pemilik[id]"]').siblings('input').autocomplete({
    source: $site_url+'/property/autoOwner',
            select: function(event, ui) {
            $('[name="pemilik[id]"]').val(ui.item.id);
            $('[name="pemilik[alamat]"]').val(ui.item.alamat);
            $('[name="pemilik[kontak]"]').val(ui.item.kontak);
            $('[name^="pemilik"]').attr('disabled', 'disabled');
            $('[name="pemilik[nama]"]').removeClass('input-large')
                .addClass('input-medium').next('button').removeClass('hidden');
    }
});

$('.owner-change').click(function() {
    $('[name^="pemilik"]').val('').removeAttr('disabled');
    $('[name="pemilik[nama]"]').removeClass('input-medium')
        .addClass('input-large').focus();
    $(this).addClass('hidden');
    return false;
});

function submitProperty($form){
    $form.find('input').removeAttr('disabled');
}

function agentRepublishProperty($id)
{
    $.get($site_url+'/property/setPublicity/'+$id,{},function(){
        applyFilter();
    });
}

function markAsSold($id)
{
    $('#sold').modal('show').find('form')//.attr('action',$site_url+'/property/sold/'+$id)
    .submit(function(e){
        $.post($site_url+'/property/sold/'+$id,
        $(this).serialize(),function(){
            $('#sold').modal('hide');
            if($('div.span9 ul.thumbnails').length>0)applyFilter();
            else if($table.length>0) $table.fnDraw();
        });
        return false;
    }).find('input').val('');
}

if($('.date').length > 0)$('.date').datepicker({dateFormat: 'dd-mm-yy'});

if($('#latesteventcontainer').length>0)
    $.ajax({
       url:$site_url+'/event/footer',
       type:'GET',
       dataType:'text',
       success:function(eventlist){
           $('#latesteventcontainer').html(eventlist);
       }
    });

if($('.slideprop').length > 0){// homepage
    $('.slideprop').each(function(){
        if($(this).find('.item').length > 0)
            $(this).carousel({
                interval: 3000
            });
    })
}

if($('#frmevt').length>0){
    $('#frmevt').submit(function(){
        var $valid = true;
        $(this).find('input').each(function(){
            if($(this).val()==''){
                $(this).css('border','1px solid red').focus();
                $valid = false;
            }
        });
        return $valid;
    });
}

if($('[name="frm-usr"]').length>0){
    $('[name="frm-usr"]').submit(function(){
        
        var $valid = true;
        var uri_segment = current_url.split('/');
        var last_segment= uri_segment[uri_segment.length-1];
        var $pwd = $('[name="login[password]"]');
        var $cpwd = $('[name="login[cpassword]"]');
        
        if($pwd.val() != $cpwd.val()) {
            $cpwd.val('').css('border', '1px solid red').focus();
            $valid = false;
        }
        
        if(last_segment=='profile'){
            if($pwd.val()==='') {
                $pwd.css('border', '1px solid red').focus();
                $valid = false;
            }
        }
        
        $(this).find('[name="agen[nama]"],[name="login[username]"]').each(function(){
            if($(this).val()==''){
                $(this).css('border','1px solid red').focus();
                $valid = false;
            }
        });
        return $valid;
    });
}

if($('[name="frmprprt"]').length>0){
    $('[name="frmprprt"]').submit(function(){
        var $valid = true;
        
        if($('[name="lokasi"]').val()==''){
            $('[name="lokasi"]').css('border','1px solid red').focus();
            $valid = false;
        }
        if($('[name="penawaran"]').val()=='jual'){
            if($('[name="harga_jual"]').val()<1){
                $('[name="harga_jual"]').css('border','1px solid red').focus();
                $valid = false;
            }
        }else if($('[name="penawaran"]').val()=='sewa'){
            if($('[name="harga_sewa"]').val()<1){
                $('[name="harga_sewa"]').css('border','1px solid red').focus();
                $valid = false;
            }
        }else{
            $('[name="harga_jual"],[name="harga_sewa"]').each(function(){
                if($(this).val()<1){
                    $(this).css('border','1px solid red').focus();
                    $valid = false;
                }
            });
        }
        return $valid;
    });
}