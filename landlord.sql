/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50516
Source Host           : localhost:3306
Source Database       : landlord

Target Server Type    : MYSQL
Target Server Version : 50516
File Encoding         : 65001

Date: 2014-08-11 05:02:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `p_fitur`
-- ----------------------------
DROP TABLE IF EXISTS `p_fitur`;
CREATE TABLE `p_fitur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fitur` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of p_fitur
-- ----------------------------
INSERT INTO `p_fitur` VALUES ('1', 'Listrik 6600 VA');
INSERT INTO `p_fitur` VALUES ('2', '2 Lantai');
INSERT INTO `p_fitur` VALUES ('3', '2 Garasi');
INSERT INTO `p_fitur` VALUES ('4', 'Kamar Mandi Dalam');
INSERT INTO `p_fitur` VALUES ('5', 'Listrik 450 VA');
INSERT INTO `p_fitur` VALUES ('6', 'Kolam Renang');
INSERT INTO `p_fitur` VALUES ('7', 'Taman Bermain');
INSERT INTO `p_fitur` VALUES ('9', 'Helipad');
INSERT INTO `p_fitur` VALUES ('17', 'Ball Room');
INSERT INTO `p_fitur` VALUES ('18', 'Lapangan Golf');
INSERT INTO `p_fitur` VALUES ('19', 'Billiard');

-- ----------------------------
-- Table structure for `p_fitur_property`
-- ----------------------------
DROP TABLE IF EXISTS `p_fitur_property`;
CREATE TABLE `p_fitur_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `fitur_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of p_fitur_property
-- ----------------------------
INSERT INTO `p_fitur_property` VALUES ('8', '1', '7');

-- ----------------------------
-- Table structure for `p_owner`
-- ----------------------------
DROP TABLE IF EXISTS `p_owner`;
CREATE TABLE `p_owner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `kontak` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of p_owner
-- ----------------------------
INSERT INTO `p_owner` VALUES ('1', 'Henri Susanto', 'Jl. Abimanyu 220', '081901088918');
INSERT INTO `p_owner` VALUES ('2', '', '', '');
INSERT INTO `p_owner` VALUES ('3', '', '', '');
INSERT INTO `p_owner` VALUES ('4', '', '', '');
INSERT INTO `p_owner` VALUES ('5', '', '', '');
INSERT INTO `p_owner` VALUES ('6', '', '', '');
INSERT INTO `p_owner` VALUES ('7', '', '', '');
INSERT INTO `p_owner` VALUES ('8', '', '', '');
INSERT INTO `p_owner` VALUES ('9', '', '', '');
INSERT INTO `p_owner` VALUES ('10', '', '', '');
INSERT INTO `p_owner` VALUES ('11', '', '', '');
INSERT INTO `p_owner` VALUES ('12', '', '', '');
INSERT INTO `p_owner` VALUES ('13', '', '', '');
INSERT INTO `p_owner` VALUES ('14', '', '', '');
INSERT INTO `p_owner` VALUES ('15', '', '', '');
INSERT INTO `p_owner` VALUES ('16', '', '', '');
INSERT INTO `p_owner` VALUES ('17', '', '', '');
INSERT INTO `p_owner` VALUES ('18', '', '', '');
INSERT INTO `p_owner` VALUES ('19', '', '', '');
INSERT INTO `p_owner` VALUES ('20', '', '', '');
INSERT INTO `p_owner` VALUES ('21', '', '', '');
INSERT INTO `p_owner` VALUES ('22', '', '', '');
INSERT INTO `p_owner` VALUES ('23', '', '', '');
INSERT INTO `p_owner` VALUES ('24', 'Gondes Margondes', 'Jl. Buntu Tak bisa masuk', '0812489238252');
INSERT INTO `p_owner` VALUES ('25', 'noname', 'asgdhd', '345364');
INSERT INTO `p_owner` VALUES ('26', 'Paulus Aditya', 'jkagfakjg', '2893579827');
INSERT INTO `p_owner` VALUES ('27', '', '', '');
INSERT INTO `p_owner` VALUES ('28', '', '', '');
INSERT INTO `p_owner` VALUES ('29', '', '', '');

-- ----------------------------
-- Table structure for `p_photo`
-- ----------------------------
DROP TABLE IF EXISTS `p_photo`;
CREATE TABLE `p_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `photo` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of p_photo
-- ----------------------------
INSERT INTO `p_photo` VALUES ('1', '1', 'Image001.jpg');
INSERT INTO `p_photo` VALUES ('2', '1', 'Foto-0008.jpg');
INSERT INTO `p_photo` VALUES ('4', '1', 'Foto-0168.jpg');
INSERT INTO `p_photo` VALUES ('5', '6', 'beranda-belakang.jpg');
INSERT INTO `p_photo` VALUES ('6', '6', 'A-Yani-Melanie.jpg');
INSERT INTO `p_photo` VALUES ('7', '7', 'Merapi.jpg');
INSERT INTO `p_photo` VALUES ('8', '7', 'K-Mandi-bawah.jpg');
INSERT INTO `p_photo` VALUES ('11', '5', 'tampak-mix4.jpg');
INSERT INTO `p_photo` VALUES ('12', '5', 'R-Keluarga.jpg');
INSERT INTO `p_photo` VALUES ('13', '5', 'R-Bilyard.jpg');

-- ----------------------------
-- Table structure for `p_property`
-- ----------------------------
DROP TABLE IF EXISTS `p_property`;
CREATE TABLE `p_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agen` int(11) NOT NULL COMMENT 'user id saja',
  `posted` date NOT NULL COMMENT 'dipakai jg utk menentukan expired, berisi jg data re-publish',
  `published` date DEFAULT NULL,
  `first_publish` date DEFAULT NULL,
  `sold` date DEFAULT NULL,
  `pemilik` int(11) NOT NULL,
  `lokasi` varchar(255) NOT NULL,
  `kota` int(11) DEFAULT NULL,
  `jenis` varchar(255) NOT NULL COMMENT 'rumah, gedung',
  `penawaran` varchar(255) NOT NULL COMMENT '1:jual 0:beli',
  `luas_tanah` int(11) DEFAULT NULL COMMENT 'tanah 200m<sup>2</sup>, bangunan 150m<sup>2</sup>',
  `luas_bangunan` int(11) DEFAULT NULL COMMENT 'tanah 200m<sup>2</sup>, bangunan 150m<sup>2</sup>',
  `bentuk` varchar(255) NOT NULL,
  `hadap` varchar(255) NOT NULL,
  `lantai` double NOT NULL COMMENT '1, 1.5, 2, .., 4.1',
  `k_tidur` varchar(255) DEFAULT NULL COMMENT '2 + 3',
  `k_mandi` varchar(255) DEFAULT NULL,
  `ac` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'ada/tidak, drpd bingung pk apa qty',
  `telepon` tinyint(4) NOT NULL DEFAULT '0',
  `listrik` int(11) NOT NULL DEFAULT '0',
  `air` varchar(255) NOT NULL COMMENT 'pdam, artetis',
  `harga_jual` bigint(20) DEFAULT NULL,
  `harga_sewa` bigint(20) DEFAULT NULL,
  `harga_terjual` bigint(20) DEFAULT NULL,
  `jenis_sertifikat` varchar(255) DEFAULT NULL COMMENT 'SHM, HGB, HP, HS, Lainnya (PPJB, Girik, Adat)',
  `no_sertifikat` varchar(255) DEFAULT NULL COMMENT 'SHM, HGB, HP, HS, Lainnya (PPJB, Girik, Adat)',
  `keterangan` varchar(255) DEFAULT NULL,
  `kondisi` varchar(255) NOT NULL COMMENT 'istimewa, bagus, butuh renovasi, sudah renovasi',
  `dokumen` varchar(255) DEFAULT NULL,
  `dilihat` int(11) NOT NULL,
  `hot` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'set it to 0 to send it to trash/recyclebin',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of p_property
-- ----------------------------
INSERT INTO `p_property` VALUES ('1', '2', '2014-01-18', '2014-05-17', '2014-05-17', null, '7', 'Suramadu', '194', 'Rumah', 'sewa', '150', '125', 'Joglo', 'Barat', '2', '8', '16', '1', '1', '9000', 'artetis', '0', '95000000', null, 'HP (Hak Pakai)', '88.1988.801.446', '', 'sudah renovasi', 'KTP', '266', '1', '1');
INSERT INTO `p_property` VALUES ('2', '2', '2014-01-18', '2014-05-17', '2014-05-17', '2014-02-08', '14', 'Suralaya', '196', 'rumah', 'jual', '0', '0', '', 'timur selatan', '1', '', '', '0', '0', '0', 'PAM/PDAM', '0', '0', null, 'SHM (Sertifikat Hak Milik)', '', '', 'istimewa', '', '48', '0', '1');
INSERT INTO `p_property` VALUES ('3', '1', '2014-01-18', '2014-05-17', '2014-05-17', '2014-04-17', '13', 'Jolodoro', '0', 'Rumah', 'jual', '0', '0', '', '', '1', '', '', '0', '0', '0', 'PAM/PDAM', '0', '0', '220000000', 'SHM (Sertifikat Hak Milik)', '', '', 'istimewa', '', '9', '0', '1');
INSERT INTO `p_property` VALUES ('5', '4', '2014-01-19', '2014-05-17', '2014-05-17', null, '27', 'Madukismo', '0', 'Rumah', 'jual', '0', '0', '', '', '1', '', '', '0', '0', '0', 'PAM/PDAM', '90000', '0', null, 'SHM (Sertifikat Hak Milik)', '', '', 'istimewa', '', '19', '1', '1');
INSERT INTO `p_property` VALUES ('6', '1', '2014-01-19', '2014-05-17', '2014-05-17', '2014-04-15', '17', '7628 Southfield Dr.', '0', 'Rumah', 'jual', '0', '0', '', '', '1', '', '', '0', '0', '0', 'PAM/PDAM', '900000', '0', '200000000', 'SHM (Sertifikat Hak Milik)', '', '', 'istimewa', '', '134', '0', '1');
INSERT INTO `p_property` VALUES ('7', '2', '2014-01-19', '2014-05-17', '2014-05-17', '2014-04-16', '23', 'Jayabaya', '0', 'Rumah', 'jual_sewa', '20000', '10000', '', '', '1.5', '2000', '1000', '1', '1', '2000', 'PAM/PDAM', '4000000000', '4000000', '150000000', 'SHM (Sertifikat Hak Milik)', '', '', 'istimewa', '', '34', '0', '1');
INSERT INTO `p_property` VALUES ('8', '2', '2014-01-25', '2014-05-17', '2014-05-17', '2014-02-08', '28', 'sesuatu', '2', 'Rumah', 'jual', '0', '0', '', '', '1', '', '', '0', '0', '0', 'PAM/PDAM', '0', '0', null, 'SHM (Sertifikat Hak Milik)', '', '', 'istimewa', '', '18', '0', '1');
INSERT INTO `p_property` VALUES ('10', '5', '2014-04-16', '2014-04-16', '2014-04-16', null, '25', 'semarang', '1', 'Rumah', 'jual', '1241', '234', 'segitiga', 'selatan', '1', '2', '2', '0', '0', '1200', 'PAM/PDAM', '400000000', '0', null, 'SHM (Sertifikat Hak Milik)', '12142325', 'dfgdhdhd', 'istimewa', '', '12', '1', '1');
INSERT INTO `p_property` VALUES ('11', '4', '2014-04-16', '2014-05-17', '2014-05-17', '2014-04-15', '29', 'semarang', '185', 'Apartemen', 'jual', '244', '234', 'kotak', 'timur', '1', '3', '1', '1', '1', '1200', 'PAM/PDAM', '750000000', '0', '200000000', 'SHM (Sertifikat Hak Milik)', '124314', 'afhkajhfkagkbkbmkbja', 'istimewa', 'ktp', '11', '0', '1');

-- ----------------------------
-- Table structure for `s_event`
-- ----------------------------
DROP TABLE IF EXISTS `s_event`;
CREATE TABLE `s_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `due_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of s_event
-- ----------------------------
INSERT INTO `s_event` VALUES ('3', 'Soft Opening Landlord Indonesia di Mall Ciputra', 'kurma yang digantung', '2014-02-22');
INSERT INTO `s_event` VALUES ('4', 'Event yang diselenggarakan kemarin', 'tumbuklah 5biji habatus saudah, kemudian teteskanlah', '2014-02-15');
INSERT INTO `s_event` VALUES ('5', 'Event Untuk Besok', '', '2014-03-09');
INSERT INTO `s_event` VALUES ('6', 'Pameran IFBC Expo 2013', '<p><img alt=\"\" src=\"http://localhost/landlord/assets/images/images/Biografi%20Sutomo%20%28Bung%20Tomo%29.jpg\" style=\"float:left; height:320px; width:234px\" /></p>\r\n\r\n<p>Landlord Property Industry sebagai peserta Pameran IFBC (Info Franchise Business Concept) mengundang Bapak/Ibu untuk hadir dan berkunjung ke angenda tahunan, Pameran Franchice dan Peluang Usaha nasional Terbesar di Semarang.</p>\r\n\r\n<p>National Road Show, Info franchise &amp; Business Concept Expo 2013, dengan Thema, &ldquo;Powerful Business Concept&rdquo;.</p>\r\n\r\n<p>Hari/tanggal: Jumat &ndash; Minggu, 17 &ndash; 19 Mei 2013</p>\r\n\r\n<p>Pukul : 10.00 &ndash; 21.00 WIB</p>\r\n\r\n<p>Tempat : Gedung wanita, Jl. Sriwijaya no. 29 semarang.</p>\r\n\r\n<p>Dapatkan informasi menarik dan diskon fantastis dari Landlord Property Industry.</p>\r\n', '2014-04-17');
INSERT INTO `s_event` VALUES ('7', 'satu lagi', '<p>none of this time</p>\r\n', '2014-04-18');
INSERT INTO `s_event` VALUES ('8', 'dipepeki enem bae lah', '', '2014-04-18');
INSERT INTO `s_event` VALUES ('10', 'mouse', '', '2014-04-18');

-- ----------------------------
-- Table structure for `s_login`
-- ----------------------------
DROP TABLE IF EXISTS `s_login`;
CREATE TABLE `s_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` tinyint(1) NOT NULL COMMENT '0:marketing 1:admin',
  `marketing_id` int(11) DEFAULT NULL,
  `last_login` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of s_login
-- ----------------------------
INSERT INTO `s_login` VALUES ('1', 'admin', 'admin@redawning.com', '21232f297a57a5a743894a0e4a801fc3', '1', null, '2014-08-10 23:26:09');
INSERT INTO `s_login` VALUES ('2', 'agen', 'hannie@dewi.ika', '941730a7089d81c58c743a7577a51640', '0', '1', '2014-04-19 15:13:09');
INSERT INTO `s_login` VALUES ('4', 'ma_melanie', 'marketing@landlordindonesia.com', '202cb962ac59075b964b07152d234b70', '0', '3', '2014-05-23 00:28:08');
INSERT INTO `s_login` VALUES ('5', 'aan', 'aan@landlordindonesia.com', '202cb962ac59075b964b07152d234b70', '0', '4', '0000-00-00 00:00:00');
INSERT INTO `s_login` VALUES ('6', 'donna', 'donna@landlordindonesia.com', '250cf8b51c773f3f8dc8b4be867a9a02', '0', '5', '0000-00-00 00:00:00');
INSERT INTO `s_login` VALUES ('7', 'wibowo', 'wibowo@landlordindonesia.com', '025134e69e103a3d2cce2dbbd3e88f8f', '0', '6', '0000-00-00 00:00:00');
INSERT INTO `s_login` VALUES ('8', 'sally', 'sally@landlord.com', 'f85c5dd0219719162ba3104641445bf9', '0', '7', '2014-05-18 13:38:52');
INSERT INTO `s_login` VALUES ('15', 'henri', 'henri@susan.to', '202cb962ac59075b964b07152d234b70', '0', '14', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `s_marketing`
-- ----------------------------
DROP TABLE IF EXISTS `s_marketing`;
CREATE TABLE `s_marketing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `office` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of s_marketing
-- ----------------------------
INSERT INTO `s_marketing` VALUES ('1', 'Hannie Ika Dewi', '(0285) 577736', '087832607886', '(0285) 577736', 'Foto-0072.jpg', 'https://www.facebook.com/hannie.dewi', '');
INSERT INTO `s_marketing` VALUES ('3', 'Elizabeth Melanie', '024 - 8645 27 77', '0821 3660 0099', '024 - 8645 30 30', 'Melanie.jpg', 'https://www.facebook.com/hannie.dewi', 'https://www.twitter.com/hannie.dewi');
INSERT INTO `s_marketing` VALUES ('4', 'Aan Adi Hartadi', '024 - 8645 27 77', '0821 3660 0099', '024 - 8645 30 30', 'aan.jpg', 'https://www.facebook.com/hannie.dewi', '');
INSERT INTO `s_marketing` VALUES ('5', 'Mara Donna Zalianty', '(0285) 577736', '087832607886', '(0285) 577736', '45714411_13085272_53181927cdb68.jpg', '', '');
INSERT INTO `s_marketing` VALUES ('6', 'wibowo', '024 - 8645 27 77', '087832607886', '024 - 8645 30 30', 'hb_wibowo.jpg', null, null);
INSERT INTO `s_marketing` VALUES ('7', 'Saliyati', '911', '081081081', '3220', 'saliyati.jpg', 'https://www.facebook.com/hannie.dewi', 'https://www.twitter.com/hannie.dewi');
INSERT INTO `s_marketing` VALUES ('14', 'henri', '123', '456', '789', null, 'fb', 'tw');

-- ----------------------------
-- Table structure for `s_pesan`
-- ----------------------------
DROP TABLE IF EXISTS `s_pesan`;
CREATE TABLE `s_pesan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `dikirim` datetime DEFAULT NULL,
  `pesan` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of s_pesan
-- ----------------------------
INSERT INTO `s_pesan` VALUES ('1', '1', '2014-02-05 23:42:27', 'much !');

-- ----------------------------
-- Table structure for `tmp`
-- ----------------------------
DROP TABLE IF EXISTS `tmp`;
CREATE TABLE `tmp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tmp
-- ----------------------------

-- ----------------------------
-- Table structure for `u_kab_kota`
-- ----------------------------
DROP TABLE IF EXISTS `u_kab_kota`;
CREATE TABLE `u_kab_kota` (
  `id_kab_kota` int(11) NOT NULL AUTO_INCREMENT,
  `id_provinsi` int(11) NOT NULL,
  `kab_kota` varchar(100) NOT NULL,
  PRIMARY KEY (`id_kab_kota`)
) ENGINE=MyISAM AUTO_INCREMENT=441 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of u_kab_kota
-- ----------------------------
INSERT INTO `u_kab_kota` VALUES ('1', '1', 'KAB. ACEH SELATAN\n');
INSERT INTO `u_kab_kota` VALUES ('2', '1', 'KAB. ACEH TENGGARA\n');
INSERT INTO `u_kab_kota` VALUES ('3', '1', 'KAB. ACEH TIMUR\n');
INSERT INTO `u_kab_kota` VALUES ('4', '1', 'KAB. TENGAH \n');
INSERT INTO `u_kab_kota` VALUES ('5', '1', 'KAB. ACEH BARAT\n');
INSERT INTO `u_kab_kota` VALUES ('6', '1', 'KAB. ACEH BESAR\n');
INSERT INTO `u_kab_kota` VALUES ('7', '1', 'KAB. PIDIE\n');
INSERT INTO `u_kab_kota` VALUES ('8', '1', 'KAB. ACEH UTARA\n');
INSERT INTO `u_kab_kota` VALUES ('9', '1', 'KAB. SIMEULUE\n');
INSERT INTO `u_kab_kota` VALUES ('10', '1', 'KAB. ACEH SINGKIL\n');
INSERT INTO `u_kab_kota` VALUES ('11', '1', 'KAB. BIREUN\n');
INSERT INTO `u_kab_kota` VALUES ('12', '1', 'KAB. ACEH BARAT DAYA\n');
INSERT INTO `u_kab_kota` VALUES ('13', '1', 'KAB. GAYO LUES\n');
INSERT INTO `u_kab_kota` VALUES ('14', '1', 'KAB. ACEH JAYA\n');
INSERT INTO `u_kab_kota` VALUES ('15', '1', 'KAB. NAGAN JAYA\n');
INSERT INTO `u_kab_kota` VALUES ('16', '1', 'KAB. ACEH TAMIANG\n');
INSERT INTO `u_kab_kota` VALUES ('17', '1', 'KAB. BENER MERIAH\n');
INSERT INTO `u_kab_kota` VALUES ('18', '1', 'KOTA BANDA ACEH\n');
INSERT INTO `u_kab_kota` VALUES ('19', '1', 'KOTA SABANG\n');
INSERT INTO `u_kab_kota` VALUES ('20', '1', 'KOTA LHOKSEUMAWE\n');
INSERT INTO `u_kab_kota` VALUES ('21', '1', 'KOTA LANGSA\n');
INSERT INTO `u_kab_kota` VALUES ('22', '2', 'KAB. TAPANULI TENGAH\n');
INSERT INTO `u_kab_kota` VALUES ('23', '2', 'KAB. TAPANULI UTARA\n');
INSERT INTO `u_kab_kota` VALUES ('24', '2', 'KAB. TAPANULI SELATAN\n');
INSERT INTO `u_kab_kota` VALUES ('25', '2', 'KAB. NIAS\n');
INSERT INTO `u_kab_kota` VALUES ('26', '2', 'KAB. LANGKAT\n');
INSERT INTO `u_kab_kota` VALUES ('27', '2', 'KAB. KARO\n');
INSERT INTO `u_kab_kota` VALUES ('28', '2', 'KAB. DELI SERDANG\n');
INSERT INTO `u_kab_kota` VALUES ('29', '2', 'KAB. SIMALUNGUN\n');
INSERT INTO `u_kab_kota` VALUES ('30', '2', 'KAB. ASAHAN\n');
INSERT INTO `u_kab_kota` VALUES ('31', '2', 'KAB. LABUHAN BATU\n');
INSERT INTO `u_kab_kota` VALUES ('32', '2', 'KAB. DAIRI\n');
INSERT INTO `u_kab_kota` VALUES ('33', '2', 'KAB. TOBA SAMOSIR\n');
INSERT INTO `u_kab_kota` VALUES ('34', '2', 'KAB. MANDAILING NATAL\n');
INSERT INTO `u_kab_kota` VALUES ('35', '2', 'KAB. NIAS SELATAN\n');
INSERT INTO `u_kab_kota` VALUES ('36', '2', 'KAB. PAKPAK BARAT\n');
INSERT INTO `u_kab_kota` VALUES ('37', '2', 'KAB. HUMBANG HASUNDUTAN\n');
INSERT INTO `u_kab_kota` VALUES ('38', '2', 'KAB. SAMOSIR \n');
INSERT INTO `u_kab_kota` VALUES ('39', '2', 'KAB. SERDANG BEDAGAI \n');
INSERT INTO `u_kab_kota` VALUES ('40', '2', 'KOTA MEDAN\n');
INSERT INTO `u_kab_kota` VALUES ('41', '2', 'KOTA PEMATANG SIANTAR\n');
INSERT INTO `u_kab_kota` VALUES ('42', '2', 'KOTA SIBOLGA\n');
INSERT INTO `u_kab_kota` VALUES ('43', '2', 'KOTA TANJUNG BALAI\n');
INSERT INTO `u_kab_kota` VALUES ('44', '2', 'KOTA BINJAI\n');
INSERT INTO `u_kab_kota` VALUES ('45', '2', 'KOTA TEBING TINGGI\n');
INSERT INTO `u_kab_kota` VALUES ('46', '2', 'KOTA PADANG SIDEMPUAN\n');
INSERT INTO `u_kab_kota` VALUES ('47', '3', 'KAB.PESISIR SELATAN\n');
INSERT INTO `u_kab_kota` VALUES ('48', '3', 'KAB. SOLOK\n');
INSERT INTO `u_kab_kota` VALUES ('49', '3', 'KAB. SW.LUNTO\n');
INSERT INTO `u_kab_kota` VALUES ('50', '3', 'KAB. TANAH DATAR\n');
INSERT INTO `u_kab_kota` VALUES ('51', '3', 'KAB. PADANG PARIAMAN\n');
INSERT INTO `u_kab_kota` VALUES ('52', '3', 'KAB. AGAM\n');
INSERT INTO `u_kab_kota` VALUES ('53', '3', 'KAB. LIMA PULUH KOTA\n');
INSERT INTO `u_kab_kota` VALUES ('54', '3', 'KAB. PASAMAN \n');
INSERT INTO `u_kab_kota` VALUES ('55', '3', 'KAB. KEPULAUAN MENTAWAI\n');
INSERT INTO `u_kab_kota` VALUES ('56', '3', 'KAB. DHARMASRAYA\n');
INSERT INTO `u_kab_kota` VALUES ('57', '3', 'KAB. SOLOK SELATAN \n');
INSERT INTO `u_kab_kota` VALUES ('58', '3', 'KAB. PASAMAN BARAT \n');
INSERT INTO `u_kab_kota` VALUES ('59', '3', 'KOTA PADANG\n');
INSERT INTO `u_kab_kota` VALUES ('60', '3', 'KOTA SOLOK\n');
INSERT INTO `u_kab_kota` VALUES ('61', '3', 'KOTA SAWHLUNTO\n');
INSERT INTO `u_kab_kota` VALUES ('62', '3', 'KOTA PADANG PANJANG\n');
INSERT INTO `u_kab_kota` VALUES ('63', '3', 'KOTA BUKITTINGGI\n');
INSERT INTO `u_kab_kota` VALUES ('64', '3', 'KOTA PAYAKUMBUH\n');
INSERT INTO `u_kab_kota` VALUES ('65', '3', 'KOTA PARIAMAN\n');
INSERT INTO `u_kab_kota` VALUES ('66', '4', 'KAB. KAMPAR\n');
INSERT INTO `u_kab_kota` VALUES ('67', '4', 'KAB. INDRAGIRI HULU\n');
INSERT INTO `u_kab_kota` VALUES ('68', '4', 'KAB. BENGKALIS\n');
INSERT INTO `u_kab_kota` VALUES ('69', '4', 'KAB. INDRAGIRI HILIR\n');
INSERT INTO `u_kab_kota` VALUES ('70', '4', 'KAB. PELALAWAN\n');
INSERT INTO `u_kab_kota` VALUES ('71', '4', 'KAB. ROKAN HULU\n');
INSERT INTO `u_kab_kota` VALUES ('72', '4', 'KAB. ROKAN HILIR\n');
INSERT INTO `u_kab_kota` VALUES ('73', '4', 'KAB. SIAK\n');
INSERT INTO `u_kab_kota` VALUES ('74', '4', 'KAB. KUANTAN SINGINGI\n');
INSERT INTO `u_kab_kota` VALUES ('75', '4', 'KOTA PEKAN BARU\n');
INSERT INTO `u_kab_kota` VALUES ('76', '4', 'KOTA DUMAI\n');
INSERT INTO `u_kab_kota` VALUES ('77', '5', 'KAB. KERINCI\n');
INSERT INTO `u_kab_kota` VALUES ('78', '5', 'KAB. MEANGIN\n');
INSERT INTO `u_kab_kota` VALUES ('79', '5', 'KAB. SAROLANGUN\n');
INSERT INTO `u_kab_kota` VALUES ('80', '5', 'KAB. BATANGHARI\n');
INSERT INTO `u_kab_kota` VALUES ('81', '5', 'KAB. MUARO JAMBI\n');
INSERT INTO `u_kab_kota` VALUES ('82', '5', 'KAB. TANJUNG JABUNG BARAT\n');
INSERT INTO `u_kab_kota` VALUES ('83', '5', 'KAB. TANJUNG JABUNG TIMUR\n');
INSERT INTO `u_kab_kota` VALUES ('84', '5', 'KAB. BUNGO\n');
INSERT INTO `u_kab_kota` VALUES ('85', '5', 'KAB. TEBO\n');
INSERT INTO `u_kab_kota` VALUES ('86', '5', 'KOTA JAMBI\n');
INSERT INTO `u_kab_kota` VALUES ('87', '6', 'KAB. OGAN KOMERING ULU \n');
INSERT INTO `u_kab_kota` VALUES ('88', '6', 'KAB. OGAN KOMERING ILIR \n');
INSERT INTO `u_kab_kota` VALUES ('89', '6', 'KAB. MUARA ENIM\n');
INSERT INTO `u_kab_kota` VALUES ('90', '6', 'KAB. LAHAT\n');
INSERT INTO `u_kab_kota` VALUES ('91', '6', 'KAB. MUSI RAWAS\n');
INSERT INTO `u_kab_kota` VALUES ('92', '6', 'KAB. MUSI BANYUASIN\n');
INSERT INTO `u_kab_kota` VALUES ('93', '6', 'KAB. BANYUASIN\n');
INSERT INTO `u_kab_kota` VALUES ('94', '6', 'KAB. OKU TIMUR \n');
INSERT INTO `u_kab_kota` VALUES ('95', '6', 'KAB. OKU SELATAN \n');
INSERT INTO `u_kab_kota` VALUES ('96', '6', 'KAB. OGAN ILIR \n');
INSERT INTO `u_kab_kota` VALUES ('97', '6', 'KOTA PALEMBANG\n');
INSERT INTO `u_kab_kota` VALUES ('98', '6', 'KOTA PAGAR ALAM\n');
INSERT INTO `u_kab_kota` VALUES ('99', '6', 'KOTA LUBUK LINGGAU\n');
INSERT INTO `u_kab_kota` VALUES ('100', '6', 'KOTA PRABUMULIH\n');
INSERT INTO `u_kab_kota` VALUES ('101', '7', 'KAB. BENGKULU SELATAN\n');
INSERT INTO `u_kab_kota` VALUES ('102', '7', 'KAB. REJANG LEBONG \n');
INSERT INTO `u_kab_kota` VALUES ('103', '7', 'KAB. BENGKULU UTARA\n');
INSERT INTO `u_kab_kota` VALUES ('104', '7', 'KAB. KAUR\n');
INSERT INTO `u_kab_kota` VALUES ('105', '7', 'KAB. SELUMA\n');
INSERT INTO `u_kab_kota` VALUES ('106', '7', 'KAB. MUKO MUKO\n');
INSERT INTO `u_kab_kota` VALUES ('107', '7', 'KAB. LEBONG \n');
INSERT INTO `u_kab_kota` VALUES ('108', '7', 'KAB. KEPAHIANG \n');
INSERT INTO `u_kab_kota` VALUES ('109', '7', 'KOTA BENGKULU\n');
INSERT INTO `u_kab_kota` VALUES ('110', '8', 'KAB. LAMPUNG SELATAN\n');
INSERT INTO `u_kab_kota` VALUES ('111', '8', 'KAB. LAMPUNG TENGAH\n');
INSERT INTO `u_kab_kota` VALUES ('112', '8', 'KAB. LAMPUNG UTARA\n');
INSERT INTO `u_kab_kota` VALUES ('113', '8', 'KAB. LAMPUNG BARAT\n');
INSERT INTO `u_kab_kota` VALUES ('114', '8', 'KAB. TULANG BAWANG\n');
INSERT INTO `u_kab_kota` VALUES ('115', '8', 'KAB. TANGGAMUS\n');
INSERT INTO `u_kab_kota` VALUES ('116', '8', 'KAB. LAMPUNG TIMUR\n');
INSERT INTO `u_kab_kota` VALUES ('117', '8', 'KAB. WAY KANAN\n');
INSERT INTO `u_kab_kota` VALUES ('118', '8', 'KOTA BANDAR LAMPUNG\n');
INSERT INTO `u_kab_kota` VALUES ('119', '8', 'KOTA METRO\n');
INSERT INTO `u_kab_kota` VALUES ('120', '9', 'KAB. BANGKA\n');
INSERT INTO `u_kab_kota` VALUES ('121', '9', 'KAB. BELITUNG\n');
INSERT INTO `u_kab_kota` VALUES ('122', '9', 'KAB. BANGKA SELATAN\n');
INSERT INTO `u_kab_kota` VALUES ('123', '9', 'KAB. BANGKA TENGAH\n');
INSERT INTO `u_kab_kota` VALUES ('124', '9', 'KAB. BANGKA BARAT\n');
INSERT INTO `u_kab_kota` VALUES ('125', '9', 'KAB. BANGKA TIMUR\n');
INSERT INTO `u_kab_kota` VALUES ('126', '9', 'KOTA PANGKAL PINANG\n');
INSERT INTO `u_kab_kota` VALUES ('127', '10', 'KAB. KEPULAUAN RIAU \n');
INSERT INTO `u_kab_kota` VALUES ('128', '10', 'KAB. KARIMUN\n');
INSERT INTO `u_kab_kota` VALUES ('129', '10', 'KAB. NATUNA\n');
INSERT INTO `u_kab_kota` VALUES ('130', '10', 'KAB. LINGGA \n');
INSERT INTO `u_kab_kota` VALUES ('131', '10', 'KOTA BATAM\n');
INSERT INTO `u_kab_kota` VALUES ('132', '10', 'KOTA TANJUNG PINANG\n');
INSERT INTO `u_kab_kota` VALUES ('133', '11', 'KAB.ADM.KEP.SERIBU\n');
INSERT INTO `u_kab_kota` VALUES ('134', '11', 'KODYA JAKARTA PUSAT\n');
INSERT INTO `u_kab_kota` VALUES ('135', '11', 'KODYA JAKARTA UTARA\n');
INSERT INTO `u_kab_kota` VALUES ('136', '11', 'KODYA JAKARTA BARAT\n');
INSERT INTO `u_kab_kota` VALUES ('137', '11', 'KODYA JAKARTA SELATAN\n');
INSERT INTO `u_kab_kota` VALUES ('138', '11', 'KODYA JAKARTA TIMUR\n');
INSERT INTO `u_kab_kota` VALUES ('139', '12', 'KAB. BOGOR\n');
INSERT INTO `u_kab_kota` VALUES ('140', '12', 'KAB. SUKABUMI\n');
INSERT INTO `u_kab_kota` VALUES ('141', '12', 'KAB. CIANJUR\n');
INSERT INTO `u_kab_kota` VALUES ('142', '12', 'KAB. BANDUNG\n');
INSERT INTO `u_kab_kota` VALUES ('143', '12', 'KAB. GARUT\n');
INSERT INTO `u_kab_kota` VALUES ('144', '12', 'KAB. TASIKMALAYA\n');
INSERT INTO `u_kab_kota` VALUES ('145', '12', 'KAB. CIAMIS\n');
INSERT INTO `u_kab_kota` VALUES ('146', '12', 'KAB. KUNINGAN\n');
INSERT INTO `u_kab_kota` VALUES ('147', '12', 'KAB. CIREBON\n');
INSERT INTO `u_kab_kota` VALUES ('148', '12', 'KAB. MAJALENGKA\n');
INSERT INTO `u_kab_kota` VALUES ('149', '12', 'KAB. SUMEDANG\n');
INSERT INTO `u_kab_kota` VALUES ('150', '12', 'KAB. INDRAMAYU\n');
INSERT INTO `u_kab_kota` VALUES ('151', '12', 'KAB. SUBANG\n');
INSERT INTO `u_kab_kota` VALUES ('152', '12', 'KAB. PURWAKARTA\n');
INSERT INTO `u_kab_kota` VALUES ('153', '12', 'KAB. KARAWANG\n');
INSERT INTO `u_kab_kota` VALUES ('154', '12', 'KAB. BEKASI\n');
INSERT INTO `u_kab_kota` VALUES ('155', '12', 'KOTA BOGOR\n');
INSERT INTO `u_kab_kota` VALUES ('156', '12', 'KOTA SUKABUMI\n');
INSERT INTO `u_kab_kota` VALUES ('157', '12', 'KOTA BANDUNG\n');
INSERT INTO `u_kab_kota` VALUES ('158', '12', 'KOTA CIREBON\n');
INSERT INTO `u_kab_kota` VALUES ('159', '12', 'KOTA BEKASI\n');
INSERT INTO `u_kab_kota` VALUES ('160', '12', 'KOTA DEPOK\n');
INSERT INTO `u_kab_kota` VALUES ('161', '12', 'KOTA CIMAHI\n');
INSERT INTO `u_kab_kota` VALUES ('162', '12', 'KOTA TASIKMALAYA\n');
INSERT INTO `u_kab_kota` VALUES ('163', '12', 'KOTA BANJAR\n');
INSERT INTO `u_kab_kota` VALUES ('164', '13', 'KAB. CILACAP\n');
INSERT INTO `u_kab_kota` VALUES ('165', '13', 'KAB. BANYUMAS\n');
INSERT INTO `u_kab_kota` VALUES ('166', '13', 'KAB. PURBALINGGA\n');
INSERT INTO `u_kab_kota` VALUES ('167', '13', 'KAB. BANJARNEGARA\n');
INSERT INTO `u_kab_kota` VALUES ('168', '13', 'KAB. KEBUMEN\n');
INSERT INTO `u_kab_kota` VALUES ('169', '13', 'KAB. PURWOREJO\n');
INSERT INTO `u_kab_kota` VALUES ('170', '13', 'KAB. WONOSOBO\n');
INSERT INTO `u_kab_kota` VALUES ('171', '13', 'KAB. MAGELANG\n');
INSERT INTO `u_kab_kota` VALUES ('172', '13', 'KAB. BOYOLALI\n');
INSERT INTO `u_kab_kota` VALUES ('173', '13', 'KAB. KLATEN\n');
INSERT INTO `u_kab_kota` VALUES ('174', '13', 'KAB. SUKOHARJO\n');
INSERT INTO `u_kab_kota` VALUES ('175', '13', 'KAB. WONOGIRI\n');
INSERT INTO `u_kab_kota` VALUES ('176', '13', 'KAB. KARANGANYAR\n');
INSERT INTO `u_kab_kota` VALUES ('177', '13', 'KAB. SRAGEN\n');
INSERT INTO `u_kab_kota` VALUES ('178', '13', 'KAB. GROBOGAN\n');
INSERT INTO `u_kab_kota` VALUES ('179', '13', 'KAB. BLORA\n');
INSERT INTO `u_kab_kota` VALUES ('180', '13', 'KAB. REMBANG\n');
INSERT INTO `u_kab_kota` VALUES ('181', '13', 'KAB. PATI\n');
INSERT INTO `u_kab_kota` VALUES ('182', '13', 'KAB. KUDUS\n');
INSERT INTO `u_kab_kota` VALUES ('183', '13', 'KAB. JEPARA\n');
INSERT INTO `u_kab_kota` VALUES ('184', '13', 'KAB. DEMAK\n');
INSERT INTO `u_kab_kota` VALUES ('185', '13', 'KAB. SEMARANG\n');
INSERT INTO `u_kab_kota` VALUES ('186', '13', 'KAB. TEMANGGUNG\n');
INSERT INTO `u_kab_kota` VALUES ('187', '13', 'KAB. KENDAL\n');
INSERT INTO `u_kab_kota` VALUES ('188', '13', 'KAB. BATANG\n');
INSERT INTO `u_kab_kota` VALUES ('189', '13', 'KAB. PEKALONGAN\n');
INSERT INTO `u_kab_kota` VALUES ('190', '13', 'KAB. PEMALANG\n');
INSERT INTO `u_kab_kota` VALUES ('191', '13', 'KAB. TEGAL\n');
INSERT INTO `u_kab_kota` VALUES ('192', '13', 'KAB. BREBES\n');
INSERT INTO `u_kab_kota` VALUES ('193', '13', 'KOTA MAGELANG\n');
INSERT INTO `u_kab_kota` VALUES ('194', '13', 'KOTA SURAKARTA\n');
INSERT INTO `u_kab_kota` VALUES ('195', '13', 'KOTA SALATIGA\n');
INSERT INTO `u_kab_kota` VALUES ('196', '13', 'KOTA SEMARANG\n');
INSERT INTO `u_kab_kota` VALUES ('197', '13', 'KOTA PEKALONGAN\n');
INSERT INTO `u_kab_kota` VALUES ('198', '13', 'KOTA TEGAL\n');
INSERT INTO `u_kab_kota` VALUES ('199', '14', 'KAB. KULON PROGO\n');
INSERT INTO `u_kab_kota` VALUES ('200', '14', 'KAB. BANTUL\n');
INSERT INTO `u_kab_kota` VALUES ('201', '14', 'KAB. GUNUNG KIDUL\n');
INSERT INTO `u_kab_kota` VALUES ('202', '14', 'KAB. SLEMAN\n');
INSERT INTO `u_kab_kota` VALUES ('203', '14', 'KOTA YOGYAKARTA\n');
INSERT INTO `u_kab_kota` VALUES ('204', '15', 'KAB. PACITAN\n');
INSERT INTO `u_kab_kota` VALUES ('205', '15', 'KAB. PONOROGO\n');
INSERT INTO `u_kab_kota` VALUES ('206', '15', 'KAB. TRENGGALEK\n');
INSERT INTO `u_kab_kota` VALUES ('207', '15', 'KAB. TULUNGAGUNG\n');
INSERT INTO `u_kab_kota` VALUES ('208', '15', 'KAB. BLITAR\n');
INSERT INTO `u_kab_kota` VALUES ('209', '15', 'KAB. KEDIRI\n');
INSERT INTO `u_kab_kota` VALUES ('210', '15', 'KAB. MALANG\n');
INSERT INTO `u_kab_kota` VALUES ('211', '15', 'KAB. LUMAJANG\n');
INSERT INTO `u_kab_kota` VALUES ('212', '15', 'KAB. JEMBER\n');
INSERT INTO `u_kab_kota` VALUES ('213', '15', 'KAB. BANYUWANGI\n');
INSERT INTO `u_kab_kota` VALUES ('214', '15', 'KAB. BONDOWOSO\n');
INSERT INTO `u_kab_kota` VALUES ('215', '15', 'KAB. SITUBONDO\n');
INSERT INTO `u_kab_kota` VALUES ('216', '15', 'KAB. PROBOLINGGO\n');
INSERT INTO `u_kab_kota` VALUES ('217', '15', 'KAB. PASURUAN\n');
INSERT INTO `u_kab_kota` VALUES ('218', '15', 'KAB. SIDOARJO\n');
INSERT INTO `u_kab_kota` VALUES ('219', '15', 'KAB. MOJOKERTO\n');
INSERT INTO `u_kab_kota` VALUES ('220', '15', 'KAB. JOMBANG\n');
INSERT INTO `u_kab_kota` VALUES ('221', '15', 'KAB. NGANJUK\n');
INSERT INTO `u_kab_kota` VALUES ('222', '15', 'KAB. MADIUN\n');
INSERT INTO `u_kab_kota` VALUES ('223', '15', 'KAB. MAGETAN\n');
INSERT INTO `u_kab_kota` VALUES ('224', '15', 'KAB. NGAWI\n');
INSERT INTO `u_kab_kota` VALUES ('225', '15', 'KAB. BOJONEGORO\n');
INSERT INTO `u_kab_kota` VALUES ('226', '15', 'KAB. TUBAN\n');
INSERT INTO `u_kab_kota` VALUES ('227', '15', 'KAB. LAMONGAN\n');
INSERT INTO `u_kab_kota` VALUES ('228', '15', 'KAB. GRESIK\n');
INSERT INTO `u_kab_kota` VALUES ('229', '15', 'KAB. BANGKALAN\n');
INSERT INTO `u_kab_kota` VALUES ('230', '15', 'KAB. SAMPANG\n');
INSERT INTO `u_kab_kota` VALUES ('231', '15', 'KAB. PAMEKASAN\n');
INSERT INTO `u_kab_kota` VALUES ('232', '15', 'KAB. SUMENEP\n');
INSERT INTO `u_kab_kota` VALUES ('233', '15', 'KOTA KEDIRI\n');
INSERT INTO `u_kab_kota` VALUES ('234', '15', 'KOTA BLITAR\n');
INSERT INTO `u_kab_kota` VALUES ('235', '15', 'KOTA MALANG\n');
INSERT INTO `u_kab_kota` VALUES ('236', '15', 'KOTA PROBOLINGGO\n');
INSERT INTO `u_kab_kota` VALUES ('237', '15', 'KOTA PASURUAN\n');
INSERT INTO `u_kab_kota` VALUES ('238', '15', 'KOTA MOJOKERTO\n');
INSERT INTO `u_kab_kota` VALUES ('239', '15', 'KOTA MADIUN\n');
INSERT INTO `u_kab_kota` VALUES ('240', '15', 'KOTA SURABAYA\n');
INSERT INTO `u_kab_kota` VALUES ('241', '15', 'BATU\n');
INSERT INTO `u_kab_kota` VALUES ('242', '16', 'KAB. PANDEGLANG\n');
INSERT INTO `u_kab_kota` VALUES ('243', '16', 'KAB. LEBAK\n');
INSERT INTO `u_kab_kota` VALUES ('244', '16', 'KAB. TANGERANG\n');
INSERT INTO `u_kab_kota` VALUES ('245', '16', 'KAB. SERANG\n');
INSERT INTO `u_kab_kota` VALUES ('246', '16', 'KOTA TANGERANG\n');
INSERT INTO `u_kab_kota` VALUES ('247', '16', 'KOTA CILEGON\n');
INSERT INTO `u_kab_kota` VALUES ('248', '17', 'KAB. JEMBARANA\n');
INSERT INTO `u_kab_kota` VALUES ('249', '17', 'KAB. TABANAN\n');
INSERT INTO `u_kab_kota` VALUES ('250', '17', 'KAB. BADUNG\n');
INSERT INTO `u_kab_kota` VALUES ('251', '17', 'KAB. GIANYAR\n');
INSERT INTO `u_kab_kota` VALUES ('252', '17', 'KAB. KLUNGKUNG\n');
INSERT INTO `u_kab_kota` VALUES ('253', '17', 'KAB. BANGLI\n');
INSERT INTO `u_kab_kota` VALUES ('254', '17', 'KAB. KARANGASEM\n');
INSERT INTO `u_kab_kota` VALUES ('255', '17', 'KAB. BULELENG\n');
INSERT INTO `u_kab_kota` VALUES ('256', '17', 'KOTA DENPASAR\n');
INSERT INTO `u_kab_kota` VALUES ('257', '18', 'KAB. LOMBOK BARAT\n');
INSERT INTO `u_kab_kota` VALUES ('258', '18', 'KAB. LOMBOK TENGAH\n');
INSERT INTO `u_kab_kota` VALUES ('259', '18', 'KAB. LOMBOK TIMUR\n');
INSERT INTO `u_kab_kota` VALUES ('260', '18', 'KAB. SUMBAWA \n');
INSERT INTO `u_kab_kota` VALUES ('261', '18', 'KAB. DOMPU\n');
INSERT INTO `u_kab_kota` VALUES ('262', '18', 'KAB. BIMA\n');
INSERT INTO `u_kab_kota` VALUES ('263', '18', 'KAB. SUMBAWA BARAT\n');
INSERT INTO `u_kab_kota` VALUES ('264', '18', 'KOTA MATARAM\n');
INSERT INTO `u_kab_kota` VALUES ('265', '18', 'KOTA BIMA\n');
INSERT INTO `u_kab_kota` VALUES ('266', '19', 'KAB. KUPANG\n');
INSERT INTO `u_kab_kota` VALUES ('267', '19', 'KAB. TIMOR TENGAH SELATAN\n');
INSERT INTO `u_kab_kota` VALUES ('268', '19', 'KAB. TIMOR TENGAH UTARA\n');
INSERT INTO `u_kab_kota` VALUES ('269', '19', 'KAB. BELU\n');
INSERT INTO `u_kab_kota` VALUES ('270', '19', 'KAB. ALOR\n');
INSERT INTO `u_kab_kota` VALUES ('271', '19', 'KAB. FLORES TIMUR\n');
INSERT INTO `u_kab_kota` VALUES ('272', '19', 'KAB. SIKKA\n');
INSERT INTO `u_kab_kota` VALUES ('273', '19', 'KAB. ENDE\n');
INSERT INTO `u_kab_kota` VALUES ('274', '19', 'KAB. NGADA\n');
INSERT INTO `u_kab_kota` VALUES ('275', '19', 'KAB. MANGGARAI\n');
INSERT INTO `u_kab_kota` VALUES ('276', '19', 'KAB. SUMBA TIMUR\n');
INSERT INTO `u_kab_kota` VALUES ('277', '19', 'KAB. SUMBA BARAT\n');
INSERT INTO `u_kab_kota` VALUES ('278', '19', 'KAB. LEMBATA\n');
INSERT INTO `u_kab_kota` VALUES ('279', '19', 'KAB. ROTE NDAO\n');
INSERT INTO `u_kab_kota` VALUES ('280', '19', 'KAB. MANGGARAI BARAT\n');
INSERT INTO `u_kab_kota` VALUES ('281', '19', 'KOTA KUPANG\n');
INSERT INTO `u_kab_kota` VALUES ('282', '20', 'KAB. SAMBAS\n');
INSERT INTO `u_kab_kota` VALUES ('283', '20', 'KAB. PONTIANAK\n');
INSERT INTO `u_kab_kota` VALUES ('284', '20', 'KAB. SANGGAU \n');
INSERT INTO `u_kab_kota` VALUES ('285', '20', 'KAB. KETAPANG\n');
INSERT INTO `u_kab_kota` VALUES ('286', '20', 'KAB. SINTANG \n');
INSERT INTO `u_kab_kota` VALUES ('287', '20', 'KAB. KAPUAS HULU\n');
INSERT INTO `u_kab_kota` VALUES ('288', '20', 'KAB. BENGKAYANG\n');
INSERT INTO `u_kab_kota` VALUES ('289', '20', 'KAB. LANDAK\n');
INSERT INTO `u_kab_kota` VALUES ('290', '20', 'KAB. MELAWI \n');
INSERT INTO `u_kab_kota` VALUES ('291', '20', 'KAB. SEKADAU \n');
INSERT INTO `u_kab_kota` VALUES ('292', '20', 'KOTA PONTIANAK\n');
INSERT INTO `u_kab_kota` VALUES ('293', '20', 'KOTA SINGKAWANG\n');
INSERT INTO `u_kab_kota` VALUES ('294', '21', 'KAB. KOTAWARINGIN BARAT\n');
INSERT INTO `u_kab_kota` VALUES ('295', '21', 'KAB. KOTAWARINGIN TIMUR\n');
INSERT INTO `u_kab_kota` VALUES ('296', '21', 'KAB. KAPUAS\n');
INSERT INTO `u_kab_kota` VALUES ('297', '21', 'KAB. BARITO SELATAN\n');
INSERT INTO `u_kab_kota` VALUES ('298', '21', 'KAB. BARITO UTARA\n');
INSERT INTO `u_kab_kota` VALUES ('299', '21', 'KAB. KATINGIN\n');
INSERT INTO `u_kab_kota` VALUES ('300', '21', 'KAB. SERUYAN\n');
INSERT INTO `u_kab_kota` VALUES ('301', '21', 'KAB. SUKAMARA\n');
INSERT INTO `u_kab_kota` VALUES ('302', '21', 'KAB. LAMANDAU\n');
INSERT INTO `u_kab_kota` VALUES ('303', '21', 'KAB. GUNUNG MAS\n');
INSERT INTO `u_kab_kota` VALUES ('304', '21', 'KAB. PULANG PISAU\n');
INSERT INTO `u_kab_kota` VALUES ('305', '21', 'KAB. MURUNG RAYA\n');
INSERT INTO `u_kab_kota` VALUES ('306', '21', 'KAB. BARITO TIMUR\n');
INSERT INTO `u_kab_kota` VALUES ('307', '21', 'KOTA PALANGKARAYA\n');
INSERT INTO `u_kab_kota` VALUES ('308', '22', 'KAB. TANAH LAUT\n');
INSERT INTO `u_kab_kota` VALUES ('309', '22', 'KAB. KOTABARU\n');
INSERT INTO `u_kab_kota` VALUES ('310', '22', 'KAB. BANJAR\n');
INSERT INTO `u_kab_kota` VALUES ('311', '22', 'KAB. BARITO KUALA\n');
INSERT INTO `u_kab_kota` VALUES ('312', '22', 'KAB. TAPIN\n');
INSERT INTO `u_kab_kota` VALUES ('313', '22', 'KAB. HULU SUNGAI SELATAN\n');
INSERT INTO `u_kab_kota` VALUES ('314', '22', 'KAB. HULU SUNGAI TENGAH\n');
INSERT INTO `u_kab_kota` VALUES ('315', '22', 'KAB. HULU SUNGAI UTARA\n');
INSERT INTO `u_kab_kota` VALUES ('316', '22', 'KAB. TABALONG\n');
INSERT INTO `u_kab_kota` VALUES ('317', '22', 'KAB. TANAH BAMBU\n');
INSERT INTO `u_kab_kota` VALUES ('318', '22', 'KAB. BALANGAN\n');
INSERT INTO `u_kab_kota` VALUES ('319', '22', 'KOTA BANJARMASIN\n');
INSERT INTO `u_kab_kota` VALUES ('320', '22', 'KOTA BANJARBARU\n');
INSERT INTO `u_kab_kota` VALUES ('321', '23', 'KAB. PASIR\n');
INSERT INTO `u_kab_kota` VALUES ('322', '23', 'KAB. KUTAI KERTANEGARA\n');
INSERT INTO `u_kab_kota` VALUES ('323', '23', 'KAB. BERAU\n');
INSERT INTO `u_kab_kota` VALUES ('324', '23', 'KAB. BULUNGAN\n');
INSERT INTO `u_kab_kota` VALUES ('325', '23', 'KAB. NUNUKAN\n');
INSERT INTO `u_kab_kota` VALUES ('326', '23', 'KAB. MALINAU\n');
INSERT INTO `u_kab_kota` VALUES ('327', '23', 'KAB. KUTAI BARAT\n');
INSERT INTO `u_kab_kota` VALUES ('328', '23', 'KAB. KUTAI TIMUR\n');
INSERT INTO `u_kab_kota` VALUES ('329', '23', 'KAB. PENAJAM PASER UTARA\n');
INSERT INTO `u_kab_kota` VALUES ('330', '23', 'KOTA BALIKPAPAN\n');
INSERT INTO `u_kab_kota` VALUES ('331', '23', 'KOTA SAMARINDA\n');
INSERT INTO `u_kab_kota` VALUES ('332', '23', 'KOTA TARAKAN\n');
INSERT INTO `u_kab_kota` VALUES ('333', '23', 'KOTA BONTANG\n');
INSERT INTO `u_kab_kota` VALUES ('334', '24', 'KAB. BOLAANG MANGONDOW\n');
INSERT INTO `u_kab_kota` VALUES ('335', '24', 'KAB. MINAHASA \n');
INSERT INTO `u_kab_kota` VALUES ('336', '24', 'KAB. KEPULAUAN SANGIHE\n');
INSERT INTO `u_kab_kota` VALUES ('337', '24', 'KAB. KEPULAUAN TALAUD\n');
INSERT INTO `u_kab_kota` VALUES ('338', '24', 'KAB. MINAHASA SELATAN\n');
INSERT INTO `u_kab_kota` VALUES ('339', '24', 'KAB. MINAHASA UTARA\n');
INSERT INTO `u_kab_kota` VALUES ('340', '24', 'KOTA MANADO\n');
INSERT INTO `u_kab_kota` VALUES ('341', '24', 'KOTA BITUNG\n');
INSERT INTO `u_kab_kota` VALUES ('342', '24', 'KOTA TOMOHON\n');
INSERT INTO `u_kab_kota` VALUES ('343', '25', 'KAB. BANGGAI\n');
INSERT INTO `u_kab_kota` VALUES ('344', '25', 'KAB. POSO \n');
INSERT INTO `u_kab_kota` VALUES ('345', '25', 'KAB. DONGGALA\n');
INSERT INTO `u_kab_kota` VALUES ('346', '25', 'KAB. TOLOI TOLI\n');
INSERT INTO `u_kab_kota` VALUES ('347', '25', 'KAB. BUOL\n');
INSERT INTO `u_kab_kota` VALUES ('348', '25', 'KAB. MOROWALI\n');
INSERT INTO `u_kab_kota` VALUES ('349', '25', 'KAB. BANGGAI KEPULAUAN\n');
INSERT INTO `u_kab_kota` VALUES ('350', '25', 'KAB. PARIGI MOUTONG\n');
INSERT INTO `u_kab_kota` VALUES ('351', '25', 'KAB. TOJO UNA UNA\n');
INSERT INTO `u_kab_kota` VALUES ('352', '25', 'KOTA PALU\n');
INSERT INTO `u_kab_kota` VALUES ('353', '26', 'KAB. SELAYAR\n');
INSERT INTO `u_kab_kota` VALUES ('354', '26', 'KAB. BULUKUMBA\n');
INSERT INTO `u_kab_kota` VALUES ('355', '26', 'KAB. BANTAENG\n');
INSERT INTO `u_kab_kota` VALUES ('356', '26', 'KAB. JENEPONTO.\n');
INSERT INTO `u_kab_kota` VALUES ('357', '26', 'KAB. TAKALAR\n');
INSERT INTO `u_kab_kota` VALUES ('358', '26', 'KAB. GOWA\n');
INSERT INTO `u_kab_kota` VALUES ('359', '26', 'KAB. SINJAI\n');
INSERT INTO `u_kab_kota` VALUES ('360', '26', 'KAB. BONE\n');
INSERT INTO `u_kab_kota` VALUES ('361', '26', 'KAB. MAROS\n');
INSERT INTO `u_kab_kota` VALUES ('362', '26', 'KAB. PANGKAJENE KEP.\n');
INSERT INTO `u_kab_kota` VALUES ('363', '26', 'KAB. BARRU\n');
INSERT INTO `u_kab_kota` VALUES ('364', '26', 'KAB. SOPPENG\n');
INSERT INTO `u_kab_kota` VALUES ('365', '26', 'KAB. WAJO\n');
INSERT INTO `u_kab_kota` VALUES ('366', '26', 'KAB. SIDENRENG RAPANG\n');
INSERT INTO `u_kab_kota` VALUES ('367', '26', 'KAB. PINRANG\n');
INSERT INTO `u_kab_kota` VALUES ('368', '26', 'KAB. ENREKANG\n');
INSERT INTO `u_kab_kota` VALUES ('369', '26', 'KAB. LUWU\n');
INSERT INTO `u_kab_kota` VALUES ('370', '26', 'KAB. TANA TORAJA\n');
INSERT INTO `u_kab_kota` VALUES ('371', '26', 'KAB. LUWU UTARA\n');
INSERT INTO `u_kab_kota` VALUES ('372', '26', 'KAB. LUWU TIMUR\n');
INSERT INTO `u_kab_kota` VALUES ('373', '26', 'KOTA MAKASAR\n');
INSERT INTO `u_kab_kota` VALUES ('374', '26', 'KOTA PARE PARE\n');
INSERT INTO `u_kab_kota` VALUES ('375', '26', 'KOTA PALOPO\n');
INSERT INTO `u_kab_kota` VALUES ('376', '27', 'KAB. KOLAKA\n');
INSERT INTO `u_kab_kota` VALUES ('377', '27', 'KAB. KONAWE\n');
INSERT INTO `u_kab_kota` VALUES ('378', '27', 'KAB. MUNA\n');
INSERT INTO `u_kab_kota` VALUES ('379', '27', 'KAB. BUTON\n');
INSERT INTO `u_kab_kota` VALUES ('380', '27', 'KAB. KONAWE SELATAN\n');
INSERT INTO `u_kab_kota` VALUES ('381', '27', 'KAB. BOMBANA\n');
INSERT INTO `u_kab_kota` VALUES ('382', '27', 'KAB. WAKATOBI\n');
INSERT INTO `u_kab_kota` VALUES ('383', '27', 'KAB. KOLAKA UTARA\n');
INSERT INTO `u_kab_kota` VALUES ('384', '27', 'KOTA KENDARI\n');
INSERT INTO `u_kab_kota` VALUES ('385', '27', 'KOTA BAU BAU\n');
INSERT INTO `u_kab_kota` VALUES ('386', '28', 'KAB. GORONTALO\n');
INSERT INTO `u_kab_kota` VALUES ('387', '28', 'KAB. BOALEMO\n');
INSERT INTO `u_kab_kota` VALUES ('388', '28', 'KAB. BONE BOLANGO\n');
INSERT INTO `u_kab_kota` VALUES ('389', '28', 'KAB. PAHUWATO\n');
INSERT INTO `u_kab_kota` VALUES ('390', '28', 'KOTA GORONTALO\n');
INSERT INTO `u_kab_kota` VALUES ('391', '29', 'KAB. MAMUJU UTARA\n');
INSERT INTO `u_kab_kota` VALUES ('392', '29', 'KAB. MAMUJU\n');
INSERT INTO `u_kab_kota` VALUES ('393', '29', 'KAB. MAMASA\n');
INSERT INTO `u_kab_kota` VALUES ('394', '29', 'KAB. POLOWALI MAMASA\n');
INSERT INTO `u_kab_kota` VALUES ('395', '29', 'KAB. MAJENE\n');
INSERT INTO `u_kab_kota` VALUES ('396', '30', 'KAB. MALUKU TENGAH \n');
INSERT INTO `u_kab_kota` VALUES ('397', '30', 'KAB. MALUKU TENGGARA \n');
INSERT INTO `u_kab_kota` VALUES ('398', '30', 'KAB. MALUKU TENGGARA BRT\n');
INSERT INTO `u_kab_kota` VALUES ('399', '30', 'KAB. BURU\n');
INSERT INTO `u_kab_kota` VALUES ('400', '30', 'KAB. SERAM BAGIAN TIMUR\n');
INSERT INTO `u_kab_kota` VALUES ('401', '30', 'KAB. SERAM BAGIAN BARAT \n');
INSERT INTO `u_kab_kota` VALUES ('402', '30', 'KAB. KEPULAUAN ARU\n');
INSERT INTO `u_kab_kota` VALUES ('403', '30', 'KOTA AMBON\n');
INSERT INTO `u_kab_kota` VALUES ('404', '31', 'KAB. HALMAHERA BARAT\n');
INSERT INTO `u_kab_kota` VALUES ('405', '31', 'KAB. HALMAHERA TENGAH\n');
INSERT INTO `u_kab_kota` VALUES ('406', '31', 'KAB. HALMAHERA UTARA\n');
INSERT INTO `u_kab_kota` VALUES ('407', '31', 'KAB. HALMAHERA SELATAN\n');
INSERT INTO `u_kab_kota` VALUES ('408', '31', 'KAB. KEPULAUAN SULA\n');
INSERT INTO `u_kab_kota` VALUES ('409', '31', 'KAB. HALMAHERA TIMUR\n');
INSERT INTO `u_kab_kota` VALUES ('410', '31', 'KOTA TERNATE\n');
INSERT INTO `u_kab_kota` VALUES ('411', '31', 'KOTA TIDORE KEPULAUAN\n');
INSERT INTO `u_kab_kota` VALUES ('412', '32', 'KAB. MERAUKE\n');
INSERT INTO `u_kab_kota` VALUES ('413', '32', 'KAB. JAYAWIJAYA\n');
INSERT INTO `u_kab_kota` VALUES ('414', '32', 'KAB. JAYAPURA\n');
INSERT INTO `u_kab_kota` VALUES ('415', '32', 'KAB. NABIRE\n');
INSERT INTO `u_kab_kota` VALUES ('416', '32', 'KAB. YAPEN WAROPEN\n');
INSERT INTO `u_kab_kota` VALUES ('417', '32', 'KAB. BIAK NUMFOR\n');
INSERT INTO `u_kab_kota` VALUES ('418', '32', 'KAB. PUNCAK JAYA\n');
INSERT INTO `u_kab_kota` VALUES ('419', '32', 'KAB. PANIAI\n');
INSERT INTO `u_kab_kota` VALUES ('420', '32', 'KAB. MIMIKA\n');
INSERT INTO `u_kab_kota` VALUES ('421', '32', 'KAB. SARMI\n');
INSERT INTO `u_kab_kota` VALUES ('422', '32', 'KAB. KEEROM\n');
INSERT INTO `u_kab_kota` VALUES ('423', '32', 'KAB. PEGUNUNGAN BINTANG\n');
INSERT INTO `u_kab_kota` VALUES ('424', '32', 'KAB. YAHUKIMO\n');
INSERT INTO `u_kab_kota` VALUES ('425', '32', 'KAB. TOLIKARA\n');
INSERT INTO `u_kab_kota` VALUES ('426', '32', 'KAB. WAROPEN\n');
INSERT INTO `u_kab_kota` VALUES ('427', '32', 'KAB. BOVEN DIGOEL\n');
INSERT INTO `u_kab_kota` VALUES ('428', '32', 'KABUPATEN. MAPPI\n');
INSERT INTO `u_kab_kota` VALUES ('429', '32', 'KAB. ASMAT\n');
INSERT INTO `u_kab_kota` VALUES ('430', '32', 'KAB. SUPIORI\n');
INSERT INTO `u_kab_kota` VALUES ('431', '32', 'KOTA JAYAPURA\n');
INSERT INTO `u_kab_kota` VALUES ('432', '33', 'KAB. SORONG\n');
INSERT INTO `u_kab_kota` VALUES ('433', '33', 'KAB. MANOKWARI\n');
INSERT INTO `u_kab_kota` VALUES ('434', '33', 'KAB. FAK FAK\n');
INSERT INTO `u_kab_kota` VALUES ('435', '33', 'KAB. SORONG SELATAN\n');
INSERT INTO `u_kab_kota` VALUES ('436', '33', 'KAB. RAJA AMPAT\n');
INSERT INTO `u_kab_kota` VALUES ('437', '33', 'KAB. TELUK BENTUNI\n');
INSERT INTO `u_kab_kota` VALUES ('438', '33', 'KAB. TELUK WONDAMA\n');
INSERT INTO `u_kab_kota` VALUES ('439', '33', 'KAB. KAIMA\n');
INSERT INTO `u_kab_kota` VALUES ('440', '33', 'KOTA SORONG\n');

-- ----------------------------
-- Table structure for `u_provinsi`
-- ----------------------------
DROP TABLE IF EXISTS `u_provinsi`;
CREATE TABLE `u_provinsi` (
  `id_provinsi` smallint(3) NOT NULL AUTO_INCREMENT,
  `provinsi` varchar(100) NOT NULL,
  PRIMARY KEY (`id_provinsi`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of u_provinsi
-- ----------------------------
INSERT INTO `u_provinsi` VALUES ('1', 'NANGGROE ACEH DARUSSALAM');
INSERT INTO `u_provinsi` VALUES ('2', 'SUMATERA UTARA');
INSERT INTO `u_provinsi` VALUES ('3', 'SUMATERA BARAT');
INSERT INTO `u_provinsi` VALUES ('4', 'RIAU');
INSERT INTO `u_provinsi` VALUES ('5', 'JAMBI');
INSERT INTO `u_provinsi` VALUES ('6', 'SUMATERA SELATAN');
INSERT INTO `u_provinsi` VALUES ('7', 'BENGKULU');
INSERT INTO `u_provinsi` VALUES ('8', 'LAMPUNG');
INSERT INTO `u_provinsi` VALUES ('9', 'KEP BANGKA BELITUNG');
INSERT INTO `u_provinsi` VALUES ('10', 'KEP RIAU');
INSERT INTO `u_provinsi` VALUES ('11', 'DKI JAKARTA');
INSERT INTO `u_provinsi` VALUES ('12', 'JAWA BARAT');
INSERT INTO `u_provinsi` VALUES ('13', 'JAWA TENGAH');
INSERT INTO `u_provinsi` VALUES ('14', 'DI JOGJAKARTA');
INSERT INTO `u_provinsi` VALUES ('15', 'JAWA TIMUR');
INSERT INTO `u_provinsi` VALUES ('16', 'BANTEN');
INSERT INTO `u_provinsi` VALUES ('17', 'BALI');
INSERT INTO `u_provinsi` VALUES ('18', 'NUSA TENGGARA BARAT');
INSERT INTO `u_provinsi` VALUES ('19', 'NUSA TENGGARA TIMUR');
INSERT INTO `u_provinsi` VALUES ('20', 'KALIMANTAN BARAT');
INSERT INTO `u_provinsi` VALUES ('21', 'KALIMANTAN TENGAH');
INSERT INTO `u_provinsi` VALUES ('22', 'KALIMANTA SELATAN');
INSERT INTO `u_provinsi` VALUES ('23', 'KALIMANTAN TIMUR');
INSERT INTO `u_provinsi` VALUES ('24', 'SULAWESI UTARA');
INSERT INTO `u_provinsi` VALUES ('25', 'SULAWESI TENGAH');
INSERT INTO `u_provinsi` VALUES ('26', 'SULAWESI SELATAN');
INSERT INTO `u_provinsi` VALUES ('27', 'SULAWESI TENGGARA');
INSERT INTO `u_provinsi` VALUES ('28', 'GORONTALO     ');
INSERT INTO `u_provinsi` VALUES ('29', 'SULAWESI BARAT');
INSERT INTO `u_provinsi` VALUES ('30', 'MALUKU');
INSERT INTO `u_provinsi` VALUES ('31', 'MALUKU UTARA');
INSERT INTO `u_provinsi` VALUES ('32', 'PAPUA');
INSERT INTO `u_provinsi` VALUES ('33', 'PAPUA BARAT (PP No 24/2007)');
